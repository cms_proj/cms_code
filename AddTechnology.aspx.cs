﻿//This code is to add new technologies into the database for example android,java,python etc

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class AddTechnology : System.Web.UI.Page
{
    connection c = new connection();
   
    //Bind Technologies

    public void gridbind()
    {
        string gb = "Select * from Technology";
        SqlCommand cmd = new SqlCommand(gb, c.con);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Login_Id"] == null)
                Response.Redirect("Home.aspx");

            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache,no-store,max-age=0,must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
            c.getcon();
            gridbind();
            c.con.Close();
        }
    }
    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView1.EditIndex = e.NewEditIndex;
        c.getcon();
        gridbind();
        c.con.Close();
    }
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        c.getcon();
        string id = GridView1.DataKeys[e.RowIndex].Values[0].ToString();
        string del = "Delete Technology where Tech_Id='" + Convert.ToString(id) + "'";
        SqlCommand cmd = new SqlCommand(del, c.con);
        cmd.ExecuteNonQuery();
        GridView1.EditIndex = -1;
        c.con.Close();
        gridbind();

    }
    protected void btn_add_Click(object sender, EventArgs e)
    {
        c.getcon();
        string insert = "insert into Technology values('"+txt_tech.Text+"')";
        SqlCommand cmd = new SqlCommand(insert, c.con);
        cmd.ExecuteNonQuery();
        gridbind();
        Panel_Popup.Visible = true;
        btn_add_ModalPopupExtender.Show();
        c.con.Close();
    }
    
protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
{
        c.getcon();
        int id = Convert.ToInt32(GridView1.DataKeys[e.RowIndex].Values[0].ToString());
        string tech = ((TextBox)GridView1.Rows[e.RowIndex].Cells[1].Controls[0]).Text;
        string update="Update Technology set Tech='"+tech+"'where Tech_Id='"+id+"'";
        SqlCommand cmd = new SqlCommand(update, c.con);
        cmd.ExecuteNonQuery();
        gridbind();
        c.con.Close();

}
protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
{
    GridView1.EditIndex = -1;
    c.getcon();
    gridbind();
    c.con.Close();
}
protected void btb_OK_Click(object sender, EventArgs e)
{
    Response.Redirect("AddTechnology.aspx");
}
}