﻿
//This code is to add Banks and Corresponding branches into the database

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class BankBranch : System.Web.UI.Page
{
    connection c = new connection();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Login_Id"] == null)
                Response.Redirect("Home.aspx");

            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache,no-store,max-age=0,must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
            c.getcon();
            BindBank();
            c.con.Close();
        }
    }

    //Bind Banks already stored in the database to the grid

    public void gridbind()
    {
        string gb = "Select Branch_Id,Branch_Name from Branch where Bank_Id='"+ddl_Bank.SelectedItem.Value+"'";
        SqlCommand cmd = new SqlCommand(gb, c.con);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
    }
    
    //Binds banks to the dropdownlist

    public void BindBank()
    {
        string sel = "Select * from Bank";
        SqlCommand cmd = new SqlCommand(sel, c.con);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            ddl_Bank.DataSource = dt;
            ddl_Bank.DataValueField = "Bank_Id";
            ddl_Bank.DataTextField = "Bank_Name";
            ddl_Bank.DataBind();
        }
        ddl_Bank.Items.Insert(0, "Select");
        ddl_Bank.Items.Add("Add");
    }
    protected void ddl_Bank_SelectedIndexChanged(object sender, EventArgs e)
    {
        c.getcon();
        if (ddl_Bank.SelectedItem.Text == "Add")
        {
            txt_bank.Visible = true;
            
        }
        else
        {
            gridbind();
        }
        c.con.Close();
    }


    protected void btn_add_Click(object sender, EventArgs e)
    {
        c.getcon();
        string bank="Insert into Bank Values('"+txt_bank.Text+"')Select @@Identity";
        SqlCommand cmd = new SqlCommand(bank, c.con);
        object obj = new object();
        obj = cmd.ExecuteScalar();
        
        string ins = "insert into Branch values('" + txt_branch.Text + "')where Bank_Id='" + Convert.ToInt32(obj)+"'";
        SqlCommand cmd2 = new SqlCommand(ins, c.con);
        cmd2.ExecuteNonQuery();
        Response.Redirect("BankBranch.aspx");
        c.con.Close();
        
    }
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }
}