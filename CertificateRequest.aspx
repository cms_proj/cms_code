﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StaffMaster.master" AutoEventWireup="true" CodeFile="CertificateRequest.aspx.cs" Inherits="CertificateRequest" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <style type="text/css">         
        .modalBackground
    {
        background-color: Black;
        filter: alpha(opacity=60);
        opacity: 0.6;
    }
    .modalPopup
    {
        background-color: #FFFFFF;
        border: 3px solid #0DA9D0;
        border-radius: 12px;
        padding:0
        }
    .modalPopup .header
    {
        background-color: #2FBDF1;
        height: 30px;
        color: White;
        line-height: 30px;
        text-align: center;
        font-weight: bold;
        border-top-left-radius: 6px;
        border-top-right-radius: 6px;
    }
    .modalPopup .body
    {
        min-height: 50px;
        line-height: 30px;
        text-align: center;
        font-weight: bold;
    }
    .modalPopup .footer
    {
        padding: 6px;
    }
    .modalPopup .yes
    {
        background-color: #2FBDF1;
        border: 1px solid #0DA9D0;
    }
        .auto-style5 {
            height: 20px;
        }
    </style>
    <table class="nav-justified">
        <tr>
            <td style="width: 250px; height: 100px">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:Panel ID="Panel1" runat="server" Height="1133px" style="margin-bottom: 6px" BorderStyle="Outset" Width="867px">
                    <table class="nav-justified">
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="text-center" colspan="3" style="font-size: 40px; font-weight: bold; text-decoration: underline;">Certificate Request Form</td>
                        </tr>
                        <tr>
                            <td style="width: 50px">&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td style="font-size: 15px; font-weight: bold; color: #0000CC;">Title&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <br />
                                <asp:DropDownList ID="ddl_title" runat="server" CssClass="search_categories">
                                    <asp:ListItem Value="0">Select</asp:ListItem>
                                    <asp:ListItem Value="1">Mr</asp:ListItem>
                                    <asp:ListItem Value="2">Ms</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width: 100px">&nbsp;</td>
                            <td style="height: 40px">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td style="font-size: 15px; font-weight: bold; color: #0000CC;">Student Name<br />
                                <asp:TextBox ID="txt_name" runat="server" CssClass="twitterStyleTextbox" Height="30px" Width="200px"></asp:TextBox>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td style="height: 40px">&nbsp;</td>
                            <td>
                                <asp:ScriptManager ID="ScriptManager1" runat="server">
                                </asp:ScriptManager>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td style="font-size: 15px; font-weight: bold; color: #0000CC;">Register No(College)<br /> University Register No. for Exam<br />
                                <asp:TextBox ID="txt_Regno" runat="server" CssClass="twitterStyleTextbox" Height="30px" Width="200px"></asp:TextBox>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td style="height: 40px">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td style="height: 40px; font-size: 15px; font-weight: bold; color: #0000CC;">Semester<br />
                                <asp:TextBox ID="txt_Sem" runat="server" CssClass="twitterStyleTextbox" Height="30px" Width="200px"></asp:TextBox>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td style="height: 40px">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td style="font-size: 15px; font-weight: bold; color: #0000CC;">College Name &amp; Location<br />
                                <asp:TextBox ID="txt_colege" runat="server" CssClass="twitterStyleTextbox" Width="400px" Height="30px"></asp:TextBox>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td style="height: 40px">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td style="height: 40px; font-size: 15px; font-weight: bold; color: #0000CC;">University<br />
                                <asp:TextBox ID="txt_univ" runat="server" Height="30px" Width="200px"></asp:TextBox>
                                <br />
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td style="font-size: 15px; font-weight: bold; color: #0000CC;">Course<br /> eg:(MCA,B.Tech/BE)<br />
                                <asp:TextBox ID="txt_course" runat="server" CssClass="twitterStyleTextbox" Height="30px" Width="200px"></asp:TextBox>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td style="height: 40px">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td style="font-size: 15px; font-weight: bold; color: #0000CC;">Project Name<br />
                                <asp:TextBox ID="txt_project" runat="server" CssClass="twitterStyleTextbox" Width="400px" Height="30px"></asp:TextBox>
                                <br />
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td style="height: 40px">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td style="font-size: 15px; font-weight: bold; color: #0000CC;">Project Duration</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td style="font-size: 15px; font-weight: bold; color: #0000CC;">From
                                <asp:TextBox ID="txt_from" runat="server" CssClass="twitterStyleTextbox" Height="30px" Width="200px"></asp:TextBox>
                                <cc1:CalendarExtender ID="txt_from_CalendarExtender" runat="server" Format="dd-MMM-yyyy" Enabled="True" TargetControlID="txt_from">
                                </cc1:CalendarExtender>
                            </td>
                            <td style="font-size: 15px; font-weight: bold; color: #0000CC; width: 200px;">To
                                <asp:TextBox ID="txt_to" runat="server" CssClass="twitterStyleTextbox" Height="30px" Width="200px"></asp:TextBox>
                                <cc1:CalendarExtender ID="txt_to_CalendarExtender" runat="server"  Format="dd-MMM-yyyy" Enabled="True" TargetControlID="txt_to">
                                </cc1:CalendarExtender>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td style="height: 40px">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td style="font-size: 15px; font-weight: bold; color: #0000CC;">I hereby declare that the above mentioned details are true to the best of my knowledge and belief</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td style="font-size: 15px; font-weight: bold">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <asp:CheckBox ID="Ckb_yes" runat="server" Text="Yes" ForeColor="#0000CC" />
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <asp:Button ID="btn_submit" runat="server" OnClick="btn_submit_Click" style="margin-left: 231px" Text="Submit" CssClass="btnme" />
                                 <cc1:ModalPopupExtender ID="btn_submit_ModalPopupExtender" runat="server" BackgroundCssClass="modalBackground" DynamicServicePath="" Enabled="True" PopupControlID="Panel_Popup" RepositionMode="None" TargetControlID="btn_submit"></cc1:ModalPopupExtender>
                            
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="auto-style5"></td>
                            <td class="auto-style5"></td>
                            <td class="auto-style5"></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <asp:Panel ID="Panel_Popup" runat="server" BorderStyle="Solid" CssClass="modalPopup" Height="141px" Visible="False" Width="340px">
                                    <div>
                                        <br />
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <asp:Label ID="Lbl_added" runat="server" CssClass="modal-body" Text="Requested!!!"></asp:Label>
                                        <br />
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;
                                        <br />
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;
                                        <asp:Button ID="btb_OK" runat="server" CssClass="btnme" OnClick="btb_OK_Click" Text="OK" />
                                        <br />
                                        <br />
                                    </div>
                                </asp:Panel>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="height: 100px">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
</asp:Content>

