﻿//This code allows the students who completed the course to request for certificate

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class CertificateRequest : System.Web.UI.Page
{
    connection c = new connection();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Login_Id"] == null)
                Response.Redirect("Home.aspx");

            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache,no-store,max-age=0,must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
        }
    }
   
    //Adds details of students into the Certificate table

    protected void btn_submit_Click(object sender, EventArgs e)
    {
        btn_submit_ModalPopupExtender.Show();
        c.getcon();
        Panel_Popup.Visible = true;
        string insert = "Insert into Certificate values('"+ddl_title.SelectedItem.Text+"','"+txt_name.Text+"','"+txt_Regno.Text+"','"+txt_Sem.Text+"','"+txt_colege.Text+"','"+txt_univ.Text+"','"+txt_course.Text+"','"+txt_project.Text+"','"+txt_from.Text+"','"+txt_to.Text+"')";
        SqlCommand cmd = new SqlCommand(insert, c.con);
        SqlDataAdapter da = new SqlDataAdapter();
        cmd.ExecuteNonQuery();
        c.con.Close();

    }
    protected void btb_OK_Click(object sender, EventArgs e)
    {
        Response.Redirect("CertificateRequest.aspx");
    }
}