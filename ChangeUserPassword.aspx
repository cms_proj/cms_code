﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StaffMaster.master" AutoEventWireup="true" CodeFile="ChangeUserPassword.aspx.cs" Inherits="ChangeUserPassword" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
     .modalBackground
    {
        background-color: Black;
        filter: alpha(opacity=60);
        opacity: 0.6;
    }
    .modalPopup
    {
        background-color: #FFFFFF;
        border: 3px solid #0DA9D0;
        border-radius: 12px;
        padding:0
        }
    .modalPopup .header
    {
        background-color: #2FBDF1;
        height: 30px;
        color: White;
        line-height: 30px;
        text-align: center;
        font-weight: bold;
        border-top-left-radius: 6px;
        border-top-right-radius: 6px;
    }
    .modalPopup .body
    {
        min-height: 50px;
        line-height: 30px;
        text-align: center;
        font-weight: bold;
    }
    .modalPopup .footer
    {
        padding: 6px;
    }
    .modalPopup .yes
    {
        background-color: #2FBDF1;
        border: 1px solid #0DA9D0;
    }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    
        <table class="nav-justified">
            <tr>
                <td style="height: 70px; width: 350px">&nbsp;</td>
                <td> <asp:Button ID ="ButDummy" runat="server" style =" display:none;" Text="Button"/></td>
                <cc1:ModalPopupExtender ID="btn_change_ModalPopupExtender" runat="server" DynamicServicePath="" Enabled="True" PopupControlId="Panel_Popup" RepositionMode="None" BackgroundCssClass="modalBackground" TargetControlID="btn_change">
            </cc1:ModalPopupExtender>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <asp:Panel ID="Panel1" runat="server" BorderStyle="Solid" Height="571px" Width="650px">
                        <table>
    <tr>
        <td colspan="3" style="font-size: 39px; font-weight: bold; color: #3366CC; font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif;" class="text-center">Reset Username and Password</td>
    </tr>
    <tr>
        <td style="width: 100px">&nbsp;</td>
        <td>&nbsp;</td>
        <td style="font-family: 'gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif; font-size: 20px; font-weight: bold">&nbsp;</td>
    </tr>
                            <tr>
                                <td style="width: 100px">&nbsp;</td>
                                <td>&nbsp;</td>
                                <td style="font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif; font-size: 20px; font-weight: normal; font-style: normal;">New Username</td>
                            </tr>
    <tr>
        <td>&nbsp;</td>
        <td style="font-size: 20px; font-weight: Bold">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </td>
        <td>
            <asp:TextBox ID="txt_newname" runat="server" CssClass="twitterStyleTextbox" Height="40px" Width="215px"></asp:TextBox>
            <br />
            <br />
        </td>
    </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td style="font-family: 'franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif; font-size: 20px; font-weight: normal; font-style: normal;">Old Password</td>
                            </tr>
    <tr>
        <td>&nbsp;</td>
        <td style="font-size: 20px; font-weight: bold">
            &nbsp;</td>
        <td>
            <asp:TextBox ID="txt_oldpwd" runat="server" CssClass="twitterStyleTextbox" Height="40px" Width="215px"></asp:TextBox>
            <asp:Label ID="lbl_oldpwd" runat="server" ForeColor="Red" Visible="False"></asp:Label>
            <br />
            <br />
        </td>
    </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td style="font-size: 20px; font-family: 'franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif; font-weight: normal; font-style: normal;">New Password</td>
                            </tr>
    <tr>
        <td>&nbsp;</td>
        <td style="font-size: 20px; font-weight: bold">
            &nbsp;&nbsp;&nbsp;&nbsp;
        </td>
        <td>
            <asp:TextBox ID="txt_newpwd" runat="server" CssClass="twitterStyleTextbox" ValidationGroup="eq" Height="40px" Width="215px" TextMode="Password"></asp:TextBox>
            <asp:RegularExpressionValidator ID="RegularExpressionValidatorPassword" runat="server" ControlToValidate="txt_newpwd" ErrorMessage="Password must contain: Minimum 8 characters atleast 1 Alphabet and 1 Number" ForeColor="Red" ValidationExpression="^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$" ValidationGroup="eq"></asp:RegularExpressionValidator>
            <br />
        </td>
    </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td style="font-size: 20px; font-weight: normal; font-family: 'franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif; font-style: normal;">Retype Password</td>
                            </tr>
    <tr>
        <td>&nbsp;</td>
        <td style="font-size: 20px; font-weight: bold">
            &nbsp;&nbsp;&nbsp;&nbsp;
        </td>
        <td>
            <asp:TextBox ID="txt_repwd" runat="server" CssClass="twitterStyleTextbox" Height="40px" Width="216px" TextMode="Password"></asp:TextBox>
            <asp:Label ID="lbl_retyped" runat="server" ForeColor="Red" Visible="False"></asp:Label>
        </td>
    </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td style="font-size: 20px; font-weight: bold">&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
    <tr>
        <td colspan="3">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="btn_cancel" runat="server" CssClass="btnme" OnClick="btn_cancel_Click" Text="Cancel" />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="btn_change" runat="server" CssClass="btnme" OnClick="btn_change_Click" Text="Change Password" />
            
            
        </td>
    </tr>
</table>
                    </asp:Panel>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td><asp:Panel ID="Panel_Popup" runat="server" BorderStyle="Solid" CssClass="modalPopup" Height="141px" Visible="False" Width="340px">
                                    <div>
                                        <br />
                                        <asp:Label ID="Lbl_added" runat="server" CssClass="modal-body" Text="Username and Password Changed Successfully!!" Font-Names="Aparajita" Font-Size="14pt"></asp:Label>
                                        <br />
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;
                                        <br />
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;
                                        <asp:Button ID="btb_OK" runat="server" CssClass="btnme" OnClick="btb_OK_Click" Text="OK" />
                                        <br />
                                        <br />
                                    </div>
                                </asp:Panel></td>
                <td>
                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td style="height: 100px;">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
    
    
        </form>
</asp:Content>
