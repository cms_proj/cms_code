﻿//This code is to change the username and password of employees

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class ChangeUserPassword : System.Web.UI.Page
{ 
    
    connection c = new connection();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Login_Id"] == null)
            Response.Redirect("Home.aspx");

        else
        {
            Response.ClearHeaders();
            Response.AddHeader("Cache-Control", "no-cache,no-store,max-age=0,must-revalidate");
            Response.AddHeader("Pragma", "no-cache");
        }
        this.UnobtrusiveValidationMode = System.Web.UI.UnobtrusiveValidationMode.None;
    }
    protected void btn_change_Click(object sender, EventArgs e)
    {
        c.getcon();
        int Id = Convert.ToInt32(Session["Login_Id"]);
        string user = "Select * from Login where Login_Id='" + Id + "'";
        SqlCommand cmd_user = new SqlCommand(user, c.con);
        SqlDataAdapter da_user = new SqlDataAdapter(cmd_user);
        DataTable dt_user = new DataTable();
        da_user.Fill(dt_user);
        if (dt_user.Rows.Count > 0)
        {
            String str_newuser = "update Login set Username='" + txt_newname.Text + "' where Login_Id='" + Id + "'";
            SqlCommand cmd_newuser = new SqlCommand(str_newuser, c.con);
            cmd_newuser.ExecuteNonQuery();
            string pass = dt_user.Rows[0][2].ToString();
            if (pass == txt_oldpwd.Text)
            {
                if (txt_newpwd.Text == txt_repwd.Text)
                {
                    String str_newpwd = "update Login set Password='" + txt_repwd.Text + "' where Login_Id='" + Id + "'";
                    SqlCommand cmd_newpwd = new SqlCommand(str_newpwd, c.con);
                    cmd_newpwd.ExecuteNonQuery();
                    Panel_Popup.Visible = true;
                    btn_change_ModalPopupExtender.Show();
                 //   Response.Write("<script>alert('Username and Password Changed Successfully');</script>");
                    lbl_retyped.Text = " ";
                    c.con.Close();
                }
                else
                {
                    lbl_retyped.Visible = true;
                    lbl_retyped.Text = "Retyped password not equal to new password";
                   
                }
            }
            else
            {
                lbl_oldpwd.Visible = true;
                lbl_oldpwd.Text = "Password Incorrect";
             
            }
                       
           
        }

   }

    protected void btn_cancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("StaffHome.aspx");
    }
    protected void btb_OK_Click(object sender, EventArgs e)
    {
        Response.Redirect("ChangeUserPassword.aspx");
    }
}