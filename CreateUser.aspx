﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMaster.master" AutoEventWireup="true" CodeFile="CreateUser.aspx.cs" Inherits="CreateUser" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    
    <style type="text/css">
     .modalBackground
    {
        background-color: Black;
        filter: alpha(opacity=60);
        opacity: 0.6;
    }
    .modalPopup
    {
        background-color: #FFFFFF;
        border: 3px solid #0DA9D0;
        border-radius: 12px;
        padding:0
        }
    .modalPopup .header
    {
        background-color: #2FBDF1;
        height: 30px;
        color: White;
        line-height: 30px;
        text-align: center;
        font-weight: bold;
        border-top-left-radius: 6px;
        border-top-right-radius: 6px;
    }
    .modalPopup .body
    {
        min-height: 50px;
        line-height: 30px;
        text-align: center;
        font-weight: bold;
    }
    .modalPopup .footer
    {
        padding: 6px;
    }
    .modalPopup .yes
    {
        background-color: #2FBDF1;
        border: 1px solid #0DA9D0;
            height: 23px;
        }
        .auto-style1 {
            height: 17px;
        }
        .auto-style2 {
            height: 20px;
        }
    </style>
    
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <table class="nav-justified">
        <tr>
            <td style="height: 100px" ></td>
            <td></td>
            <td ></td>
        </tr>
        <tr>
            <td ></td>
            <td >
                <asp:Panel ID="Panel1" runat="server" Height="673px" BorderStyle="Solid" Width="506px">
                    <table>
                        <tr>
                            <td class="text-center" colspan="3" style="font-weight: bold; font-size:39px; font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif; color: #000099;">&nbsp;</td>
                            
                        </tr>
                        <tr>
                            <td class="text-center" colspan="3" style="font-weight: bold; font-size:39px; font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif; color: #000099;">Staff Details </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td style="font-weight: bold; font-size: 14px;">Name</td>
                            <td>
                                <asp:Label ID="lbl_name" runat="server" Font-Size="13pt"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style1"></td>
                            <td style="font-weight: bold" class="auto-style1"></td>
                            <td class="auto-style1"></td>
                        </tr>
                        <tr>
                            <td class="auto-style2"></td>
                            <td style="font-weight: bold; font-size: 14px;" class="auto-style2">Gender</td>
                            <td class="auto-style2">
                                <asp:Label ID="lbl_gender" runat="server" Font-Size="13pt"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style1"></td>
                            <td class="auto-style1" style="font-weight: bold"></td>
                            <td class="auto-style1"></td>
                        </tr>
                        <tr>
                            <td class="auto-style1"></td>
                            <td class="auto-style1" style="font-weight: bold; font-size: 14px;">Date of Birth</td>
                            <td class="auto-style1">
                                <asp:Label ID="lbl_dob" runat="server" Font-Size="13pt"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td style="font-weight: bold">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td style="font-weight: bold; font-size: 14px;">Address</td>
                            <td>
                                <asp:Label ID="lbl_address" runat="server" Font-Size="13pt"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td style="font-weight: bold">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td style="font-weight: bold; font-size: 14px;">Contact No.</td>
                            <td>
                                <asp:Label ID="lbl_no" runat="server" Font-Size="13pt"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td style="font-weight: bold">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width: 100px"></td>
                            <td style="font-weight: bold; font-size: 14px;">E-mail</td>
                            <td>
                                <asp:Label ID="lbl_email" runat="server" Font-Size="13pt"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px">&nbsp;</td>
                            <td style="font-weight: bold">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td style="font-weight: bold; font-size: 14px;">Qualification</td>
                            <td>
                                <asp:Label ID="lbl_qual" runat="server" Font-Size="13pt"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td style="font-weight: bold">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td style="font-weight: bold; font-size: 14px;">Date of Joining</td>
                            <td>
                                <asp:Label ID="lbl_doj" runat="server" Font-Size="13pt"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td style="font-weight: bold">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td style="font-weight: bold; font-size: 14px;">Random User Name</td>
                            <td>
                                <asp:Label ID="lbl_usrname" runat="server" Font-Size="13pt"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td style="font-weight: bold">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td style="font-weight: bold; font-size: 14px;">Random Password</td>
                            <td>
                                <asp:Label ID="lbl_paswd" runat="server" Font-Bold="False" Font-Size="13pt"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td style="font-weight: bold">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td style="width: 200px; height: 50px;"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td colspan="2">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;<asp:Button ID ="ButDummy" runat="server" style =" display:none;" Text="Button"/><asp:Button ID="btn_create" runat="server" OnClick="btn_create_Click" Text="Create User" CssClass="btnme" />
                                <cc1:ModalPopupExtender ID="btn_create_ModalPopupExtender" runat="server" BackgroundCssClass="modalBackground" DynamicServicePath="" Enabled="True" PopupControlID="Panel_Popup" RepositionMode="None"  TargetControlID="ButDummy">
                                </cc1:ModalPopupExtender>
                            </td>
                        </tr>
    </table>
                </asp:Panel>
            </td>
            <td></td>
        </tr>
        <tr>
            <td style="width: 250px; height: 100px;"></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td style="width: 100px">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 100px">
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
            </td>
            <td><asp:Panel ID="Panel_Popup" runat="server" Height="125px" BorderStyle="Solid" CssClass="modalPopup" Width="255px">
                                                                    <div>
                                                                        <br />
                                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                        <asp:Label ID="Lbl_added" runat="server" CssClass="modal-body" Font-Bold="False" Font-Size="11pt" Text="User Created Succesfully!!!"></asp:Label>
                                                                        <br />
                                                                        <br />
                                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                        <asp:Button ID="btb_OK" runat="server" Text="OK" OnClick="btb_OK_Click" CssClass="btnme" />
                                                                        <br />
                                                                        <br />
                                                                        
                                                                    </div>
                                                                </asp:Panel></td>
            <td>&nbsp;</td>
        </tr>
    </table>
    
</asp:Content>

