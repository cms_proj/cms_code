﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StaffMaster.master" AutoEventWireup="true" CodeFile="Deallocation.aspx.cs" Inherits="Deallocation" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
              .modalBackground {
            background-color:black;
            filter:alpha(opacity = 40);
            opacity:0.4;
        }
        .modalPoPup {
            background-color: #d9eff8;
            border:3px solid #0DA9D0;
        }
        .modalPoPup .header {
            background-color: #2fBDF1;
            height: 30px;
            color: white;
            line-height: 30px;
            text-align: center;
            font-weight:bold;
        }
        .modalPoPup .footer {
            padding: 3px;
        }
        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <table>
        <tr>
            <td style="width: 100px">&nbsp;</td>
            <td>&nbsp; <asp:Button ID ="ButDummy" runat="server" style =" display:none;" Text="Button"/>&nbsp;
                <cc1:ModalPopupExtender ID="LinkButton1_ModalPopupExtender" runat="server" DynamicServicePath="" Enabled="True" TargetControlID="ButDummy" BackgroundCssClass ="modalBackground" 
                CancelControlID ="ButCancel" PopupControlID ="Panel1" RepositionMode="None">
                                    </cc1:ModalPopupExtender></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>

            <td><asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="User_Id" CellPadding="4" ForeColor="#333333" GridLines="None" Font-Bold="True" Height="184px" Width="552px">
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>
                            <asp:BoundField DataField="Name" HeaderText="UserName" />
                            <asp:BoundField DataField="System_Name" HeaderText="System" />
                            <asp:BoundField DataField="Schedule" HeaderText="TimeSchedule" />
                            <asp:BoundField DataField="From_Date" HeaderText="From Date" />
                            <asp:BoundField DataField="To_Date" HeaderText="To Date" />
                            <asp:BoundField DataField="Status" HeaderText="Status" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click">Edit</asp:LinkButton>
                                    
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EditRowStyle BackColor="#2461BF" />
                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                        <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 300px; height: 100px;"></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>
                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
            </td>
            <td></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                    <asp:Panel ID="Panel1" runat="server" Visible="False" CssClass="modalPoPup">
                        <div class="header"> Schedule Details</div>
                        <table>
                         
                            
                            <tr>
                                <td style="width: 40px">&nbsp;</td>
                                <td style="height: 30px;">Name</td>
                                <td>
                                    <asp:Label ID="lbl_Name" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td style="height: 30px;">Course</td>
                                <td>
                                    <asp:Label ID="lbl_pan_course" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 30px;">&nbsp;</td>
                                <td>Opted Technology</td>
                                <td>
                                    <asp:Label ID="lbl_pan_opt" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 30px;" >&nbsp;</td>
                                <td>Time Schedule</td>
                                <td>
                                    <asp:DropDownList ID="ddl_pan_time" runat="server" Height="16px" Width="109px" CssClass="search_categories">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 30px;">&nbsp;</td>
                                <td>Schedule Date</td>
                                <td>From&nbsp;&nbsp;
                                    <asp:TextBox ID="txt_fromdate" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                    <cc1:CalendarExtender ID="txt_fromdate_CalendarExtender" runat="server" Enabled="True" TargetControlID="txt_fromdate">
                                    </cc1:CalendarExtender>
                                    &nbsp;&nbsp; To&nbsp;&nbsp;
                                    <asp:TextBox ID="txt_todate" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                    <cc1:CalendarExtender ID="txt_todate_CalendarExtender" runat="server" Enabled="True" TargetControlID="txt_todate">
                                    </cc1:CalendarExtender>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 30px;">&nbsp;</td>
                                <td>System</td>
                                <td>
                                    <asp:Label ID="lbl_system" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 30px;">&nbsp;</td>
                                <td>System Status</td>
                                <td>
                                    <asp:DropDownList ID="ddl_pan_status" runat="server" CssClass="search_categories">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td >&nbsp;</td>
                                <td>&nbsp;</td>
                                <td >
                                    <asp:Button ID="ButOk" runat="server" Text="OK" OnClick="ButOk_Click " CssClass="btnme" />
                                    <cc1:RoundedCornersExtender ID="ButOk_RoundedCornersExtender" runat="server" Enabled="True" TargetControlID="ButOk">
                                    </cc1:RoundedCornersExtender>
                                    <asp:Button ID="ButCancel" runat="server" Text="Cancel" CssClass="btnme" />
                                </td>
                            </tr>
                        </table>
                        <div class="footer"></div>
                    </asp:Panel>
                </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </table>
</form>
</asp:Content>

