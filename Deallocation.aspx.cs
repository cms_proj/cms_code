﻿
//Code to Deallocate occupied labs and systems within a certain period of time.
//The user can deallocate when a student takes a break during the course peroid or discontinues the course

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

public partial class Deallocation : System.Web.UI.Page
{
    connection c = new connection();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Login_Id"] == null)
                Response.Redirect("Home.aspx");

            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache,no-store,max-age=0,must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
            c.getcon();
            gridbind();
            c.con.Close();
        }
    }

    //Binds the userschedule details into the grid

    public void gridbind()
    {
        string gb = "Select User_Id,Name,System_Name,Schedule,From_Date,To_Date, UserSchedule.Status from UserSchedule inner join Registration on UserSchedule.Reg_Id = Registration.Reg_Id inner join Enquiry on Enquiry.Enq_Id = Registration.Enq_Id inner join System on System.System_Id = UserSchedule.System_Id inner join TimeSchedule on TimeSchedule.TimeSch_Id = UserSchedule.TimeSch_Id ";
        SqlCommand cmd = new SqlCommand(gb, c.con);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        c.getcon();

        // Binds Time Schedule to the dropdownlist

        string time = "Select * from TimeSchedule";
        SqlCommand cmdtime = new SqlCommand(time, c.con);
        DataTable timedt = new DataTable();
        SqlDataAdapter timeda = new SqlDataAdapter(cmdtime);
        timeda.Fill(timedt);
        if (timedt.Rows.Count > 0)
        {
            ddl_pan_time.DataSource = timedt;
            ddl_pan_time.DataValueField = "TimeSch_Id";
            ddl_pan_time.DataTextField = "Schedule";
            ddl_pan_time.DataBind();
        }
        //   ddl_pan_time.Items.Insert(0, "Select");
        ddl_pan_status.Items.Add("Allocated");
        ddl_pan_status.Items.Add("Free");
        LinkButton1_ModalPopupExtender.Show();
        LinkButton lk = (LinkButton)sender;
        GridViewRow gw = lk.NamingContainer as GridViewRow;
        int id = Convert.ToInt32(GridView1.DataKeys[gw.RowIndex].Values[0].ToString());
        string name = "Select Name,Course,Tech,From_Date,To_Date,System_Name,Schedule,UserSchedule.Status,User_Id from UserSchedule inner join Registration on UserSchedule.Reg_Id = Registration.Reg_Id inner join Enquiry on Enquiry.Enq_Id = Registration.Enq_Id inner join OptedTech on Registration.OpTech_Id = OptedTech.OpTech_Id inner join Course on OptedTech.Course_Id = Course.Course_Id inner join System on System.System_Id = UserSchedule.System_Id inner join Technology on Technology.Tech_Id = OptedTech.Tech_Id inner join TimeSchedule on TimeSchedule.TimeSch_Id = UserSchedule.TimeSch_Id where UserSchedule.User_Id= '" + id + "'";
        SqlCommand cmd = new SqlCommand(name, c.con);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            Panel1.Visible = true;
            lbl_Name.Text = dt.Rows[0][0].ToString();
            lbl_pan_course.Text = dt.Rows[0][1].ToString();
            lbl_pan_opt.Text = dt.Rows[0][2].ToString();
            txt_fromdate.Text = dt.Rows[0][3].ToString();
            txt_todate.Text = dt.Rows[0][4].ToString();
            lbl_system.Text = dt.Rows[0][5].ToString();
            ddl_pan_time.Items.FindByText(dt.Rows[0][6].ToString()).Selected = true;
            ddl_pan_status.Items.FindByText(dt.Rows[0][7].ToString()).Selected = true;
            Session["User_Id"] = dt.Rows[0][8].ToString();


        }
        
        c.con.Close();
    }
    
    //To save the updated details

    protected void ButOk_Click(object sender, EventArgs e)
    {
        c.getcon();
        int Id = Convert.ToInt32(Session["User_Id"]);
        string update = "Update UserSchedule set Status='" + ddl_pan_status.SelectedItem.Value + "',TimeSch_Id='" + ddl_pan_time.SelectedItem.Value + "',From_Date='" + txt_fromdate.Text + "',To_Date='" + txt_todate.Text + "'where User_Id='"+Id+"'";
        SqlCommand cmd = new SqlCommand(update, c.con);
        cmd.ExecuteNonQuery();
        c.con.Close();
        gridbind();
    }
}