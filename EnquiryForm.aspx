﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StaffMaster.master" AutoEventWireup="true" CodeFile="EnquiryForm.aspx.cs" Inherits="EnquiryForm" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <title></title>
    <style type="text/css">
     
        .modalBackground
    {
        background-color: Black;
        filter: alpha(opacity=60);
        opacity: 0.6;
    }
    .modalPopup
    {
        background-color: #FFFFFF;
        border: 3px solid #0DA9D0;
        border-radius: 12px;
        padding:0
        }
    .modalPopup .header
    {
        background-color: #2FBDF1;
        height: 30px;
        color: White;
        line-height: 30px;
        text-align: center;
        font-weight: bold;
        border-top-left-radius: 6px;
        border-top-right-radius: 6px;
    }
    .modalPopup .body
    {
        min-height: 50px;
        line-height: 30px;
        text-align: center;
        font-weight: bold;
    }
    .modalPopup .footer
    {
        padding: 6px;
    }
    .modalPopup .yes
    {
        background-color: #2FBDF1;
        border: 1px solid #0DA9D0;
    }
       /*.modalBackground {
            background-color:black;
            filter:alpha(opacity = 40);
            opacity:0.4;
        }
        .modalPoPup {
            background-color: #d9eff8;
            border:3px solid #0DA9D0;
        }
        .modalPoPup .header {
            background-color: #2fBDF1;
            height: 30px;
            color: white;
            line-height: 30px;
            text-align: center;
            font-weight:bold;
        }
        .modalPoPup .footer {
            padding: 3px;
        }*/
       
        
               
        
       
        
               
        </style>
    <link href="assets/css/style.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1">
     <div>    
         <table>
               <tr>
                <td>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:CRMConnectionString %>" SelectCommand="SELECT * FROM [Enquiry]"></asp:SqlDataSource>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:MultiView ID="MultiView1" runat="server">
                        <table>
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:View ID="View1" runat="server">
                                                    
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    
                                                    <table>
                                                        <caption>
                                                            <tr>
                                                            <td 200px"="" width:="">
                                                                &nbsp;</td>
                                                                <td 200px"="" width:="" style="width: 200px; height: 40px">&nbsp;</td>
                                                                <tr>
                                                                    <td style="width: 100px">&nbsp;</td>
                                                                    <td style="width: 100px">&nbsp;</td>
                                                                    <td>
                                                                        <asp:Panel ID="Panel1" runat="server" BackColor="White" BorderColor="#333300" BorderStyle="Solid" Height="829px" Width="788px">
                                                                            <table style="border: medium hidden #000080; padding: 2px; margin: 2px; top: 40px; bottom: 40px;">
                                                                                <tr>
                                                                                    <td>&nbsp;</td>
                                                                                    <td>&nbsp;</td>
                                                                                    <td colspan="4"></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 30px">&nbsp;</td>
                                                                                    <td style="width: 30px">&nbsp;</td>
                                                                                    <td colspan="4">&nbsp;</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="text-center" colspan="6" style="font-size: 39px; font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif; font-weight: bold; color: #000099;">ENQUIRY FORM</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="text-center" colspan="6" style="font-size: 30px; font-weight: bold; color: #666666;">Personal Details</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>&nbsp;</td>
                                                                                    <td>&nbsp;</td>
                                                                                    <td>&nbsp;</td>
                                                                                    <td>&nbsp;</td>
                                                                                    <td>&nbsp;</td>
                                                                                    <td>&nbsp;</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>&nbsp;</td>
                                                                                    <td>&nbsp;</td>
                                                                                    <td class="text-left" style="font-size: 15px; font-weight: bold">Name</td>
                                                                                    <td class="text-right">
                                                                                        <asp:TextBox ID="txt_name" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txt_name" ErrorMessage="*Name can contain only letters and White Spaces" ForeColor="Red" ValidationExpression="[a-zA-Z'.\s]{1,50}" ValidationGroup="eq"></asp:RegularExpressionValidator>
                                                                                    </td>
                                                                                    <td></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>&nbsp;</td>
                                                                                    <td>&nbsp;</td>
                                                                                    <td class="text-left">&nbsp;</td>
                                                                                    <td class="text-right">&nbsp;</td>
                                                                                    <td>&nbsp;</td>
                                                                                    <td>&nbsp;</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>&nbsp;</td>
                                                                                    <td>&nbsp;</td>
                                                                                    <td class="text-left" style="font-size: 15px; font-weight: bold">Gender</td>
                                                                                    <td>
                                                                                        <asp:RadioButtonList ID="rbl_gen" runat="server" Font-Size="10pt" RepeatDirection="Horizontal">
                                                                                            <asp:ListItem>Male</asp:ListItem>
                                                                                            <asp:ListItem>Female</asp:ListItem>
                                                                                        </asp:RadioButtonList>
                                                                                    </td>
                                                                                    <td  class="text-left" style="font-weight: bold; font-size: 15px">Age&nbsp;
                                                                                        <asp:TextBox ID="txt_age" runat="server" CssClass="twitterStyleTextbox" style="margin-left: 0px" Width="54px"></asp:TextBox>
                                                                                       </td>
                                                                                    <td style="width: 10px">&nbsp;</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>&nbsp;</td>
                                                                                    <td>&nbsp;</td>
                                                                                    <td class="text-left">&nbsp;</td>
                                                                                    <td class="text-right">&nbsp;</td>
                                                                                    <td class="text-left">&nbsp;</td>
                                                                                    <td style="width: 10px">&nbsp;</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="font-weight: 700">&nbsp;</td>
                                                                                    <td style="font-weight: 700">&nbsp;</td>
                                                                                    <td  class="text-left" colspan="2" style="font-size: 15px; font-weight: bold">Address for Communication</td>
                                                                                    <td>&nbsp;</td>
                                                                                    <td>&nbsp;</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="font-weight: 700"></td>
                                                                                    <td style="font-weight: 700"></td>
                                                                                    <td colspan="2"></td>
                                                                                    <td ></td>
                                                                                    <td ></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>&nbsp;</td>
                                                                                    <td>&nbsp;</td>
                                                                                    <td class="text-right" style="font-size: 14px; font-weight: bold">House No<strong> &nbsp;&nbsp;</strong></td>
                                                                                    <td  class="text-right">
                                                                                        <asp:TextBox ID="txt_house" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                                                                    </td>
                                                                                    <td>&nbsp;</td>
                                                                                    <td>&nbsp;</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>&nbsp;</td>
                                                                                    <td>&nbsp;</td>
                                                                                    <td style="width: 100px">&nbsp;</td>
                                                                                    <td>&nbsp;</td>
                                                                                    <td>&nbsp;</td>
                                                                                    <td>&nbsp;</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>&nbsp;</td>
                                                                                    <td>&nbsp;</td>
                                                                                    <td class="text-right" style="font-size: 14px; font-weight: bold">PO&nbsp; &nbsp;&nbsp;</td>
                                                                                    <td class="text-right">
                                                                                        <asp:TextBox ID="txt_PO" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                                                                    </td>
                                                                                    <td>&nbsp;</td>
                                                                                    <td>&nbsp;</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>&nbsp;</td>
                                                                                    <td>&nbsp;</td>
                                                                                    <td>&nbsp;</td>
                                                                                    <td>&nbsp;</td>
                                                                                    <td>&nbsp;</td>
                                                                                    <td>&nbsp;</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>&nbsp;</td>
                                                                                    <td>&nbsp;</td>
                                                                                    <td class="text-right" style="font-weight: bold; font-size: 14px;">Town/City&nbsp;&nbsp; &nbsp;</td>
                                                                                    <td  class="text-right">
                                                                                        <asp:TextBox ID="txt_twn" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                                                                    </td>
                                                                                    <td>&nbsp;</td>
                                                                                    <td></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>&nbsp;</td>
                                                                                    <td>&nbsp;</td>
                                                                                    <td>&nbsp;</td>
                                                                                    <td>&nbsp;</td>
                                                                                    <td>&nbsp;</td>
                                                                                    <td>&nbsp;</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>&nbsp;</td>
                                                                                    <td>&nbsp;</td>
                                                                                    <td class="text-right" style="font-weight: bold; font-size: 15px">&nbsp;Pincode&nbsp;&nbsp; &nbsp;</td>
                                                                                    <td  class="text-right">
                                                                                        <asp:TextBox ID="txt_Pin" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                                                                    </td>
                                                                                    <td>&nbsp;</td>
                                                                                    <td>&nbsp;</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td ></td>
                                                                                    <td ></td>
                                                                                    <td ></td>
                                                                                    <td ></td>
                                                                                    <td ></td>
                                                                                    <td ></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td >&nbsp;</td>
                                                                                    <td >&nbsp;</td>
                                                                                    <td style="width: 150px">&nbsp;</td>
                                                                                    <td >&nbsp;</td>
                                                                                    <td >&nbsp;</td>
                                                                                    <td >&nbsp;</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>&nbsp;</td>
                                                                                    <td>&nbsp;</td>
                                                                                    <td  class="text-left" style="font-size: 15px; font-weight: bold">Telephone(Res)</td>
                                                                                    <td  class="text-right">
                                                                                        <asp:TextBox ID="txt_res" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                                                                        </td>
                                                                                    <td>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txt_res" ErrorMessage="*Required" ForeColor="Red" ValidationGroup="eq"></asp:RequiredFieldValidator>
                                                                                    </td>
                                                                                    <td>&nbsp;</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>&nbsp;</td>
                                                                                    <td>&nbsp;</td>
                                                                                    <td>&nbsp;</td>
                                                                                    <td>&nbsp;</td>
                                                                                    <td>&nbsp;</td>
                                                                                    <td>&nbsp;</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>&nbsp;</td>
                                                                                    <td>&nbsp;</td>
                                                                                    <td  class="text-left" style="font-size: 15px; font-weight: bold">Mobile</td>
                                                                                    <td  class="text-right">
                                                                                        <asp:TextBox ID="txt_mob" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                                                                        </td>
                                                                                    <td>
                                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txt_mob" ErrorMessage="Please enter a 10 digit number" ForeColor="Red" ValidationExpression="[0-9]{10,12}" ValidationGroup="eq"></asp:RegularExpressionValidator>
                                                                                    </td>
                                                                                    <td>&nbsp;</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>&nbsp;</td>
                                                                                    <td>&nbsp;</td>
                                                                                    <td class="text-left">&nbsp;</td>
                                                                                    <td>&nbsp;</td>
                                                                                    <td>&nbsp;</td>
                                                                                    <td>&nbsp;</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 100px">&nbsp;</td>
                                                                                    <td style="width: 100px">&nbsp;</td>
                                                                                    <td class="text-left" style="font-size: 15px; font-weight: bold">E-mail</td>
                                                                                    <td  class="text-right">
                                                                                        <asp:TextBox ID="txtEmail" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                                                                    </td>
                                                                                    <td>
                                                                                        <asp:RequiredFieldValidator ID="rfvEmail0" runat="server" ControlToValidate="txtEmail" ErrorMessage="*" ForeColor="Red" ValidationGroup="eq"></asp:RequiredFieldValidator>
                                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtEmail" CssClass="requiredFieldValidateStyle" ErrorMessage="Please Enter Valid Email ID" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="eq">
                  </asp:RegularExpressionValidator>
                                                                                    </td>
                                                                                    <td>&nbsp;</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style="width: 100px">&nbsp;</td>
                                                                                    <td>&nbsp;</td>
                                                                                    <td>&nbsp;</td>
                                                                                    <td>&nbsp;</td>
                                                                                    <td>&nbsp;</td>
                                                                                    <td>&nbsp;</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                        <asp:Button ID="btn_next" runat="server" OnClick="btn_next_Click" style="margin-left: 0px; text-align: center;" Text="Next" ValidationGroup="eq" CssClass="btnme"/>
                                                                                        &nbsp;</td>
                                                                                </tr>
                                                                            </table>
                                                                        </asp:Panel>
                                                                    </td>
                                                                    <td>&nbsp;</td>
                                                                    <td>&nbsp;</td>
                                                                </tr>
                                                                </td">
                                                        </tr>
                                                        </caption>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 70px">&nbsp;</td>
                                                            <td style="width: 70px">&nbsp;</td>
                                                            <td style="width: 70px">&nbsp;</td>
                                                            <td style="width: 70px; height: 100px;">&nbsp;</td>
                                                            <td>&nbsp;</td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                 </asp:View>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:View ID="View2" runat="server">
                                                    
                                                    <table>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td style="width: 300px; height: 70px">&nbsp;</td>
                                                            <td>
                                                            </td>
                                                            <td >&nbsp;</td>
                                                            <td >&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td>
                                                                <asp:Panel ID="Panel2" runat="server" BorderStyle="Solid" Height="802px" Width="842px">
                                                                    <table>
                                                                        <tr>
                                                                            <td class="text-center" colspan="8" style="font-size: 30px; font-weight: bold; color: #666666;">&nbsp;</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="text-center" colspan="8" style="font-size: 39px; font-weight: bold; color: #0000CC;">Educational Qualification</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="text-center" colspan="8" style="font-size: 30px; font-weight: bold; color: #0000CC;">&nbsp;</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="text-center" colspan="8">&nbsp;</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>&nbsp;</td>
                                                                            <td>&nbsp;</td>
                                                                            <td style="font-size: 15px; font-weight: bold">Educational Qualification</td>
                                                                            <td style="font-size: 15px; font-weight: bold">&nbsp;</td>
                                                                            <td>
                                                                                <asp:DropDownList ID="ddl_Qual" runat="server" CssClass="search_categories">
                                                                                </asp:DropDownList>
                                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                <asp:Label ID="Label1" runat="server" ForeColor="Red" Text="Select" Visible="False"></asp:Label>
                                                                            </td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td>&nbsp;</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td>&nbsp;</td>
                                                                            <td></td>
                                                                            <td style="height: 20px"></td>
                                                                            <td></td>
                                                                            <td style="width: 300px"></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td></td>
                                                                            <td>&nbsp;</td>
                                                                            <td style="font-size: 15px; font-weight: bold">College</td>
                                                                            <td style="font-size: 15px; font-weight: bold">&nbsp;</td>
                                                                            <td>
                                                                                <asp:DropDownList ID="ddl_Colg" runat="server" AutoPostBack="True" CssClass="search_categories" OnSelectedIndexChanged="ddl_Colg_SelectedIndexChanged">
                                                                                </asp:DropDownList>
                                                                                <asp:Panel ID="Panel_addColg" runat="server" Height="103px" Visible="False" Width="254px">
                                                                                    <table>
                                                                                        <tr>
                                                                                            <td>
                                                                                                &nbsp;</td>
                                                                                            <td>
                                                                                                &nbsp;</td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="lbl_addcolg" runat="server" Font-Bold="True" Text="Enter College"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:TextBox ID="txt_addColg" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 100px">&nbsp;</td>
                                                                                            <td>&nbsp;</td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <asp:Label ID="lbl_addUniv" runat="server" Font-Bold="True" Text="Enter University"></asp:Label>
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:TextBox ID="txt_adduniv" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td style="width: 50px">&nbsp;</td>
                                                                                            <td>&nbsp;</td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </asp:Panel>
                                                                            </td>
                                                                            <td></td>
                                                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                <asp:Label ID="Label_university" runat="server" Font-Bold="True" Text="University :"></asp:Label>
                                                                            </td>
                                                                            <td>&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                <asp:Label ID="lbl_univ" runat="server"></asp:Label>
                                                                            </td>
                                                                            <td>&nbsp;</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>&nbsp;</td>
                                                                            <td>&nbsp;</td>
                                                                            <td>&nbsp;</td>
                                                                            <td>&nbsp;</td>
                                                                            <td style="height: 30px">&nbsp;</td>
                                                                            <td>&nbsp;</td>
                                                                            <td>&nbsp;</td>
                                                                            <td>&nbsp;</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td style="font-size: 15px; font-weight: bold">IT Skills</td>
                                                                            <td style="font-size: 15px; font-weight: bold">&nbsp;</td>
                                                                            <td>
                                                                                <asp:DropDownList ID="ddl_skill" runat="server" CssClass="search_categories">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td style="width: 100px"></td>
                                                                            <td></td>
                                                                            <td>&nbsp;</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>&nbsp;</td>
                                                                            <td>&nbsp;</td>
                                                                            <td>&nbsp;</td>
                                                                            <td>&nbsp;</td>
                                                                            <td>&nbsp;</td>
                                                                            <td style="height: 30px">&nbsp;</td>
                                                                            <td>&nbsp;</td>
                                                                            <td>&nbsp;</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>&nbsp;</td>
                                                                            <td>&nbsp;</td>
                                                                            <td style="font-size: 15px; font-weight: bold">How do you know about CCS Training programs?(Check anyone)</td>
                                                                            <td style="font-size: 15px; font-weight: bold">&nbsp;</td>
                                                                            <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                <asp:CheckBoxList ID="cbl_source" runat="server" BorderStyle="None" CellPadding="0" CssClass="twitterStyleTextbox" Font-Bold="False" Font-Italic="True" Font-Size="10pt" RepeatColumns="3" RepeatDirection="Horizontal" TabIndex="7">
                                                                                </asp:CheckBoxList>
                                                                            </td>
                                                                            <td colspan="2">&nbsp;</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>&nbsp;</td>
                                                                            <td>&nbsp;</td>
                                                                            <td>&nbsp;</td>
                                                                            <td>&nbsp;</td>
                                                                            <td>&nbsp;</td>
                                                                            <td style="height: 30px">&nbsp;</td>
                                                                            <td>&nbsp;</td>
                                                                            <td>&nbsp;</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>&nbsp;</td>
                                                                            <td>&nbsp;</td>
                                                                            <%--<td>If you are reffered by any CCS or Malayala Manorama employee, please tick to specify :</td>
                                                            <td>
                                                                <asp:CheckBox ID="CheckBox1" runat="server" OnCheckedChanged="CheckBox1_CheckedChanged" />
                                                            </td>--%>
                                                                            <td></td>
                                                                            <td>&nbsp;</td>
                                                                            <td></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td>
                                                                                <asp:Label ID="lbl_refname" runat="server" Font-Bold="True" Font-Size="13pt" Text="Name" Visible="False"></asp:Label>
                                                                            </td>
                                                                            <td>&nbsp;</td>
                                                                            <td>
                                                                                <asp:TextBox ID="txt_refname" runat="server" Visible="False"></asp:TextBox>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="lbl_refDes" runat="server" Font-Bold="True" Font-Size="13pt" Text="Designation" Visible="False"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txt_desg" runat="server" Visible="False"></asp:TextBox>
                                                                            </td>
                                                                            <td>&nbsp;</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td style="font-size: 15px; font-weight: bold">Technologies interested</td>
                                                                            <td style="font-size: 15px; font-weight: bold">&nbsp;</td>
                                                                            <td>
                                                                                <asp:DropDownList ID="ddl_Tech" runat="server" CssClass="search_categories">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td>&nbsp;</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>&nbsp;</td>
                                                                            <td>&nbsp;</td>
                                                                            <td>&nbsp;</td>
                                                                            <td style="width: 50px">&nbsp;</td>
                                                                            <td>&nbsp;</td>
                                                                            <td>&nbsp;</td>
                                                                            <td>&nbsp;</td>
                                                                            <td>&nbsp;</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td colspan="5">
                                                                                <asp:Button ID="btn_submit" runat="server" CssClass="btnme" OnClick="btn_submit_Click" style="margin-left: 346px" Text="Submit" />
                                                                                <cc1:ModalPopupExtender ID="btn_submit_ModalPopupExtender" runat="server" BackgroundCssClass="modalBackground" DynamicServicePath="" Enabled="True" PopupControlID="Panel_Popup" RepositionMode="None" TargetControlID="btn_submit">
                                                                                </cc1:ModalPopupExtender>
                                                                            </td>
                                                                            <td></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>&nbsp;</td>
                                                                            <td style="width: 100px">&nbsp;</td>
                                                                            <td>
                                                                                <asp:ScriptManager ID="ScriptManager1" runat="server">
                                                                                </asp:ScriptManager>
                                                                            </td>
                                                                            <td>&nbsp;</td>
                                                                            <td>
                                                                                <asp:Panel ID="Panel_Popup" runat="server" BorderStyle="Solid" CssClass="modalPopup" Height="132px" Visible="False" Width="280px">
                                                                                    <div>
                                                                                        <br />
                                                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                        <asp:Label ID="Lbl_added" runat="server" CssClass="modal-body" Font-Bold="False" Font-Size="11pt" Text="Added Successfully!!!"></asp:Label>
                                                                                        <br />
                                                                                        <br />
                                                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                        <asp:Button ID="btb_OK" runat="server" CssClass="btnme" OnClick="btb_OK_Click" Text="OK" />
                                                                                        <br />
                                                                                        <br />
                                                                                    </div>
                                                                                </asp:Panel>
                                                                            </td>
                                                                            <td></td>
                                                                            <td></td>
                                                                            <td style="width: 30px">&nbsp;</td>
                                                                        </tr>
                                                                    </table>
                                                                </asp:Panel>
                                                            </td>
                                                            <td></td>
                                                            <td style="width: 30px"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td style="width: 100px">&nbsp;</td>
                                                            <td>&nbsp;</td>
                                                            <td>&nbsp;</td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                    
                                                </asp:View>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </asp:MultiView>                    
        </table>
    
    </div>
  </form>
    </asp:Content>
