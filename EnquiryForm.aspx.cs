﻿//Enquiry form to be filled by the student who came to know about the course

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;


public partial class EnquiryForm : System.Web.UI.Page
{
    connection c = new connection();

    protected void Page_Load(object sender, EventArgs e)
    {
        this.UnobtrusiveValidationMode = System.Web.UI.UnobtrusiveValidationMode.None;
        if (!IsPostBack)
        {
            if (Session["Login_Id"] == null)
                Response.Redirect("Home.aspx");

            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache,no-store,max-age=0,must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
            MultiView1.ActiveViewIndex = 0;
        }
    }

    
    protected void btn_next_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 1;
        Label1.Visible = false;
        
        //Binds Educational Qualification to dropdownlist
        c.getcon();
        string edu = "Select * from Qualification";
        SqlCommand cmdedu = new SqlCommand(edu, c.con);
        DataTable edudt = new DataTable();
        SqlDataAdapter eduda = new SqlDataAdapter(cmdedu);
        eduda.Fill(edudt);
        if (edudt.Rows.Count > 0)
        {
            ddl_Qual.DataSource = edudt;
            ddl_Qual.DataValueField = "Qual_Id";
            ddl_Qual.DataTextField = "Qual";
            ddl_Qual.DataBind();
        }
        ddl_Qual.Items.Insert(0, "Select");
        c.con.Close();

        //Binds College to dropdownlist
        c.getcon();
        string col = "Select * from College Order by College";
        SqlCommand cmdcol = new SqlCommand(col, c.con);
        DataTable coldt = new DataTable();
        SqlDataAdapter colda = new SqlDataAdapter(cmdcol);
        colda.Fill(coldt);
        if (coldt.Rows.Count > 0)
        {
            ddl_Colg.DataSource = coldt;
            ddl_Colg.DataValueField = "Colg_Id";
            ddl_Colg.DataTextField = "College";
            ddl_Colg.DataBind();
        }
        ddl_Colg.Items.Insert(0, "Select");
        ddl_Colg.Items.Add("Others");
        c.con.Close();

        //Binds Skills to  dropdownlist
        c.getcon();
        string sk = "Select * from Skills";
        SqlCommand cmdsk = new SqlCommand(sk, c.con);
        DataTable skdt = new DataTable();
        SqlDataAdapter skda = new SqlDataAdapter(cmdsk);
        skda.Fill(skdt);
        if (skdt.Rows.Count > 0)
        {
            ddl_skill.DataSource = skdt;
            ddl_skill.DataValueField = "Skill_Id";
            ddl_skill.DataTextField = "Skill";
            ddl_skill.DataBind();
        }
        ddl_skill.Items.Insert(0, "Select");
        c.con.Close();
       
        //Binds Technologies to dropdownlist
        c.getcon();
        string tec = "Select * from Technology";
        SqlCommand cmdtec=new SqlCommand(tec,c.con);
        DataTable tecdt=new DataTable();
        SqlDataAdapter techda=new SqlDataAdapter(cmdtec);
        techda.Fill(tecdt);
        if(tecdt.Rows.Count>0)
        {
            ddl_Tech.DataSource=tecdt;
            ddl_Tech.DataValueField="Tech_Id";
            ddl_Tech.DataTextField="Tech";
            ddl_Tech.DataBind();
        }
        ddl_Tech.Items.Insert(0,"Select");
        c.con.Close();

        //Binds Source to checkbox list
        c.getcon();
        string sou="Select * from Source";
        SqlCommand cmdsou= new SqlCommand(sou,c.con);
        DataTable soudt = new DataTable();
        SqlDataAdapter souda = new SqlDataAdapter(cmdsou);
        souda.Fill(soudt);
        if(soudt.Rows.Count>0)
        {
            cbl_source.DataSource =soudt;
            cbl_source.DataValueField="So_Id";
            cbl_source.DataTextField="Source";
            cbl_source.DataBind();
        }
        c.con.Close();
    }
// Adds the details of students to table
    protected void btn_submit_Click(object sender, EventArgs e)
    {
        if (ddl_Qual.SelectedIndex > 0)
        {

          //Adds college and university if it doesnotexists in table
          if (ddl_Colg.SelectedItem.Text == "Others")
           {
             Label1.Visible = false;
             c.getcon();
             string uni = "Select Uni_Id from University where Uni like '"+txt_adduniv.Text+ "'";
             SqlCommand cmduni = new SqlCommand(uni, c.con);
             DataTable unidt = new DataTable();
             SqlDataAdapter unida = new SqlDataAdapter(cmduni);
             unida.Fill(unidt);
             if (unidt.Rows.Count > 0)
             {
                int uniid = Convert.ToInt32(unidt.Rows[0][0]);
                string s_colg = "Insert into College values( '" + txt_addColg.Text + "','" + uniid + "')Select @@Identity";
                SqlCommand cmd_colg = new SqlCommand(s_colg, c.con);
                object o_colg = cmd_colg.ExecuteScalar();
                int colgid = Convert.ToInt32(o_colg);
                string s = "Insert into Enquiry values( '" + txt_name.Text + "','" + rbl_gen.SelectedItem.Text + "','" + txt_age.Text + "','" + txt_house.Text + "','" + txt_PO.Text + "','" + txt_twn.Text + "','" + txt_Pin.Text + "','" + txt_res.Text + "','" + txt_mob.Text + "','" + txtEmail.Text + "','" + ddl_Qual.SelectedItem.Value + "','" + colgid + "','" + ddl_skill.SelectedItem.Value + "','" + cbl_source.SelectedItem.Value + "','" + txt_refname.Text + "','" + txt_desg.Text + "','" + ddl_Tech.SelectedItem.Value + "','Not Registered')";
                SqlCommand cmd = new SqlCommand(s, c.con);
                cmd.ExecuteNonQuery();
            }
            else
            {
                string s_uni = "Insert into University values( '" + txt_adduniv.Text + "')Select @@Identity";
                SqlCommand cmd_uni = new SqlCommand(s_uni, c.con);
                object o_uni = cmd_uni.ExecuteScalar();
                int uniid = Convert.ToInt32(o_uni);
                string s_colg = "Insert into College values( '" + txt_addColg.Text + "','" + uniid + "')Select @@Identity";
                SqlCommand cmd_colg = new SqlCommand(s_colg, c.con);
                object o_colg = cmd_colg.ExecuteScalar();
                int colgid = Convert.ToInt32(o_colg);
                string s = "Insert into Enquiry values( '" + txt_name.Text + "','" + rbl_gen.SelectedItem.Text + "','" + txt_age.Text + "','" + txt_house.Text + "','" + txt_PO.Text + "','" + txt_twn.Text + "','" + txt_Pin.Text + "','" + txt_res.Text + "','" + txt_mob.Text + "','" + txtEmail.Text + "','" + ddl_Qual.SelectedItem.Value + "','" + colgid + "','" + ddl_skill.SelectedItem.Value + "','" + cbl_source.SelectedItem.Value + "','" + txt_refname.Text + "','" + txt_desg.Text + "','" + ddl_Tech.SelectedItem.Value + "')";
                SqlCommand cmd = new SqlCommand(s, c.con);
                cmd.ExecuteNonQuery();
            }
               Panel_Popup.Visible = true;
               btn_submit_ModalPopupExtender.Show();
               //Response.Write("<script>alert('Added Successfully')</script>");
               c.con.Close();
                

            }
            else
            {
                Label1.Visible = false;
                c.getcon();
                string s = "Insert into Enquiry values( '" + txt_name.Text + "','" + rbl_gen.SelectedItem.Text + "','" + txt_age.Text + "','" + txt_house.Text + "','" + txt_PO.Text + "','" + txt_twn.Text + "','" + txt_Pin.Text + "','" + txt_res.Text + "','" + txt_mob.Text + "','" + txtEmail.Text + "','" + ddl_Qual.SelectedItem.Value + "','" + ddl_Colg.SelectedItem.Value + "','" + ddl_skill.SelectedItem.Value + "','" + cbl_source.SelectedItem.Value + "','" + txt_refname.Text + "','" + txt_desg.Text + "','" + ddl_Tech.SelectedItem.Value + "','Not Registered')";
                SqlCommand cmd = new SqlCommand(s, c.con);
                cmd.ExecuteNonQuery();
                Panel_Popup.Visible = true;
                btn_submit_ModalPopupExtender.Show();
                //Response.Write("<script>alert('Added Successfully')</script>");
                c.con.Close();
            }
        }
        else
        {
            Label1.Visible = true;         
        }
    }

    protected void ddl_Colg_SelectedIndexChanged(object sender, EventArgs e)
    {
        c.getcon();
        if (Convert.ToString(ddl_Colg.SelectedItem.Text) == "Others")
        {
            Panel_addColg.Visible = true;
            lbl_univ.Visible = false;
            Label_university.Visible = false;


        }
        else
        {
            string uni = "Select Uni from College c inner join University u on c.Uni_Id=u.Uni_Id where c.Colg_Id='" + ddl_Colg.SelectedItem.Value + "'";
            SqlCommand cmduni = new SqlCommand(uni, c.con);
            DataTable unidt = new DataTable();
            SqlDataAdapter unida = new SqlDataAdapter(cmduni);
            unida.Fill(unidt);
            if (unidt.Rows.Count > 0)
            {

                lbl_univ.Text = unidt.Rows[0][0].ToString();
                //ddl_univ.Items.FindByText(dt.Rows[0][0].ToString()).Selected = true;
            }

            c.con.Close();
        }
    }
protected void CheckBox1_CheckedChanged(object sender, EventArgs e)
{
    lbl_refname.Visible = true;
    lbl_refDes.Visible = true;
    txt_refname.Visible = true;
    txt_desg.Visible = true;
}
protected void btb_OK_Click(object sender, EventArgs e)
{
    Response.Redirect("EnquiryForm.aspx");
}
}

