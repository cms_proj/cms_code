﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StaffMaster.master" AutoEventWireup="true" CodeFile="Followup.aspx.cs" Inherits="Followup" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
      <title></title>
    <style type="text/css">
           .modalBackground
    {
        background-color: Black;
        filter: alpha(opacity=60);
        opacity: 0.6;
    }
    .modalPopup
    {
        background-color: #FFFFFF;
        border: 3px solid #0DA9D0;
        border-radius: 12px;
        padding:0
        }
    .modalPopup .header
    {
        background-color: #2FBDF1;
        height: 30px;
        color: White;
        line-height: 30px;
        text-align: center;
        font-weight: bold;
        border-top-left-radius: 6px;
        border-top-right-radius: 6px;
    }
    .modalPopup .body
    {
        min-height: 50px;
        line-height: 30px;
        text-align: center;
        font-weight: bold;
    }
    .modalPopup .footer
    {
        padding: 6px;
    }
    .modalPopup .yes
    {
        background-color: #2FBDF1;
        border: 1px solid #0DA9D0;
            height: 24px;
        }
        
        .auto-style5 {
            height: 20px;
        }
        .auto-style6 {
            width: 200px;
            height: 20px;
        }
        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     
        
    
    <table>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>
                <asp:Panel ID="Panel1" runat="server" Height="519px" Width="828px" BorderStyle="Solid">
                <table>
            <tr>
                <td style="font-size: 39px; font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif; font-weight: bold; color: #003399;" class="text-center" colspan="7" >Follow Up</td>
            </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td></td>
                    </tr>
            <tr>
                <td ></td>
                <td >&nbsp;</td>
                <td style="width: 100px">&nbsp;</td>
                <td>&nbsp;</td>
                <td style="font-size: 15px; font-weight: bold">Name&nbsp;&nbsp;&nbsp;&nbsp;<asp:DropDownList ID="ddl_name" runat="server" AutoPostBack="True" Font-Size="10pt" OnSelectedIndexChanged="ddl_name_SelectedIndexChanged" style="margin-left: 0px" CssClass="search_categories">
                    </asp:DropDownList>
                </td>
                <td style="font-size: 15px; font-weight: bold">Date of Test&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:TextBox ID="txt_date" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                    <cc1:CalendarExtender ID="txt_date_CalendarExtender" runat="server" Enabled="True" TargetControlID="txt_date">
                    </cc1:CalendarExtender>
                </td>
                <td>&nbsp;</td>
            </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td style="width: 100px">&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
            <tr>
                <td></td>
                <td></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td style="font-size: 15px; font-weight: bold">Status&nbsp;&nbsp;
                    <asp:DropDownList ID="ddl_status" runat="server" AutoPostBack="True" Font-Bold="False" Font-Size="10pt" style="margin-left: 0px" CssClass="search_categories">
                    </asp:DropDownList>
                </td>
                <td >
                    </td>
                <td >&nbsp;</td>
            </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
            <tr>
                <td >
                    &nbsp;</td>
                <td></td>
                <td>&nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;  <asp:Button ID="btn_add" runat="server" OnClick="btn_add_Click" Text="Add" CssClass="btnme" />
                    <cc1:ModalPopupExtender ID="btn_add_ModalPopupExtender" runat="server" BackgroundCssClass="modalBackground" DynamicServicePath="" Enabled="True" PopupControlID="PanelPopup" TargetControlID="btn_add">
                    </cc1:ModalPopupExtender>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </td>
                <td >
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btn_update" runat="server" OnClick="btn_update_Click" Text="Update" Width="96px" CssClass="btnme" />
                </td>
                <td >&nbsp;</td>
            </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td style="height: 50px">&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
            <tr>
                <td style="width: 40px" >
                    </td>
                <td colspan="6">
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" DataKeyNames="Follow_Id" Font-Bold="True" GridLines="Vertical" Height="144px" HorizontalAlign="Center" OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowDeleting="GridView1_RowDeleting" OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating" style="margin-right: 20px" Width="545px">
                        <AlternatingRowStyle BackColor="#DCDCDC" />
                        <Columns>
                            <asp:BoundField DataField="Name" HeaderText="Name" />
                            <asp:BoundField DataField="Test_Date" HeaderText="Date of test" />
                            <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
                            <asp:BoundField DataField="Status" HeaderText="Status" />
                            <asp:CommandField HeaderText="Edit" ShowEditButton="True" />
                            <asp:CommandField HeaderText="Delete" ShowDeleteButton="True" />
                        </Columns>
                        <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                        <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                        <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                        <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                        <SortedAscendingHeaderStyle BackColor="#0000A9" />
                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                        <SortedDescendingHeaderStyle BackColor="#000065" />
                    </asp:GridView>
                </td>
            </tr>
            
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td><asp:Panel ID="Panel_Popup" runat="server" Height="135px" BorderStyle="Solid" CssClass="modalPopup" Visible="False" Width="316px">
                                                                    <div>
                                                                        <br />
                                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                        <asp:Label ID="Lbl_added" runat="server" CssClass="modal-body" Text="Added Successfully!!!" Font-Size="11pt"></asp:Label>
                                                                        <br />
&nbsp;<br /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                        <asp:Button ID="btn_OK" runat="server" Text="OK" OnClick="btb_OK_Click" CssClass="btnme" />
                                                                        <br />
                                                                        <br />
                                                                        
                                                                        &nbsp;</div>
                                                                </asp:Panel></td>
                        <td>
                            <asp:ScriptManager ID="ScriptManager1" runat="server">
                            </asp:ScriptManager>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
            
            <tr>
                
                <td>&nbsp;</td>
            </tr>
        </table>
                </asp:Panel>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style5" style="height: 100px"></td>
            <td style="height: 100px;" class="auto-style6" ></td>
            <td class="auto-style5" style="height: 100px" ></td>
            <td class="auto-style5" style="height: 100px">&nbsp;&nbsp;</td>
            <td class="auto-style5" style="height: 100px"></td>
        </tr>
    </table>
     </asp:Content>
