﻿//This code allows the employee to follow the student and allot a date for entrance test 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Followup : System.Web.UI.Page
{
    connection c = new connection();
   
    //Binds the follow up data to the  grid
    
    public void gridbind()
    {
        string gb = "Select Follow_Id, Name,Test_Date,Follow.Status,Remarks from Follow inner join Enquiry on Follow.Enq_Id=Enquiry.Enq_Id";
        SqlCommand cmd = new SqlCommand(gb, c.con);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
       
        if (!IsPostBack)
        {
            if (Session["Login_Id"] == null)
                Response.Redirect("Home.aspx");

            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache,no-store,max-age=0,must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
            ddl_status.Items.Insert(0, "Select");
            ddl_status.Items.Add("Confirmed");
            ddl_status.Items.Add("Not Confirmed");
            c.getcon();
            gridbind();
            c.con.Close();
          
           //Binds Name to dropdownlist
            c.getcon();
            string nam = "Select * from Enquiry";
            SqlCommand cmdnam = new SqlCommand(nam, c.con);
            DataTable namdt = new DataTable();
            SqlDataAdapter namda = new SqlDataAdapter(cmdnam);
            namda.Fill(namdt);
            if (namdt.Rows.Count > 0)
            {
                ddl_name.DataSource = namdt;
                ddl_name.DataValueField = "Enq_Id";
                ddl_name.DataTextField = "Name";
                ddl_name.DataBind();
            }
            ddl_name.Items.Insert(0, "Select");
            c.con.Close();
        }
    }
    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView1.EditIndex = e.NewEditIndex;
        c.getcon();
        gridbind();
        c.con.Close();
    }
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        c.getcon();
        string id = GridView1.DataKeys[e.RowIndex].Values[0].ToString();
        string del = "Delete Follow where Follow_Id='" + Convert.ToString(id) + "'";
        SqlCommand cmd = new SqlCommand(del, c.con);
        cmd.ExecuteNonQuery();
        GridView1.EditIndex = -1;
        c.con.Close();
        gridbind();
    }
    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        c.getcon();
        int id = Convert.ToInt32(GridView1.DataKeys[e.RowIndex].Values[0].ToString());
        string name = ((TextBox)GridView1.Rows[e.RowIndex].Cells[0].Controls[0]).Text;
        string date = ((TextBox)GridView1.Rows[e.RowIndex].Cells[1].Controls[0]).Text;
        string status = ((TextBox)GridView1.Rows[e.RowIndex].Cells[3].Controls[0]).Text;
        string remarks = ((TextBox)GridView1.Rows[e.RowIndex].Cells[2].Controls[0]).Text;
        string cmdstr = "Update Follow set Test_Date='" + date + "',Status='" + status + "',Remarks='" + remarks + "' where Follow_Id='"+id+"'";
        SqlCommand cmd = new SqlCommand(cmdstr, c.con);
        cmd.ExecuteNonQuery();
        c.con.Close();
        GridView1.EditIndex = -1;
        gridbind();
    }

    protected void ddl_name_SelectedIndexChanged(object sender, EventArgs e)
    {
        c.getcon();
        string s = "Select Test_Date from Follow where Enq_Id = '" + ddl_name.SelectedItem.Value + "'";
        SqlCommand cmdnam = new SqlCommand(s, c.con);
        DataTable namdt = new DataTable();
        SqlDataAdapter namda = new SqlDataAdapter(cmdnam);
        namda.Fill(namdt);
        if (namdt.Rows.Count > 0)
        {
            DataRow Row = namdt.Rows[namdt.Rows.Count - 1];
            string date = Convert.ToString(Row[0]);
            txt_date.Text = date;
        }
        c.con.Close();
    }

    
    //protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    //{
      // GridView1.PageIndex = e.NewPageIndex;
       //GridView1.DataBind();
    //}

    protected void btn_add_Click(object sender, EventArgs e)
    {
        c.getcon();
        string s = "Select * from Follow where Enq_Id='" + ddl_name.SelectedItem.Value + "'";
            SqlCommand cmdnam = new SqlCommand(s, c.con);
            DataTable namdt = new DataTable();
            SqlDataAdapter namda = new SqlDataAdapter(cmdnam);
            namda.Fill(namdt);
            if (namdt.Rows.Count > 0)

                Response.Write("<script>alert('Record already Exists')</script>");
            else
            {
                string u = "Insert into Follow values ('" + ddl_name.SelectedItem.Value + "','" + txt_date.Text + "','"+ddl_status.SelectedItem.Value+"','')";
                SqlCommand cmd = new SqlCommand(u, c.con);
                cmd.ExecuteNonQuery();
                gridbind();
                //Response.Write("<script>alert('Inserted Sucessfully')</script>");
            }
    }
    protected void btn_update_Click(object sender, EventArgs e)
    {
            c.getcon();
            string u = "Update Follow set Test_Date='" + txt_date.Text + "',Remarks='',status='"+ddl_status.SelectedItem.Value+"' where Enq_Id='" + ddl_name.SelectedItem.Value + "'";
            SqlCommand cmd = new SqlCommand(u, c.con);
            cmd.ExecuteNonQuery();
            gridbind();
            Response.Write("<script>alert('Updated Sucessfully')</script>");
    }

    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView1.EditIndex = -1;
        c.getcon();
        gridbind();
        c.con.Close();
    }
    protected void btb_OK_Click(object sender, EventArgs e)
    {
        Response.Redirect("Followup");
    }
}
