﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMaster.master" AutoEventWireup="true" CodeFile="GenerateCertificate.aspx.cs" Inherits="GenerateCertificate" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <style>
            .modalBackground
    {
        background-color: Black;
        filter: alpha(opacity=60);
        opacity: 0.6;
    }
    .modalPopup
    {
        background-color: #FFFFFF;
        border: 3px solid #0DA9D0;
        border-radius: 12px;
        padding:0
        }
    .modalPopup .header
    {
        background-color: #2FBDF1;
        height: 30px;
        color: White;
        line-height: 30px;
        text-align: center;
        font-weight: bold;
        border-top-left-radius: 6px;
        border-top-right-radius: 6px;
    }
    .modalPopup .body
    {
        min-height: 50px;
        line-height: 30px;
        text-align: center;
        font-weight: bold;
    }
    .modalPopup .footer
    {
        padding: 6px;
    }
    .modalPopup .yes
    {
        background-color: #2FBDF1;
        border: 1px solid #0DA9D0;
    }
     
    </style>
    <table class="nav-justified">
        <tr>
            <td style="width: 400px; height: 70px">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:Panel ID="Panel1" runat="server" Height="296px" BorderStyle="Solid" Width="734px">
                    <table class="nav-justified">
                        <tr>
                            <td> &nbsp;</td>
             <td>  &nbsp;</td>
                            <tr>
                                <td>
                                    <asp:Button ID="ButDummy" runat="server" style=" display:none;" Text="Button" />
                                </td>
                                <td>
                                    <cc1:ModalPopupExtender ID="Link_View_ModalPopupExtender" runat="server" BackgroundCssClass="modalBackground" DynamicServicePath="" Enabled="True" PopupControlID="Panel_Popup" RepositionMode="None" TargetControlID="ButDummy">
                                    </cc1:ModalPopupExtender>
                                </td>
                                <caption>
                                    &nbsp;<tr>
                                        <td colspan="2">
                                            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" DataKeyNames="Certificate_Id" Font-Bold="True" Font-Italic="True" Font-Size="12pt" GridLines="Vertical" Height="184px" HorizontalAlign="Center" Width="524px">
                                                <AlternatingRowStyle BackColor="#DCDCDC" />
                                                <Columns>
                                                    <asp:BoundField DataField="Name" HeaderText="Name" />
                                                    <asp:BoundField DataField="Reg_No" HeaderText="Register No." />
                                                    <asp:BoundField DataField="College" HeaderText="College" />
                                                    <asp:TemplateField HeaderText="View" ShowHeader="False">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="Link_View" runat="server" OnClick="Link_View_Click">View</asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                                <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
                                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                                <RowStyle BackColor="#EEEEEE" BorderStyle="Solid" ForeColor="Black" />
                                                <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                <SortedAscendingHeaderStyle BackColor="#0000A9" />
                                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                <SortedDescendingHeaderStyle BackColor="#000065" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </caption>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td></td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                                    </asp:ScriptManager>
                                </td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="height: 100px"></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td><asp:Panel ID="Panel_Popup" runat="server" Height="570px" BackColor="#CCFFFF" CssClass="modalPopup" Width="812px" Visible="False">
                                    <div class="header" style="font-size: 20px; font-weight: bold; color: #000000"> Student Details</div><br />
                                        <table class="nav-justified">
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td style="font-size: 15px; font-weight: bold; height: 40px">Title</td>
                                                <td>
                                                    <asp:TextBox ID="txt_title" runat="server" CssClass="twitterStyleTextbox" Font-Bold="True" Font-Size="11pt"></asp:TextBox>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td style="font-size: 15px; font-weight: bold; height: 40px">Name</td>
                                                <td>
                                                    <asp:TextBox ID="txt_name" runat="server" CssClass="twitterStyleTextbox" Font-Bold="True" Font-Size="11pt"></asp:TextBox>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td style="font-size: 15px; font-weight: bold; height: 40px">Register No</td>
                                                <td>
                                                    <asp:TextBox ID="txt_regno" runat="server" CssClass="twitterStyleTextbox" Font-Bold="True" Font-Size="11pt"></asp:TextBox>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td style="font-size: 15px; font-weight: bold; height: 40px">Semester</td>
                                                <td>
                                                    <asp:TextBox ID="txt_sem" runat="server" CssClass="twitterStyleTextbox" Font-Bold="True" Font-Size="11pt"></asp:TextBox>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td style="font-size: 15px; font-weight: bold; height: 40px">College</td>
                                                <td>
                                                    <asp:TextBox ID="txt_colg" runat="server" CssClass="twitterStyleTextbox" Font-Bold="True" Font-Size="11pt"></asp:TextBox>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td style="font-size: 15px; font-weight: bold; height: 40px">University</td>
                                                <td>
                                                    <asp:TextBox ID="txt_univ" runat="server" CssClass="twitterStyleTextbox" Font-Bold="True" Font-Size="11pt"></asp:TextBox>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td style="font-size: 15px; font-weight: bold; height: 40px">Course</td>
                                                <td>
                                                    <asp:TextBox ID="txt_course" runat="server" CssClass="twitterStyleTextbox" Font-Bold="True" Font-Size="11pt"></asp:TextBox>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td style="height: 40px; font-size: 15px; font-weight: bold">Project</td>
                                                <td>
                                                    <asp:TextBox ID="txt_project" runat="server" CssClass="twitterStyleTextbox" Font-Bold="True" Font-Size="11pt"></asp:TextBox>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td style="font-size: 15px; font-weight: bold; height: 40px">Date </td>
                                                <td>
                                                    &nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td style="font-size: 15px; font-weight: bold; height: 40px">From</td>
                                                <td>
                                                    <asp:TextBox ID="txt_fromdate" runat="server"  CssClass="twitterStyleTextbox" Font-Italic="True" Font-Bold="True" Font-Size="11pt"></asp:TextBox>
                                                    <cc1:CalendarExtender ID="txt_fromdate_CalendarExtender" runat="server" Enabled="True" Format="dd-MMM-yyyy" TargetControlID="txt_fromdate">
                                                    </cc1:CalendarExtender>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td style="font-size: 15px; font-weight: bold">To</td>
                                                <td>
                                                    <asp:TextBox ID="txt_todate" runat="server" CssClass="twitterStyleTextbox" Font-Bold="True" Font-Italic="True" Font-Size="11pt"></asp:TextBox>
                                                    <cc1:CalendarExtender ID="txt_todate_CalendarExtender" runat="server" Enabled="True" Format="dd-MMM-yyyy" TargetControlID="txt_todate">
                                                    </cc1:CalendarExtender>
                                                </td>
                                                <td ></td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>
                                                    <asp:Button ID="btn_Edit" runat="server" CssClass="btnme" Text="Update" OnClick="btn_Edit_Click" />
                                                </td>
                                                <td>
                                                    <asp:Button ID="btn_Print" runat="server" CssClass="btnme" Text="Print" OnClick="btn_Print_Click" />
                                                </td>
                                                <td>
                                                    <asp:Button ID="btn_cancel" runat="server" CssClass="btnme" Text="Cancel" OnClick="btn_cancel_Click" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100px">&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                        </table>
                                    </asp:Panel></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
</asp:Content>

