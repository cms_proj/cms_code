﻿
//This code allows the admin to view the student's request for certificate and take print out of the certificate

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
public partial class GenerateCertificate : System.Web.UI.Page
{
    connection c = new connection();

    //Bind the details of the student to grid

    public void gridbind()
    {
        string gb = "Select Certificate_Id,Name,Reg_No,College from Certificate";
        SqlCommand cmd = new SqlCommand(gb, c.con);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Login_Id"] == null)
                Response.Redirect("Home.aspx");

            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache,no-store,max-age=0,must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
            c.getcon();
            gridbind();
            c.con.Close();
        }
    }

    //View the details of the student

    protected void Link_View_Click(object sender, EventArgs e)
    {
        Panel_Popup.Visible = true;
        c.getcon();
        Link_View_ModalPopupExtender.Show();
        LinkButton lk = (LinkButton)sender;
        GridViewRow grv = lk.NamingContainer as GridViewRow;
        int id = Convert.ToInt32(GridView1.DataKeys[grv.RowIndex].Values[0].ToString());
        string sel = "Select * from Certificate where Certificate_Id='"+id+"'";
        Session["Certificate_Id"] = id;
        SqlCommand cmd = new SqlCommand(sel, c.con);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            
            txt_title.Text = dt.Rows[0][1].ToString();
            txt_name.Text = dt.Rows[0][2].ToString();
            txt_regno.Text = dt.Rows[0][3].ToString();
            txt_sem.Text = dt.Rows[0][4].ToString();
            txt_colg.Text = dt.Rows[0][5].ToString();
            txt_univ.Text = dt.Rows[0][6].ToString();
            txt_course.Text = dt.Rows[0][7].ToString();
            txt_project.Text = dt.Rows[0][8].ToString();
            txt_fromdate.Text = dt.Rows[0][9].ToString();
            txt_todate.Text = dt.Rows[0][10].ToString();

        }
        cmd.ExecuteNonQuery();
        c.con.Close();


    }

    //Edits the details of the student

    protected void btn_Edit_Click(object sender, EventArgs e)
    {
        c.getcon();
        int id = Convert.ToInt32(Session["Certificate_Id"]);
        string up = "Update Certificate set Title='"+txt_title.Text+"',Name='"+txt_name.Text+"',Reg_No='"+txt_regno.Text+"',Semester='"+txt_sem.Text+"',College='"+txt_colg.Text+"',University='"+txt_univ.Text+"',Course='"+txt_course.Text+"',Project='"+txt_project.Text+"',From_Date='"+txt_fromdate.Text+"',To_Date='"+txt_todate.Text+"' where Certificate_Id='"+id+"'";
        SqlCommand cmd = new SqlCommand(up, c.con);
        cmd.ExecuteNonQuery();
        Panel_Popup.Visible = false;
        c.con.Close();
    }
    protected void btn_cancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("GenerateCertificate.aspx");
    }
    
    //Shows the pdf of certificate

    private void ShowPdf(string strS)
    {
        Response.ClearContent();
        Response.ClearHeaders();
        Response.ContentType = "application/pdf";
        Response.AddHeader("Content-Disposition", "attachment;filename=" + strS);
        Response.TransmitFile(strS);
        Response.End();
        Response.Flush();
        Response.Clear();
    }
    protected void btn_Print_Click(object sender, EventArgs e)
    {
        c.getcon();
        int id = Convert.ToInt32(Session["Certificate_Id"]);
        string sel = "Select * from Certificate where Certificate_Id='" + id + "'";
        
        SqlCommand cmd = new SqlCommand(sel, c.con);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);
        cmd.ExecuteNonQuery();
        

        //pdf
        //pdf generation

        //Create a Document object
        var document = new Document(PageSize.A4, 50, 50, 25, 25);

        // Create a new PdfWriter object, specifying the output stream
        var output = new MemoryStream();
        //var writer = PdfWriter.GetInstance(document, output);
        var writer = PdfWriter.GetInstance(document, new FileStream(HttpContext.Current.Server.MapPath("~/Test.pdf"), FileMode.Create));
        writer.PageEvent = new ITextEvents();

        //Font set
        BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
        Font times = new Font(bfTimes, 15, Font.BOLD);


        //Font set
        BaseFont bfTimesText = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
        Font timesText = new Font(bfTimes, 14, Font.NORMAL);

        BaseFont bfTimes1 = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
        Font times1 = new Font(bfTimes1, 14, Font.BOLD);


        // Open the Document for writing
        document.Open();

        // Create a new Paragraph object with the text, "Hello, World!"

        var para1 = new Paragraph(" ",times);
        document.Add(para1);
        para1.SpacingAfter = 500f;
        var welcomeParagrapha = new Paragraph("CERTIFICATE", times);
       
        welcomeParagrapha.Alignment = Element.ALIGN_CENTER;
        welcomeParagrapha.SetLeading(60.0f, 3.0f);
        //welcomeParagraph.FirstLineIndent = 50f;
        welcomeParagrapha.SpacingAfter = 15f;

        //var welcomeParagraph = new Paragraph(dtp.Rows[0][2].ToString(), timesText);
        //welcomeParagraph.FirstLineIndent = 50f;
        //welcomeParagrapha.SpacingAfter = 15f;
        // welcomeParagraphp.Font=Element.

        var welcomeParagraphb = new Paragraph("                    This is to Certify that  ", times1);
        welcomeParagraphb.Alignment = Element.ALIGN_JUSTIFIED;
        
        Phrase ph1 = new Phrase(dt.Rows[0][1].ToString(), timesText);
        welcomeParagraphb.Add(ph1);

        Phrase s = new Phrase(". ", timesText);
        welcomeParagraphb.Add(s);

        Phrase ph2 = new Phrase(dt.Rows[0][2].ToString(), timesText);
        welcomeParagraphb.Add(ph2);

        Phrase ph3 = new Phrase(" (", timesText);
        welcomeParagraphb.Add(ph3);
        
        Phrase ph4 = new Phrase(dt.Rows[0][3].ToString(), timesText);
        welcomeParagraphb.Add(ph4);

        Phrase ph5 = new Phrase(") ", timesText);
        welcomeParagraphb.Add(ph5);

        Phrase ph6 = new Phrase(dt.Rows[0][4].ToString(), timesText);
        welcomeParagraphb.Add(ph6);

        Phrase ph7 = new Phrase(" th Semester ", timesText);
        welcomeParagraphb.Add(ph7);

        Phrase ph8 = new Phrase(dt.Rows[0][7].ToString(), timesText);
        welcomeParagraphb.Add(ph8);

        Phrase ph9 = new Phrase(" student of ", timesText);
        welcomeParagraphb.Add(ph9);

        Phrase ph10 = new Phrase(dt.Rows[0][5].ToString(), timesText);
        welcomeParagraphb.Add(ph10);

        Phrase ph11 = new Phrase(" affiliated to ", timesText);
        welcomeParagraphb.Add(ph11);

        Phrase ph12 = new Phrase(dt.Rows[0][6].ToString(), timesText);
        welcomeParagraphb.Add(ph12);

        Phrase ph13 = new Phrase(" University has succesfully completed the project Titled ", timesText);
        welcomeParagraphb.Add(ph13);

        Phrase ph14 = new Phrase(@" """, timesText);
        welcomeParagraphb.Add(ph14);

        Phrase ph15 = new Phrase(dt.Rows[0][8].ToString(), timesText);
        welcomeParagraphb.Add(ph15);

        Phrase ph16 = new Phrase(@""" ", timesText);
        welcomeParagraphb.Add(ph16);

        Phrase ph17 = new Phrase(" in our Company. She worked on this Project from  ", timesText);
        welcomeParagraphb.Add(ph17);

        Phrase ph18 = new Phrase(dt.Rows[0][9].ToString(), timesText);
        welcomeParagraphb.Add(ph18);

        Phrase ph19 = new Phrase(" to ", timesText);
        welcomeParagraphb.Add(ph19);

        Phrase ph20 = new Phrase(dt.Rows[0][10].ToString(), timesText);
        welcomeParagraphb.Add(ph20);

        var welcomeParagraphf = new Paragraph("  ", times1);
        welcomeParagraphf.Alignment = Element.ALIGN_RIGHT;

        var welcomeParagraphc = new Paragraph(" For CCS Technologies (P)ltd  ", times1);
        welcomeParagraphc.Alignment = Element.ALIGN_RIGHT;

        var welcomeParagraphe = new Paragraph("  ", times1);
        welcomeParagraphe.Alignment = Element.ALIGN_RIGHT;

        var welcomeParagraphd = new Paragraph(" CDC Head  ", times1);
        welcomeParagraphd.Alignment = Element.ALIGN_RIGHT;

        

        // Add the Paragraph object to the document
        document.Add(welcomeParagrapha);
        document.Add(welcomeParagraphb);
        document.Add(welcomeParagraphf);
        document.Add(welcomeParagraphc);
        document.Add(welcomeParagraphe);
        document.Add(welcomeParagraphd);

        

        document.Close();
        c.con.Close();
        string path = Server.MapPath("~/Test.pdf");
        ShowPdf(path);

        Response.Redirect(Server.MapPath("~/Test.pdf"));
    }
}