﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMaster.master" AutoEventWireup="true" CodeFile="ManageCourseTechnology.aspx.cs" Inherits="ManageCourseTechnology" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
         .modalBackground
    {
        background-color: Black;
        filter: alpha(opacity=60);
        opacity: 0.6;
    }
    .modalPopup
    {
        background-color: #FFFFFF;
        border: 3px solid #0DA9D0;
        border-radius: 12px;
        padding:0
        }
    .modalPopup .header
    {
        background-color: #2FBDF1;
        height: 30px;
        color: White;
        line-height: 30px;
        text-align: center;
        font-weight: bold;
        border-top-left-radius: 6px;
        border-top-right-radius: 6px;
    }
    .modalPopup .body
    {
        min-height: 50px;
        line-height: 30px;
        text-align: center;
        font-weight: bold;
    }
    .modalPopup .footer
    {
        padding: 6px;
    }
    .modalPopup .yes
    {
        background-color: #2FBDF1;
        border: 1px solid #0DA9D0;
    }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <table class="nav-justified">
        <tr>
            <td style="width: 100px">&nbsp;</td>
            <td style="width: 400px; height: 100px"><asp:Button ID ="ButDummy" runat="server" style =" display:none;" Text="Button"/>
                 <cc1:ModalPopupExtender ID="ButAdd_ModalPopupExtender" runat="server" DynamicServicePath=""  Enabled="True" PopupControlID="Panel_Popup" RepositionMode="None" TargetControlID="ButAdd" BackgroundCssClass="modalBackground">
            </cc1:ModalPopupExtender>
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                <asp:Panel ID="Panel1" runat="server" Height="331px" BorderStyle="Solid" Width="373px">
                    <table>
    <tr>
        <td colspan="6" style="height: 50px">&nbsp;</td>
    </tr>
                        <tr>
                            <td colspan="6" style="height: 50px; font-size: 20px; font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif; font-weight: bold; color: #000000;">&nbsp;&nbsp;&nbsp; Program and included Technologies</td>
                        </tr>
    <tr>
        <td style="height: 50px">&nbsp;</td>
        <td style="font-size: 15px; font-weight: bold">Course </td>
        <td>
            <asp:DropDownList ID="DdlCourse" runat="server" CssClass="search_categories">
            </asp:DropDownList>
        &nbsp;<asp:Label ID="req_course" runat="server" Text="*Select" ForeColor="Red" Visible="False"></asp:Label>
        </td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td class="auto-style1" style="height: 50px">&nbsp;</td>
        <td class="auto-style1" style="font-size: 15px; font-weight: bold">Technology</td>
        <td class="auto-style1">
            <asp:DropDownList ID="DdlTech" runat="server" CssClass="search_categories">
            </asp:DropDownList>
        &nbsp; <asp:Label ID="req_tech" runat="server" Text="*Select" ForeColor="Red" Visible="False"></asp:Label>
        </td>
        <td class="auto-style1"></td>
        <td class="auto-style1"></td>
    </tr>
    <tr>
        <td style="width: 100px">&nbsp;</td>
        <td style="width: 100px">&nbsp;</td>
        <td style="width: 100px">&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td colspan="6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="ButAdd" runat="server" CssClass="btnme" OnClick="ButAdd_Click" Text="Add" Width="73px" />
           
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
</table>
                </asp:Panel>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td style="width: 100px; height: 100px;">
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
            </td>
            <td><asp:Panel ID="Panel_Popup" runat="server" Height="135px" BorderStyle="Solid" CssClass="modalPopup" Visible="False" Width="316px">
                                                                    <div>
                                                                        <br />
                                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                        <asp:Label ID="Lbl_added" runat="server" CssClass="modal-body" Text="Added Successfully!!!" Font-Size="11pt"></asp:Label>
                                                                        <br />
&nbsp;<br /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                        <asp:Button ID="btn_OK" runat="server" Text="OK" OnClick="btb_OK_Click" CssClass="btnme" />
                                                                        <br />
                                                                        <br />
                                                                        
                                                                        &nbsp;</div>
                                                                </asp:Panel></td>
            <td>&nbsp;</td>
        </tr>
    </table>
    
</asp:Content>

