﻿
//Code to add new courses and technologies

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class ManageCourseTechnology : System.Web.UI.Page
{
    connection c = new connection();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Login_Id"] == null)
                Response.Redirect("Home.aspx");

            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache,no-store,max-age=0,must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
            // Binds Course to dropdownlist
            c.getcon();
            string cou = "Select * from Course";
            SqlCommand cmdco = new SqlCommand(cou, c.con);
            DataTable codt = new DataTable();
            SqlDataAdapter coda = new SqlDataAdapter(cmdco);
            coda.Fill(codt);
            if (codt.Rows.Count > 0)
            {
                DdlCourse.DataSource = codt;
                DdlCourse.DataValueField = "Course_Id";
                DdlCourse.DataTextField = "Course";

                DdlCourse.DataBind();
            }
            DdlCourse.Items.Insert(0, "Select");
            c.con.Close();

            // Binds Technology to dropdownlist
            c.getcon();
            string tec = "Select * from Technology";
            SqlCommand cmdtec = new SqlCommand(tec, c.con);
            DataTable dtec = new DataTable();
            SqlDataAdapter datec = new SqlDataAdapter(cmdtec);
            datec.Fill(dtec);
            if (dtec.Rows.Count > 0)
            {
                DdlTech.DataSource = dtec;
                DdlTech.DataValueField = "Tech_Id";
                DdlTech.DataTextField = "Tech";
                DdlTech.DataBind();
            }
            DdlTech.Items.Insert(0, "Select");
            c.con.Close();
        }
    }
    protected void ButAdd_Click(object sender, EventArgs e)
    {
        if (DdlCourse.SelectedIndex > 0 && DdlTech.SelectedIndex > 0)
        {
            req_course.Visible = false;
            req_tech.Visible = false;
            c.getcon();
            string s = "Insert into OptedTech values('" + DdlCourse.SelectedItem.Value + "','" + DdlTech.SelectedItem.Value + "')";
            SqlCommand cmd = new SqlCommand(s, c.con);
            cmd.ExecuteNonQuery();
            Panel_Popup.Visible = true;
            ButAdd_ModalPopupExtender.Show();
            c.con.Close();
        }
        else
        {
            req_course.Visible = true;
            req_tech.Visible = true;
        }
    }
    protected void btb_OK_Click(object sender, EventArgs e)
    {
        Response.Redirect("ManageCourseTechnology.aspx");
    }
}