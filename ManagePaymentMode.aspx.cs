﻿//Code to add or remove payment modes such as online,cheque etc

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;


public partial class ManagePaymentMode : System.Web.UI.Page
{
    connection c = new connection();
    
    //Binds the existing payment modes to grid
    
    public void gridbind()
    {
        string gb = "Select * from PaymentMode";
        SqlCommand cmd = new SqlCommand(gb, c.con);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Login_Id"] == null)
                Response.Redirect("Home.aspx");

            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache,no-store,max-age=0,must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
            c.getcon();
            gridbind();
            c.con.Close();

        }
    }
    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView1.EditIndex = e.NewEditIndex;
        c.getcon();
        gridbind();
        c.con.Close();
    }
    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        c.getcon();
        int id = Convert.ToInt32(GridView1.DataKeys[e.RowIndex].Values[0].ToString());
        string pay = ((TextBox)GridView1.Rows[e.RowIndex].Cells[1].Controls[0]).Text;
        string update = "Update PaymentMode set Payment_Name='" + pay + "'where Pmode_Id='" + id + "'";
        SqlCommand cmd = new SqlCommand(update, c.con);
        cmd.ExecuteNonQuery();
        gridbind();
        c.con.Close();
    }
    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView1.EditIndex = -1;
        c.getcon();
        gridbind();
        c.con.Close();

    }
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        c.getcon();
        string id = GridView1.DataKeys[e.RowIndex].Values[0].ToString();
        string del = "Delete PaymentMode where Pmode_Id='" + Convert.ToString(id) + "'";
        SqlCommand cmd = new SqlCommand(del, c.con);
        cmd.ExecuteNonQuery();
        GridView1.EditIndex = -1;
        c.con.Close();
        gridbind();
    }

    //Adds new mode to the database

    protected void btn_Add_Click(object sender, EventArgs e)
    {
        c.getcon();
        string ins = "insert into PaymentMode values('"+txt_PayMode.Text+"')";
        SqlCommand cmd = new SqlCommand(ins,c.con);
        cmd.ExecuteNonQuery();
        gridbind();
        Panel_Popup.Visible = true;
        btn_Add_ModalPopupExtender.Show();
        c.con.Close();
    }
    protected void btb_OK_Click(object sender, EventArgs e)
    {
        Response.Redirect("ManagePaymentMode.aspx");
    }
}