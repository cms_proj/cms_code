﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StaffMaster.master" AutoEventWireup="true" CodeFile="NormalEvaluation.aspx.cs" Inherits="NormalEvaluation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
      
        .auto-style5 {
            height: 598px;
        }
      
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <table class="nav-justified">
        <tr>
            <td style="width: 300px; height: 100px">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style5"></td>
            <td class="auto-style5">
                <asp:Panel ID="Panel1" runat="server" BorderStyle="Solid" Width="517px" Height="672px">
                    <table>
        <tr>
            <td class="text-center" colspan="3" style="font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif; font-size: 39px; font-weight: bold; color: #003399;">Evaluation</td>
        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td style="font-size: 15px; font-weight: bold;">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td style="font-size: 15px; font-weight: bold;">Name</td>
                            <td>
                                <asp:DropDownList ID="ddl_name" runat="server" AutoPostBack="True" CssClass="search_categories" OnSelectedIndexChanged="ddl_name_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td style="font-size: 15px; font-weight: bold;">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
        <tr>
            <td>&nbsp;</td>
            <td style="font-size: 15px; font-weight: bold">Test Name</td>
            <td>
                <asp:DropDownList ID="ddl_test" runat="server" AutoPostBack="True" CssClass="search_categories" >
                </asp:DropDownList>
            </td>
        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td style="font-size: 15px; font-weight: bold">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
        <tr>
            <td>&nbsp;</td>
            <td style="font-size: 15px; font-weight: bold">Marks Scored</td>
            <td>
                <asp:TextBox ID="txt_mark" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
            </td>
        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td style="font-size: 15px; font-weight: bold">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td style="font-size: 15px; font-weight: bold">Status</td>
                            <td>
                                <asp:DropDownList ID="ddl_status" runat="server" AutoPostBack="True" CssClass="search_categories">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td style="font-size: 15px; font-weight: bold">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
        <tr>
            <td >&nbsp;</td>
            <td style="font-size: 15px; font-weight: bold">Remarks</td>
            <td>
                <asp:TextBox ID="txt_remark" runat="server" TextMode="MultiLine" CssClass="twitterStyleTextbox"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td style="width: 200px"></td>
            <td>
            </td>
        </tr>
        <tr>
            <td >&nbsp;</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="lbl_save" runat="server" CssClass="btnme" OnClick="lbl_save_Click" Text="Save" />
            </td>
        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td colspan="2">&nbsp;</td>
                        </tr>
        <tr>
            <td colspan="3">
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" DataKeyNames="Evaluation_Id" Font-Bold="True" GridLines="Vertical" Height="176px" HorizontalAlign="Center" Width="401px" OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating">
                    <AlternatingRowStyle BackColor="#DCDCDC" />
                    <Columns>
                        <asp:BoundField DataField="Test" HeaderText="Test Name" />
                        <asp:BoundField DataField="Eval_Mark" HeaderText="Mark Scored" />
                        <asp:BoundField DataField="Eval_Remarks" HeaderText="Remarks" />
                        <asp:CommandField HeaderText="Edit" ShowEditButton="True" />
                    </Columns>
                    <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                    <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                    <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#0000A9" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#000065" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td style="width: 100px" >&nbsp;</td>
            <td></td>
            <td></td>
        </tr>
    </table>
                </asp:Panel>
            </td>
            <td class="auto-style5"></td>
        </tr>
        <tr>
            <td style="height: 100px">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
    
</asp:Content>

