﻿
//Code that adds marks of all the tests conducted as a part of evaluation.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class NormalEvaluation : System.Web.UI.Page
{
    connection c =new connection();

    //Binds the Evaluation details to the grid

    public void gridbind()
    {
        string gb="Select Test.Test,Eval_Mark,Eval_Remarks,Evaluation_Id from NormalEvaluation inner join Test on Test.Test_Id=NormalEvaluation.Test_Id inner join Enquiry on Enquiry.Enq_Id=NormalEvaluation.Enq_Id where NormalEvaluation.Enq_Id='"+ddl_name.SelectedItem.Value+"'";
        //string gb = "Select Evaluation_Id,Enquiry.Name,Eval_Mark,NormalEvaluation.Eval_Status,Eval_Remarks from NormalEvaluation inner join Enquiry on NormalEvaluation.Enq_Id=Enquiry.Enq_Id";
        SqlCommand cmd = new SqlCommand(gb, c.con);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Login_Id"] == null)
                Response.Redirect("Home.aspx");

            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache,no-store,max-age=0,must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
             
            //Binds Name to dropdownlist
            
            c.getcon();
            string nam = "Select * from Enquiry inner join InitialEvaluation on Enquiry.Enq_Id =InitialEvaluation.Enq_Id where InitialEvaluation.Status='Passed'";
            SqlCommand cmdtest = new SqlCommand(nam, c.con);
            DataTable testdt = new DataTable();
            SqlDataAdapter testda = new SqlDataAdapter(cmdtest);
            testda.Fill(testdt);
            if (testdt.Rows.Count > 0)
            {
                ddl_name.DataSource = testdt;
                ddl_name.DataValueField = "Enq_Id";
                ddl_name.DataTextField = "Name";
                ddl_name.DataBind();
            }
            ddl_name.Items.Insert(0, "Select");
            c.con.Close();

            
            ddl_status.Items.Insert(0, "Select");
            ddl_status.Items.Add("Passed");
            ddl_status.Items.Add("Failed");
            ddl_status.Items.Add("Not Attended");
         }
    }

    //Adds normal evaluation details to the database

    protected void lbl_save_Click(object sender, EventArgs e)
    {
        c.getcon();
        string save = "Insert into NormalEvaluation values('"+ddl_name.SelectedItem.Value+"','"+ddl_test.SelectedItem.Value+"','"+txt_mark.Text+"','"+ddl_status.SelectedItem.Value+"','"+txt_remark.Text+"')";
        SqlCommand cmd = new SqlCommand(save, c.con);
        
        cmd.ExecuteNonQuery();
        gridbind();
        c.con.Close();

    }
    protected void ddl_name_SelectedIndexChanged(object sender, EventArgs e)
    {
        // Binds Test names to the dropdownlist
        c.getcon();
        gridbind();
        if (ddl_name.SelectedItem.Text != "Select")
        {
            int eid = Convert.ToInt32(ddl_name.SelectedItem.Value);
            string test = "Select Reg_Id from Registration where Registration.Enq_Id='" + eid + "'";
            SqlCommand cmdtst = new SqlCommand(test, c.con);
            DataTable tesdt = new DataTable();
            SqlDataAdapter tesda = new SqlDataAdapter(cmdtst);
            tesda.Fill(tesdt);
            if (tesdt.Rows.Count > 0)
            {
                string str = "select Test,Test_Id from Test where Test_Id not in(Select Test_Id from NormalEvaluation where Enq_Id='" + ddl_name.SelectedItem.Value + "')AND  Test_Id IN (Select Test_Id from Test where Test.Test_Id not in(select Test_Id from InitialEvaluation where InitialEvaluation.Enq_Id='" + ddl_name.SelectedItem.Value + "'))";
                //Convert.ToInt32(tesdt.Rows[0][0].ToString())
                SqlCommand cmd = new SqlCommand(str, c.con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    ddl_test.DataSource = dt;
                    ddl_test.DataValueField = "Test_Id";
                    ddl_test.DataTextField = "Test";
                    ddl_test.DataBind();
                }
                ddl_test.Items.Insert(0, "Select");
                c.con.Close();
            }
        }
    }
    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView1.EditIndex = e.NewEditIndex;
        c.getcon();
        gridbind();
        c.con.Close();
    }
    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        c.getcon();
        int id = Convert.ToInt32(GridView1.DataKeys[e.RowIndex].Values[0].ToString());


        string update = "Update";
        SqlCommand cmd = new SqlCommand(update, c.con);
        cmd.ExecuteNonQuery();

        c.con.Close();
    }
}