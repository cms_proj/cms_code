﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StaffMaster.master" AutoEventWireup="true" CodeFile="PaymentPage.aspx.cs" Inherits="PaymentPage" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    
    <style type="text/css">
               
    </style>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <table class="nav-justified">
        <tr>
            <td style="height: 50px; width: 200px;"></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:Panel ID="Panel1" runat="server" Height="2048px" BorderStyle="Solid" Width="845px">
                    <table>
    <tr>
        
        <td>
            <asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>
        </td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td style="height: 50px"></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td style="font-size: 15px; font-weight: bold">Receipt Number<br /> <br />
            <asp:TextBox ID="txt_RecNo" runat="server" CssClass="twitterStyleTextbox" Font-Bold="True"></asp:TextBox>
        </td>
        <td>
            &nbsp;</td>
        <td>&nbsp;</td>
        <td style="font-size: 15px; font-weight: bold">Receipt Date<br /> <br />
            <asp:TextBox ID="txt_RecDate" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
            <cc1:CalendarExtender ID="txt_RecDate_CalendarExtender" runat="server" Enabled="True" TargetControlID="txt_RecDate">
            </cc1:CalendarExtender>
            <br />
        </td>
        <td></td>
    </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td style="font-size: 15px; font-weight: bold">&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
    <tr>
        <td>&nbsp;</td>
        <td></td>
        <td>&nbsp;</td>
        <td>Choose Name</td>
        <td>
            <asp:DropDownList ID="ddl_name" runat="server" OnSelectedIndexChanged="ddl_name_SelectedIndexChanged" AutoPostBack="True" CssClass="search_categories">
            </asp:DropDownList>
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
    <tr>
        <td>&nbsp;</td>
        <td></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td colspan="2">
            <asp:Panel ID="PanelPayDetails" runat="server" BackColor="White" BorderColor="Black" BorderStyle="Solid" Height="219px" Width="331px">
                &nbsp;&nbsp;&nbsp;&nbsp;
                <table>
                    <tr>
                        <td>&nbsp;</td>
                        <td style="font-size: 15px; font-weight: bold">Type of Installment :</td>
                        <td>
                            <asp:Label ID="lbl_instType" runat="server"></asp:Label>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td style="font-size: 15px; font-weight: bold">&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td ></td>
                        <td style="font-size: 15px; font-weight: bold" >Total Payment :</td>
                        <td >
                            <asp:Label ID="lbl_TotPay" runat="server"></asp:Label>
                        </td>
                        <td ></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td style="font-size: 15px; font-weight: bold">&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td style="font-size: 15px; font-weight: bold">Payment Pending :</td>
                        <td>
                            <asp:Label ID="lbl_PendPay" runat="server"></asp:Label>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td style="font-size: 15px; font-weight: bold">&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td style="font-size: 15px; font-weight: bold" >Installment Currently Paying :</td>
                        <td>
                            <asp:Label ID="lbl_instCP" runat="server"></asp:Label>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td  colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Label ID="lbl_notPay" runat="server" style="text-align: center"></asp:Label>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td colspan="2">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </asp:Panel>
        </td>
        <td></td>
        <td ></td>
    </tr>
    <tr>
        <td >&nbsp;</td>
        <td ></td>
        <td>&nbsp;</td>
        <td >&nbsp;</td>
        <td >&nbsp;</td>
        <td >&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td >&nbsp;</td>
        <td></td>
        <td>&nbsp;</td>
        <td >
            <asp:Label ID="lbl_nextinstamt" runat="server" Text="Current Payment Amount" Visible="False" Font-Bold="True"></asp:Label>
        </td>
        <td >
            <asp:TextBox ID="txt_instamt" runat="server" Visible="False" CssClass="twitterStyleTextbox"></asp:TextBox>
        </td>
        <td >&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td >&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td >
            &nbsp;</td>
        <td >
            &nbsp;</td>
        <td >&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td></td>
                            <td>&nbsp;</td>
                            <td>
                                <asp:Label ID="lbl_pmode" runat="server" Font-Bold="True" Text="Choose Payment Mode" Visible="False"></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddl_mode" runat="server" AutoPostBack="True" CssClass="search_categories" OnSelectedIndexChanged="ddl_mode_SelectedIndexChanged" Visible="False">
                                </asp:DropDownList>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
    <tr>
        <td style="width: 100px" >&nbsp;</td>
        <td></td>
        <td>&nbsp;</td>
        <td >&nbsp;</td>
        <td >
            <asp:Label ID="lbl_notipChq" runat="server" Text="Note : No Postdated Cheques" Visible="False" ForeColor="Red"></asp:Label>
        </td>
        <td >&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td ></td>
        <td ></td>
        <td></td>
        <td ></td>
        <td >&nbsp;</td>
        <td ></td>
        <td ></td>
    </tr>
    <tr>
        <td >&nbsp;</td>
        <td ></td>
        <td>&nbsp;</td>
        <td  colspan="2">
            <asp:Panel ID="Panel2" runat="server" Height="248px" Width="370px" BorderStyle="Solid" Visible="False">
                <br />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <table >
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <asp:GridView ID="GridView_Cheque" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" Height="143px" Width="338px">
                                <AlternatingRowStyle BackColor="White" />
                                <Columns>
                                    <asp:BoundField DataField="Payer_Name" HeaderText="Payer Name" />
                                    <asp:BoundField DataField="Date" HeaderText="Date" />
                                    <asp:BoundField DataField="Amount" HeaderText="Amount" />
                                    <asp:TemplateField HeaderText="Choose">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="CheckBoxpchq" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EditRowStyle BackColor="#2461BF" />
                                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                <RowStyle BackColor="#EFF3FB" />
                                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                <SortedDescendingHeaderStyle BackColor="#4870BE" />
                            </asp:GridView>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Button ID="btn_add" runat="server" Text="Add" OnClick="btn_add_Click" CssClass="btnme" />
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </asp:Panel>
        </td>
        <td >&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td >&nbsp;</td>
        <td ></td>
        <td>&nbsp;</td>
        <td >&nbsp;</td>
        <td >&nbsp;</td>
        <td >&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td ></td>
        <td></td>
        <td>&nbsp;</td>
        <td colspan="4">
            <asp:MultiView ID="MultiViewPayment" runat="server">
                <table>
                    <tr>
                        <td>
                            <asp:View ID="ViewCash" runat="server">
                                
                                <asp:Panel ID="Panel_cash" runat="server" BorderStyle="Outset" Width="370px">
                                    <table>
                                    <tr>
                                        <td colspan="5" style="font-family: 'felix Titling'; font-size: 25px; font-weight: bold; color: #000099;">Specify Cash Details</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>&nbsp;</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20px"></td>
                                        <td>&nbsp;</td>
                                        <td style="font-size: 15px; font-weight: bold">Cash Amount</td>
                                        <td>
                                            <asp:TextBox ID="txt_amt" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>&nbsp;</td>
                                        <td style="font-size: 15px; font-weight: bold">Remarks if any</td>
                                        <td>
                                            <asp:TextBox ID="txt_remarks" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20px"></td>
                                        <td>&nbsp;</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </table>
                                </asp:Panel>
                                
                            </asp:View>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:View ID="ViewCheque" runat="server">
                                
                                <asp:Panel ID="Panel_Cheque" runat="server" Width="374px" BorderStyle="Outset">
                                    <table>
                                    <tr>
                                        <td colspan="5" style="font-family: 'felix Titling'; font-size: 25px; font-weight: bold; color: #000099; text-decoration: blink">Specify Cheque Details</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20px"></td>
                                        <td>&nbsp;</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>&nbsp;</td>
                                        <td style="font-size: 15px; font-weight: bold">Choose Bank</td>
                                        <td>
                                            <asp:DropDownList ID="ddl_bankname" runat="server" CssClass="search_categories" AutoPostBack="True" OnSelectedIndexChanged="ddl_bankname_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td style="font-size: 15px; font-weight: bold">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>&nbsp;</td>
                                        <td style="font-size: 15px; font-weight: bold">Choose Branch</td>
                                        <td>
                                            <asp:DropDownList ID="ddl_branch" runat="server" CssClass="search_categories">
                                            </asp:DropDownList>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td style="font-size: 15px; font-weight: bold">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>&nbsp;</td>
                                        <td style="font-size: 15px; font-weight: bold">Amount</td>
                                        <td>
                                            <asp:TextBox ID="txt_amtcheque" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td style="font-size: 15px; font-weight: bold">&nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>&nbsp;</td>
                                        <td style="font-size: 15px; font-weight: bold">Date</td>
                                        <td>
                                            <asp:TextBox ID="txt_date" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td style="font-size: 15px; font-weight: bold">&nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>&nbsp;</td>
                                        <td style="font-size: 15px; font-weight: bold">Payer Name</td>
                                        <td>
                                            <asp:TextBox ID="txt_payrname" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td style="font-size: 15px; font-weight: bold">&nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>&nbsp;</td>
                                        <td style="font-size: 15px; font-weight: bold">Remarks(If any)</td>
                                        <td>
                                            <asp:TextBox ID="txt_remCheq" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td style="font-size: 15px; font-weight: bold">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                </table>
                                </asp:Panel>
                                
                            </asp:View>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:View ID="ViewOnlinePay" runat="server">
                                
                                <asp:Panel ID="Panel_online" runat="server" BorderStyle="Outset" Width="374px">
                                    <table>
                                    <tr>
                                        <td colspan="4" style="font-family: 'Felix Titling'; font-size: 25px; font-weight: bold; color: #000099; ">Specify Online Payment Details</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td style="font-size: 15px; font-weight: bold">Specify Transaction ID</td>
                                        <td>
                                            <asp:TextBox ID="txt_transac" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 30px">&nbsp;</td>
                                        <td style="font-size: 15px; font-weight: bold">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td style="font-size: 15px; font-weight: bold">Account Number</td>
                                        <td>
                                            <asp:TextBox ID="txt_accountno" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td style="font-size: 15px; font-weight: bold">&nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td style="font-size: 15px; font-weight: bold">Payment Date</td>
                                        <td>
                                            <asp:TextBox ID="txt_paydate" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td style="font-size: 15px; font-weight: bold">&nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td style="font-size: 15px; font-weight: bold">Amount</td>
                                        <td>
                                            <asp:TextBox ID="txt_onlineamt" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td style="font-size: 15px; font-weight: bold">&nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td style="font-weight: bold; font-size: 15px">Specify IFSC Code</td>
                                        <td>
                                            <asp:TextBox ID="txt_IFSC" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td style="font-weight: bold; font-size: 15px">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td style="font-size: 15px; font-weight: bold">Remarks(If any)</td>
                                        <td>
                                            <asp:TextBox ID="txt_onlinerem" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                        </td>
                                        <td></td>
                                    </tr>
                                </table>
                                </asp:Panel>
                                
                            </asp:View>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                </table>
            </asp:MultiView>
        </td>
        <td></td>
    </tr>
    <tr>
        <td colspan="8" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="btn_save" runat="server" CssClass="btnme" OnClick="btn_save_Click" Text="Save" />
        </td>
    </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>&nbsp;</td>
                            <td></td>
                            <td></td>
                        </tr>
    <tr>
        <td ></td>
        <td ></td>
        <td></td>
        <td  colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </td>
        <td></td>
        <td ></td>
    </tr>
    <tr>
        <td style="width: 100px" ></td>
        <td ></td>
        <td></td>
        <td ></td>
        <td ></td>
        <td ></td>
        <td></td>
    </tr>
    <tr>
        <td</td>
        <td ></td>
        <td ></td>
        <td></td>
        <td ></td>
        <td ></td>
        <td></td>
    </tr>
</table>
                </asp:Panel>
            </td>
            <td></td>
        </tr>
        <tr>
            <td style="width: 100px; height: 100px;"></td>
            <td></td>
            <td></td>
        </tr>
    </table>
    
</asp:Content>

