﻿//This code adds the payment details of students to the table.Students can pay as Lumpsum or Installment.
//If the choosen type is installment,Student can pay fees as any modes like cheque,cash,online etc..

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.IO;


public partial class PaymentPage : System.Web.UI.Page
{
    connection c = new connection();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Login_Id"] == null)
                Response.Redirect("Home.aspx");

            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache,no-store,max-age=0,must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
            c.getcon();
           
            //Binds name to the database
            string nam = "Select * from Enquiry inner join Registration on Enquiry.Enq_Id=Registration.Enq_Id";
            SqlCommand cmdnam = new SqlCommand(nam, c.con);
            DataTable namdt = new DataTable();
            SqlDataAdapter namda = new SqlDataAdapter(cmdnam);
            namda.Fill(namdt);
            if (namdt.Rows.Count > 0)
            {
                ddl_name.DataSource = namdt;
                ddl_name.DataValueField = "Reg_Id";
                ddl_name.DataTextField = "Name";
                ddl_name.DataBind();
            }
            ddl_name.Items.Insert(0, "Select");
            c.con.Close();

            btn_add.Visible = false;
            btn_save.Visible = false;
            lbl_nextinstamt.Visible = false;
            txt_instamt.Visible = false;
            lbl_pmode.Visible = false;
            ddl_mode.Visible = false;
        }
    }

    //Binds Payment  mode to database
    public void dropbind()
    {
        string strdr = "Select * from PaymentMode";
        SqlCommand cmddr = new SqlCommand(strdr, c.con);
        DataTable dtdr = new DataTable();
        SqlDataAdapter dadr = new SqlDataAdapter(cmddr);
        dadr.Fill(dtdr);
        if (dtdr.Rows.Count > 0)
        {
            ddl_mode.DataSource = dtdr;
            ddl_mode.DataValueField = "Pmode_Id";
            ddl_mode.DataTextField = "Payment_Name";
            ddl_mode.DataBind();
            ddl_mode.Items.Insert(0, "Select");
        }
    }
    
    protected void ddl_name_SelectedIndexChanged(object sender, EventArgs e)
    {
        PanelPayDetails.Visible = true;

        c.getcon();
        string strin = "select Payment_Mode,Amount,NumOfInstl from Registration where Enq_Id='" + ddl_name.SelectedItem.Value + "'";
        SqlCommand cmdin = new SqlCommand(strin, c.con);
        DataTable dtin = new DataTable();
        SqlDataAdapter dain = new SqlDataAdapter(cmdin);
        dain.Fill(dtin);
        if (dtin.Rows.Count > 0)
        {
            lbl_instType.Text = dtin.Rows[0][0].ToString();
            lbl_TotPay.Text = dtin.Rows[0][1].ToString();
            if (lbl_instType.Text == "Lumpsum")
            {
                string strlm = "Select * from Registration inner join Payment on Registration.Reg_Id=Payment.Reg_Id where Enq_Id='" + ddl_name.SelectedItem.Value + "'";
                SqlCommand cmdlm = new SqlCommand(strlm, c.con);
                DataTable dtlm = new DataTable();
                SqlDataAdapter dalm = new SqlDataAdapter(cmdlm);
                dalm.Fill(dtlm);
                if (dtlm.Rows.Count > 0)
                {
                    lbl_PendPay.Text = "Paid";
                    lbl_instCP.Text = "Nil";
                    lbl_notPay.Visible = true;
                    lbl_notPay.Text = "Payment Completed";
                }
                else
                {
                    lbl_PendPay.Text = dtin.Rows[0][1].ToString();
                    lbl_instCP.Text = "1";
                    lbl_notPay.Visible = false;
                    lbl_nextinstamt.Visible = true;
                    lbl_pmode.Visible = true;
                    txt_instamt.Visible = true;
                    txt_instamt.Text = dtin.Rows[0][1].ToString();
                    ddl_mode.Visible = true;
                    dropbind();
                }
            }
            else if (lbl_instType.Text == "Installment")
            {
                string strinsl = "Select Payment.Instal_Id, Installment.Inst_Amt from Registration inner join Payment on Registration.Reg_Id=Payment.Reg_Id inner join Installment on Installment.Instal_Id=Payment.Instal_Id where Enq_Id='" + ddl_name.SelectedItem.Value + "'";
                SqlCommand cmdinsl = new SqlCommand(strinsl, c.con);
                DataTable dtinsl = new DataTable();
                SqlDataAdapter dainsl = new SqlDataAdapter(cmdinsl);
                dainsl.Fill(dtinsl);
                int count = dtinsl.Rows.Count;
                if (dtinsl.Rows.Count > 0)
                {
                    if (dtin.Rows[0][2].ToString() == count.ToString())
                    {
                        lbl_PendPay.Text = "Paid";
                        lbl_instCP.Text = "Nil";
                        lbl_notPay.Visible = true;
                        lbl_notPay.Text = "Payment Completed";
                    }
                    else
                    {
                        string str1 = "Select Amount,Instal_No,Inst_Amt from Installment inner join Registration on Installment.Reg_Id=Registration.Reg_Id where Registration.Enq_Id='" + ddl_name.SelectedItem.Value + "'";
                        SqlCommand cmd1 = new SqlCommand(str1, c.con);
                        DataTable dt1 = new DataTable();
                        SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                        da1.Fill(dt1);
                        
                        int pendingpay = 0;
                        if (dt1.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtinsl.Rows.Count; i++)
                            {
                                pendingpay = pendingpay + Convert.ToInt32(dtinsl.Rows[i][1].ToString());
                            }
                            pendingpay = Convert.ToInt32(dtin.Rows[0][1].ToString()) - pendingpay;
                            lbl_PendPay.Text = pendingpay.ToString();
                            lbl_instCP.Text = (count + 1).ToString();
                            lbl_notPay.Visible = false;
                            lbl_nextinstamt.Visible = true;
                            lbl_pmode.Visible = true;
                            txt_instamt.Visible = true;
                            txt_instamt.Text = dt1.Rows[count][2].ToString();
                            ddl_mode.Visible = true;
                            dropbind();
                        }


                    }
                }
                else
                {
                    string str2 = "Select Amount,Instal_No,Inst_Amt from Installment inner join Registration on Installment.Reg_Id=Registration.Reg_Id where Registration.Enq_Id='" + ddl_name.SelectedItem.Value + "'";
                    SqlCommand cmd2 = new SqlCommand(str2, c.con);
                    DataTable dt2 = new DataTable();
                    SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
                    da2.Fill(dt2);
                    lbl_PendPay.Text = dtin.Rows[0][1].ToString();
                    lbl_instCP.Text = "1";
                    lbl_notPay.Visible = false;
                    lbl_nextinstamt.Visible = true;
                    lbl_pmode.Visible = true;
                    txt_instamt.Visible = true;
                    txt_instamt.Text = dt2.Rows[0][2].ToString();
                    ddl_mode.Visible = true;
                    dropbind();
                }
            }
        }
    }
        
    //Binds bank to dropdownlist
    public void dropbank()
    {
        string bank = "Select * from Bank";
        SqlCommand cmdbank = new SqlCommand(bank, c.con);
        DataTable bankdt = new DataTable();
        SqlDataAdapter bankda = new SqlDataAdapter(cmdbank);
        bankda.Fill(bankdt);
        if (bankdt.Rows.Count > 0)
        {
            ddl_bankname.DataSource = bankdt;
            ddl_bankname.DataValueField = "Bank_Id";
            ddl_bankname.DataTextField = "Bank_Name";
            ddl_bankname.DataBind();
        }
        ddl_bankname.Items.Insert(0, "Select");

    }


    //Binds branch to dropdownlist 
    public void dropbranch()
    {
        string br = "Select Branch_Name, Branch_Id from Branch inner join Bank on Bank.Bank_Id=Branch.Bank_Id where Bank.Bank_Id='"+ddl_bankname.SelectedItem.Value+"'";
        SqlCommand cmdbr = new SqlCommand(br, c.con);
        DataTable brdt = new DataTable();
        SqlDataAdapter brda = new SqlDataAdapter(cmdbr);
        brda.Fill(brdt);
        if (brdt.Rows.Count > 0)
        {
            ddl_branch.DataSource = brdt;
            ddl_branch.DataValueField = "Branch_Id";
            ddl_branch.DataTextField = "Branch_Name";
            ddl_branch.DataBind();
        }
        ddl_branch.Items.Insert(0, "Select");

    }

    public void postdatedCheque()
    {
        string strpd = "Select * from Cheque where PostChequeStatus='PostCheque'";
        SqlCommand cmdpd = new SqlCommand(strpd, c.con);
        DataTable dtpd = new DataTable();
        SqlDataAdapter dapd = new SqlDataAdapter(cmdpd);
        dapd.Fill(dtpd);
        if (dtpd.Rows.Count > 0)
        {
            GridView_Cheque.DataSource = dtpd;
            GridView_Cheque.DataBind();
            GridView_Cheque.Visible = true;
            Panel2.Visible = true;
            lbl_notipChq.Visible = false;
        }
        else
        {
            GridView_Cheque.Visible = false;
            Panel2.Visible = false;
            lbl_notipChq.Visible = true;
        }
    }

    protected void ddl_mode_SelectedIndexChanged(object sender, EventArgs e)
    { 
        btn_save.Visible=true;
        
        if (ddl_mode.SelectedItem.Text == "Cash")
        {
            MultiViewPayment.ActiveViewIndex = 0;
        }
        else if (ddl_mode.SelectedItem.Text == "Cheque")
        {
            MultiViewPayment.ActiveViewIndex = 1;
            c.getcon();
            dropbank();
            c.con.Close();
        }
        else if (ddl_mode.SelectedItem.Text == "Postdated Cheque")
        {
            c.getcon();
            postdatedCheque();
            c.con.Close();
        }
        else if (ddl_mode.SelectedItem.Text == "Online Payment")
        {
            MultiViewPayment.ActiveViewIndex = 2;
        }
    }
  
    //Adds Payment details to the database

    protected void btn_add_Click(object sender, EventArgs e)
    {
        c.getcon();
        dropbank();
        
        for (int i = 0; i < GridView_Cheque.Rows.Count; i++)
        {
            CheckBox cb = (CheckBox)GridView_Cheque.Rows[i].Cells[0].FindControl("CheckBoxpchq");
            int id = Convert.ToInt32(GridView_Cheque.DataKeys[i].Values[0]);
            if (cb.Checked == true)
            {
                MultiViewPayment.ActiveViewIndex = 1;
                string strsel = "Select Bank,Branch, Amount,Date,Payer_Name,Remark,Cheque_Id from Cheque inner join Branch on Branch.Branch_Id=Cheque.Branch_Id inner join Bank on Bank.Bank_Id=Branch.Bank_Id where Cheque_Id='" + id + "'";
                SqlCommand cmdsel = new SqlCommand(strsel, c.con);
                DataTable dtsel = new DataTable();
                SqlDataAdapter dasel = new SqlDataAdapter(cmdsel);
                dasel.Fill(dtsel);
                if (dtsel.Rows.Count > 0)
                {
                    ddl_bankname.Items.FindByText(dtsel.Rows[0][0].ToString()).Selected = true;
                    ddl_branch.Items.FindByText(dtsel.Rows[0][1].ToString()).Selected = true;
                    txt_amtcheque.Text = dtsel.Rows[0][2].ToString();
                    txt_date.Text = dtsel.Rows[0][3].ToString();
                    txt_payrname.Text = dtsel.Rows[0][4].ToString();
                    txt_remCheq.Text = dtsel.Rows[0][5].ToString();

                }
            }

            c.con.Close();

        }

    }
    protected void btn_save_Click(object sender, EventArgs e)
    {
        c.getcon();
        string strsel = "Select Instal_Id from Installment where Instal_No='" + Convert.ToInt32(lbl_instCP.Text) + "' and Reg_Id='" + ddl_name.SelectedItem.Value + "'";
        SqlCommand cmdsel = new SqlCommand(strsel,c.con);
        DataTable dtsel = new DataTable();
        SqlDataAdapter dasel = new SqlDataAdapter(cmdsel);
        dasel.Fill(dtsel);

        string strpay = "Insert into Payment values('" + ddl_name.SelectedItem.Value + "','" + ddl_mode.SelectedItem.Value + "','" + txt_RecNo.Text + "','" + txt_RecDate.Text + "','"+dtsel.Rows[0][0]+"')Select @@Identity";
        SqlCommand cmdpay = new SqlCommand(strpay, c.con);
        object obj1 = new object();
        obj1 = cmdpay.ExecuteScalar();
        
        if (ddl_mode.SelectedItem.Text == "Cash")
        {
            string strc = "Insert into cash values ('" + Convert.ToInt32(obj1) + "','" + txt_amt.Text + "','" + txt_remarks.Text + "')";
            SqlCommand cmdc = new SqlCommand(strc, c.con);
            cmdc.ExecuteNonQuery();

        }


        else if (ddl_mode.SelectedItem.Text == "Cheque")
        {
            string strChq = "Insert into Cheque values ('" +ddl_branch.SelectedItem.Value + "','"+ddl_name.SelectedItem.Value+"','"+Convert.ToInt32(obj1)+"','"+txt_amtcheque.Text+"','"+txt_date.Text+"','"+txt_payrname.Text+"','Paid','Not','"+txt_remCheq.Text+"')";
            SqlCommand cmdChq = new SqlCommand(strChq, c.con);
            cmdChq.ExecuteNonQuery();
        }
        else if (ddl_mode.SelectedItem.Text == "Postdated Cheque")
        {
            string strpchq = "Update Cheque set ChequePaidStatus='Paid'";
            SqlCommand cmdpchq = new SqlCommand(strpchq, c.con);
            cmdpchq.ExecuteNonQuery();
        }
        else if (ddl_mode.SelectedItem.Text == "Online Payment")
        {
            string strOP = "Insert into OnlinePayment values ('" + Convert.ToInt32(obj1) + "','" + txt_transac.Text + "','" + txt_accountno.Text + "','" + txt_paydate.Text + "','" + txt_onlineamt.Text + "','" + txt_IFSC.Text + "','" + txt_onlinerem.Text + "')";
            SqlCommand cmdOP = new SqlCommand(strOP, c.con);
            cmdOP.ExecuteNonQuery();
        }
        c.con.Close();
            
     }
    protected void ddl_bankname_SelectedIndexChanged(object sender, EventArgs e)
    {
        c.getcon();
        dropbranch();
        c.con.Close();
    }
}

