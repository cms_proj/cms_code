﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StaffMaster.master" AutoEventWireup="true" CodeFile="PostDatedCheque.aspx.cs" Inherits="PostDatedCheque" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
                    
                         
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table>
        <tr>
            <td style="height: 100px">&nbsp;</td>
            <td>
                &nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:Panel ID="Panel1" runat="server" Height="495px" style="margin-left: 162px" Width="641px" BorderStyle="Solid">
                     <table>
                        <tr>
                            <td colspan="7">
                                <asp:ScriptManager ID="ScriptManager1" runat="server">
                                </asp:ScriptManager>
                            </td>
                        </tr>
                         <tr>
                             <td colspan="7" style="font-size: 38px; font-weight: bold; text-align: center;">Cheque Details</td>
                         </tr>
                         <tr>
                             <td >&nbsp;</td>
                             <td style="font-size: 15px; font-weight: bold">&nbsp;</td>
                             <td>
                                 &nbsp;</td>
                             <td>&nbsp;</td>
                             <td>&nbsp;</td>
                             <td>&nbsp;</td>
                             <td>&nbsp;</td>
                         </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td style="font-size: 15px; font-weight: bold">Choose Name</td>
                            <td>
                                <asp:DropDownList ID="ddl_name" runat="server" CssClass="search_categories">
                                </asp:DropDownList>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td style="font-size: 15px; font-weight: bold">&nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>&nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td style="font-size: 15px; font-weight: bold">Choose Bank</td>
                            <td>
                                <asp:DropDownList ID="ddl_bank" runat="server" AutoPostBack="True" CssClass="search_categories" OnSelectedIndexChanged="ddl_bank_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                            <td style="font-size: 15px; font-weight: bold">Choose Branch</td>
                            <td>
                                <asp:DropDownList ID="ddl_branch" runat="server" AutoPostBack="True" CssClass="search_categories">
                                </asp:DropDownList>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td style="font-size: 15px; font-weight: bold">&nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td style="font-size: 15px; font-weight: bold">&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                      
                            <td>&nbsp;</td>
                      
                         <tr>
                             <td>&nbsp;</td>
                             <td style="font-size: 15px; font-weight: bold">Amount on Cheque</td>
                             <td>
                                 <asp:TextBox ID="txt_ChqAmt" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                             </td>
                             <td>&nbsp;</td>
                             <td>&nbsp;</td>
                             <td>&nbsp;</td>
                             <td></td>
                             <tr>
                                 <td>&nbsp;</td>
                                 <td style="font-size: 15px; font-weight: bold">&nbsp;</td>
                                 <td>&nbsp;</td>
                                 <td>&nbsp;</td>
                                 <td>&nbsp;</td>
                                 <td>&nbsp;</td>
                                 <td></td>
                             </tr>
                             <tr>
                                 <td></td>
                                 <td style="font-size: 15px; font-weight: bold">Cheque Date</td>
                                 <td>
                                     <asp:TextBox ID="txt_ChqDate" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                     <cc1:CalendarExtender ID="txt_ChqDate_CalendarExtender" runat="server" Enabled="True" TargetControlID="txt_ChqDate">
                                     </cc1:CalendarExtender>
                                 </td>
                                 <td</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        
                            </tr>
                             <tr>
                                 <td>&nbsp;</td>
                                 <td style="font-size: 15px; font-weight: bold">&nbsp;</td>
                                 <td>&nbsp;</td>
                                 <td>&nbsp;</td>
                                 <td>&nbsp;</td>
                                 <td>&nbsp;</td>
                                 <td>&nbsp;</td>
                             </tr>
                             <tr>
                                 <td>&nbsp;</td>
                                 <td style="font-size: 15px; font-weight: bold">Payer Name</td>
                                 <td>
                                     <asp:TextBox ID="txt_Payer" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                 </td>
                                 <td>&nbsp;</td>
                                 <td>&nbsp;</td>
                                 <td>&nbsp;</td>
                                 <td>&nbsp;</td>
                             </tr>
                             <tr>
                                 <td>&nbsp;</td>
                                 <td style="font-size: 15px; font-weight: bold">&nbsp;</td>
                                 <td>&nbsp;</td>
                                 <td>&nbsp;</td>
                                 <td>&nbsp;</td>
                                 <td>&nbsp;</td>
                                 <td>&nbsp;</td>
                             </tr>
                             <tr>
                                 <td>&nbsp;</td>
                                 <td style="font-size: 15px; font-weight: bold">Remarks</td>
                                 <td>
                                     <asp:TextBox ID="txt_Rem" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                 </td>
                                 <td>&nbsp;</td>
                                 <td>&nbsp;</td>
                                 <td>&nbsp;</td>
                                 <td>&nbsp;</td>
                             </tr>
                             <tr>
                                 <td style="width: 100px">&nbsp;</td>
                                 <td>&nbsp;</td>
                                 <td>&nbsp;</td>
                                 <td>&nbsp;</td>
                                 <td>&nbsp;</td>
                                 <td>&nbsp;</td>
                                 <td>&nbsp;</td>
                             </tr>
                             <tr>
                                 <td >&nbsp;</td>
                                 <td colspan="4">
                                     <asp:Button ID="btn_save" runat="server" CssClass="btnme" OnClick="btn_save_Click" Text="Save" />
                                     <asp:Button ID="btn_cancel" runat="server" CssClass="btnme" OnClick="btn_cancel_Click" Text="Cancel" />
&nbsp;</td>
                                 <td>&nbsp;</td>
                                 <td>
                                     <br />
                                     <br />
                                 </td>
                             </tr>
                         </tr>
                    </table>
                </asp:Panel>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Panel ID="Panel2" runat="server" Height="368px" style="margin-left: 165px" Width="634px" BorderStyle="Ridge">
                    <div>
                        <div class="text-center" style="font-size: 20px; font-weight: bold">
                            
                            <br />
                            All Cheque&nbsp; Details<br />
                            <br />
                            
                        </div>
                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="3" GridLines="Vertical" style="text-align: center" DataKeyNames="Cheque_Id" OnRowDeleting="GridView1_RowDeleting" BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" Height="194px" Width="631px">
                            <AlternatingRowStyle BackColor="#DCDCDC" />
                            <Columns>
                                <asp:BoundField DataField="Name" HeaderText="Intern Name" />
                                <asp:BoundField DataField="Amount" HeaderText="Amount" />
                                <asp:BoundField DataField="Date" HeaderText="Cheque Date" />
                                <asp:BoundField DataField="Payer_Name" HeaderText="Payer Name" />
                                <asp:BoundField DataField="Remark" HeaderText="Remark" />
                                <asp:TemplateField></asp:TemplateField>
                                <asp:CommandField ShowDeleteButton="True" />
                            </Columns>
                            <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                            <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                            <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                            <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                            <SortedAscendingHeaderStyle BackColor="#0000A9" />
                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                            <SortedDescendingHeaderStyle BackColor="#000065" />
                        </asp:GridView>
                    </div>
                </asp:Panel>
                &nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="height: 100px; width: 100px">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
</asp:Content>

