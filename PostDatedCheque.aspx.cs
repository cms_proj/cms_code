﻿//Code to add Postdated cheque details

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class PostDatedCheque : System.Web.UI.Page
{
    connection c = new connection();
   
    //Code to bind postdated cheque datas to grid
    public void gridbind()
    {
        string gb = "Select Enquiry.Name,Cheque.Amount,Date,Payer_Name,Remark,Cheque_Id from Cheque inner join Registration on Cheque.Reg_Id=Registration.Reg_Id inner join Enquiry on Registration.Enq_Id=Enquiry.Enq_Id where PostChequeStatus='PostCheque'";
        SqlCommand cmd = new SqlCommand(gb, c.con);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }

    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Login_Id"] == null)
                Response.Redirect("Home.aspx");

            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache,no-store,max-age=0,must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }

            //Bind name to dropdownlist
            
            c.getcon();
            string nam = "Select * from Enquiry inner join Registration on Enquiry.Enq_Id=Registration.Enq_Id";
            SqlCommand cmdnam = new SqlCommand(nam, c.con);
            DataTable namdt = new DataTable();
            SqlDataAdapter namda = new SqlDataAdapter(cmdnam);
            namda.Fill(namdt);
            if (namdt.Rows.Count > 0)
            {
                ddl_name.DataSource = namdt;
                ddl_name.DataValueField = "Reg_Id";
                ddl_name.DataTextField = "Name";
                ddl_name.DataBind();
            }
            ddl_name.Items.Insert(0, "Select");
            dropbank();
            gridbind();
            c.con.Close();        
        }
    }

    //Bind branch to dropdownlist
    public void dropbranch()
    {
        string br = "Select Branch_Name,Branch_Id from Branch inner join Bank on Bank.Bank_Id=Branch.Bank_Id where Bank.Bank_Id='" + ddl_bank.SelectedItem.Value + "'";
        SqlCommand cmdbr = new SqlCommand(br, c.con);
        DataTable brdt = new DataTable();
        SqlDataAdapter brda = new SqlDataAdapter(cmdbr);
        brda.Fill(brdt);
        if (brdt.Rows.Count > 0)
        {
            ddl_branch.DataSource = brdt;
            ddl_branch.DataValueField = "Branch_Id";
            ddl_branch.DataTextField = "Branch_Name";
            ddl_branch.DataBind();
        }
        ddl_branch.Items.Insert(0, "Select");

    }

    //Bind bank to dropdownlist
    public void dropbank()
    {
        string bank = "Select * from Bank";
        SqlCommand cmdbank = new SqlCommand(bank, c.con);
        DataTable bankdt = new DataTable();
        SqlDataAdapter bankda = new SqlDataAdapter(cmdbank);
        bankda.Fill(bankdt);
        if (bankdt.Rows.Count > 0)
        {
            ddl_bank.DataSource = bankdt;
            ddl_bank.DataValueField = "Bank_Id";
            ddl_bank.DataTextField = "Bank_Name";
            ddl_bank.DataBind();
        }
        ddl_bank.Items.Insert(0, "Select");

    }
    protected void ddl_bank_SelectedIndexChanged(object sender, EventArgs e)
    {
       c.getcon();
       dropbranch();
       c.con.Close();
    }

    //Adds cheque details to table
    protected void btn_save_Click(object sender, EventArgs e)
    {
        c.getcon();
        string insert = "Insert into Cheque values('"+ddl_branch.SelectedItem.Value+"','"+ddl_name.SelectedItem.Value+"','','"+txt_ChqAmt.Text+"','"+txt_ChqDate.Text+"','"+txt_Payer.Text+"','Not Paid','PostCheque','"+txt_Rem.Text+"')";
        SqlCommand cmd = new SqlCommand(insert, c.con);
        cmd.ExecuteNonQuery();
        c.con.Close();
    }

    protected void btn_cancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("StaffHome.aspx");
    }
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        c.getcon();

    }
}