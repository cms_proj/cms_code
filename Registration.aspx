﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StaffMaster.master" AutoEventWireup="true" CodeFile="Registration.aspx.cs" Inherits="Registration" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title></title>
    
    <style type="text/css">
        .modalBackground
    {
        background-color: Black;
        filter: alpha(opacity=60);
        opacity: 0.6;
    }
    .modalPopup
    {
        background-color: #FFFFFF;
        border: 3px solid #0DA9D0;
        border-radius: 12px;
        padding:0
        }
    .modalPopup .header
    {
        background-color: #2FBDF1;
        height: 30px;
        color: White;
        line-height: 30px;
        text-align: center;
        font-weight: bold;
        border-top-left-radius: 6px;
        border-top-right-radius: 6px;
    }
    .modalPopup .body
    {
        min-height: 50px;
        line-height: 30px;
        text-align: center;
        font-weight: bold;
    }
    .modalPopup .footer
    {
        padding: 6px;
    }
    .modalPopup .yes
    {
        background-color: #2FBDF1;
        border: 1px solid #0DA9D0;
    }
        .auto-style5 {
            height: 20px;
        }
    </style>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <form >
    <div>
    
        
    
        <table class="nav-justified">
            <tr>
                <td ></td>
                <td style="width: 20px; height: 100px"></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td >
                    <asp:Panel ID="Panel1" runat="server" Height="1329px" BorderStyle="Solid">
                        <table>
            <tr>
                <td class="text-center" colspan="6" style="height: 50px" >&nbsp;</td>
            </tr>
                            <tr>
                                <td class="text-center" colspan="6"><strong class="auto-style7" style="font-family: Arial, Helvetica, sans-serif; font-size: 39px; font-weight: bold; color: #000099; text-align: center;">New&nbsp; Registration</strong></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td colspan="4">&nbsp;</td>
                            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td><strong>Choose Name</strong></td>
                <td>&nbsp;</td>
                <td>
                    <asp:DropDownList ID="ddl_name" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddl_name_SelectedIndexChanged" CssClass="search_categories">
                    </asp:DropDownList>
                </td>
                <td style="font-weight: bold">
                    Enquiry ID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:Label ID="lbl_enqid" runat="server"></asp:Label>
                </td>
            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td><strong>Registration Date</strong></td>
                <td>&nbsp;</td>
                <td>
                    <asp:TextBox ID="txt_date" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                    <cc1:CalendarExtender ID="txt_date_CalendarExtender" runat="server" Enabled="True" TargetControlID="txt_date">
                    </cc1:CalendarExtender>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td><strong>Duration</strong> </td>
                <td>&nbsp;</td>
                <td>
                    <asp:TextBox ID="txt_Dur" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="req_duration" runat="server" ErrorMessage="*Required" Font-Bold="False" Font-Names="Arial" ForeColor="Red" ControlToValidate="txt_Dur" ValidationGroup="eq"></asp:RequiredFieldValidator>
                </td>
                <td>
                    </td>
            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td><asp:Button ID ="ButDummy" runat="server" style =" display:none;" Text="Button"/>
                                     <cc1:ModalPopupExtender ID="btn_register_ModalPopupExtender" runat="server" BackgroundCssClass="modalBackground" DynamicServicePath="" Enabled="True" PopupControlID="Panel_Popup" TargetControlID="btn_register" RepositionMode="None">
                    </cc1:ModalPopupExtender>


                                </td>
                            </tr>
            <tr>
                <td >&nbsp;</td>
                <td >&nbsp;</td>
                <td><strong>Choose Course</strong> </td>
                <td>&nbsp;</td>
                <td >
                    <asp:DropDownList ID="ddl_course" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddl_course_SelectedIndexChanged" CssClass="search_categories">
                    </asp:DropDownList>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="req_course" runat="server" Font-Bold="False" Font-Names="Arial" ForeColor="Red" Text="Select" Visible="False"></asp:Label>
                </td>
                <td>
                    </td>
            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
            <tr>
                <td >&nbsp;</td>
                <td >&nbsp;</td>
                <td><strong>Opted Technology</strong></td>
                <td>&nbsp;</td>
                <td >
                    <asp:DropDownList ID="ddl_opt" runat="server" CssClass="search_categories">
                    </asp:DropDownList>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="req_tech" runat="server" Font-Bold="False" ForeColor="Red" Text="Select" Visible="False"></asp:Label>
                </td>
                <td>
                    </td>
            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
            <tr>
                <td class="auto-style6"></td>
                <td class="auto-style6"></td>
                <td class="auto-style6"><strong>Specify Tution Fee</strong>&nbsp;</td>
                <td class="auto-style6">&nbsp;</td>
                <td class="auto-style6">
                    <asp:TextBox ID="txt_fee" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="req_fee" runat="server" ErrorMessage="*Required" ForeColor="Red" ValidationGroup="eq" ControlToValidate="txt_fee"></asp:RequiredFieldValidator>
                </td>
                <td class="auto-style6" >
                    <b></b></td>
            </tr>
            <tr>
                <td ></td>
                <td ></td>
                <td><strong>Choose Payment Mode</strong></td>
                <td>&nbsp;</td>
                <td >
                    <asp:RadioButtonList ID="rbl_paymnt" runat="server" AutoPostBack="True" RepeatDirection="Horizontal" OnSelectedIndexChanged="rbl_paymnt_SelectedIndexChanged" CssClass="twitterStyleTextbox">
                        <asp:ListItem>Lumpsum</asp:ListItem>
                        <asp:ListItem>Installment</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td >
                    <asp:Label ID="lbl_due" runat="server" Font-Bold="True" Text="Due Date" Visible="False"></asp:Label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:TextBox ID="txt_duedate" runat="server" CssClass="twitterStyleTextbox" Visible="False"></asp:TextBox>
                    <cc1:CalendarExtender ID="txt_duedate_CalendarExtender" runat="server" Enabled="True" TargetControlID="txt_duedate">
                    </cc1:CalendarExtender>
                    </td>
            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
                                <td>&nbsp;</td>
                            </tr>
            <tr>
                <td style="width: 100px" ></td>
                <td ></td>
                <td style="width: 300px" ></td>
                <td colspan="2">
                    <asp:Panel ID="Panel_Instal" runat="server" BorderColor="#663300" BorderStyle="Double" Height="400px" style="margin-right: 10px" Visible="False" Width="489px">
                        &nbsp;<table>
                            <tr>
                                <td>&nbsp;</td>
                                <td>
                                    <table>
                                        <tr>
                                            <td>No of Installments&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
                                            <td>
                                                <asp:TextBox ID="txt_instno" runat="server" CssClass="twitterStyleTextbox" OnTextChanged="txt_instno_TextChanged"></asp:TextBox>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:Button ID="btn_OK" runat="server" CssClass="btnme" OnClick="btn_OK_Click" Text="OK" />
                                            </td>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>
                                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" Font-Bold="True" Height="229px" HorizontalAlign="Center" OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating" ShowFooter="True" Width="471px">
                                        <AlternatingRowStyle BackColor="#DCDCDC" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="No">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("InstNo") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Installment Amount">
                                                <FooterTemplate>
                                                    <asp:TextBox ID="txt_InstAmtgrid" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                                </FooterTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("InstAmount") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Last Date">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("InstDate") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox ID="txt_Lastdategrid" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                                    <cc1:CalendarExtender ID="txt_Lastdategrid_CalendarExtender" runat="server" Enabled="True" TargetControlID="txt_Lastdategrid">
                                                    </cc1:CalendarExtender>
                                                </FooterTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label4" runat="server" Text='<%# Bind("InstDate") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <asp:Button ID="btn_add" runat="server" OnClick="btn_add_Click" Text="Add" />
                                                    &nbsp;
                                                </FooterTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label1" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                        <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
                                        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                        <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                                        <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                        <SortedAscendingHeaderStyle BackColor="#0000A9" />
                                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                        <SortedDescendingHeaderStyle BackColor="#000065" />
                                    </asp:GridView>
                                </td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td>
                    </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td></td>
                <td>&nbsp;</td>
                <td>
                    </td>
                <td>
                    </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td><strong>Course Start Date<br /> </strong></td>
                <td>&nbsp;</td>
                <td>
                    <strong>
                    <asp:TextBox ID="txt_startdate" runat="server" CssClass="twitterStyleTextbox" Width="145px"></asp:TextBox>
                    <cc1:CalendarExtender ID="txt_startdate_CalendarExtender" runat="server" Enabled="True" TargetControlID="txt_startdate">
                    </cc1:CalendarExtender>
                    </strong>
                </td>
                <td>
                    <strong>Course End Date</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:TextBox ID="txt_endate" runat="server" style="margin-left: 0px" CssClass="twitterStyleTextbox"></asp:TextBox>
                    <cc1:CalendarExtender ID="txt_endate_CalendarExtender" runat="server" Enabled="True" TargetControlID="txt_endate">
                    </cc1:CalendarExtender>
                </td>
            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td><strong>Remarks</strong></td>
                <td>&nbsp;</td>
                <td>
                    <asp:TextBox ID="txt_remark" runat="server" TextMode="MultiLine" CssClass="twitterStyleTextbox" ></asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    <br />
                    <strong>Choose Photo </strong>
                    <br />
                </td>
                <td>&nbsp;</td>
                <td>
                    <asp:FileUpload ID="FileUpload1" runat="server" style="margin-left: 0px" CssClass="twitterStyleTextbox" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    
                    <asp:RequiredFieldValidator ID="req_photo" runat="server" ErrorMessage="*Choose Photo" ForeColor="Red" ControlToValidate="FileUpload1" ValidationGroup="eq"></asp:RequiredFieldValidator>
                    
                </td>
                <td>
                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
                    </td>
            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td style="width: 1000px">&nbsp;</td>
                            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>
                    <asp:Button ID="btn_register" runat="server" Text="Register" OnClick="btn_register_Click" ValidationGroup="eq" CssClass="btnme" />
                   
                </td>
                <td>
                    &nbsp;</td>
            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>
                                    <asp:Panel ID="Panel_Popup" runat="server" Height="139px" BorderStyle="Solid" CssClass="modalPopup" Visible="False" Width="296px">
                                                                    <div>
                                                                        <br />
                                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                        <asp:Label ID="Lbl_added" runat="server" CssClass="modal-body" Font-Bold="True" Text="Registered Successfully!!!"></asp:Label>
                                                                        <br />
                                                                        &nbsp;&nbsp;<br /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                        <asp:Button ID="btb_OK" runat="server" Text="OK" OnClick="btb_OK_Click" CssClass="btnme" />
                                                                        <br />
                                                                        <br />
                                                                        
                                                                    </div>
                                                                </asp:Panel>
                                </td>
                                <td>&nbsp;</td>
                            </tr>
        </table>
                    </asp:Panel>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td style="height: 100px">&nbsp;</td>
                <td >
                    &nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
    
        
    
    </div>
  </form>
    </asp:Content>

