﻿
//Staff can register student based on the mark obtained in the entrance test.Those who passed in the test will only appears in the list

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;

public partial class Registration : System.Web.UI.Page
{
    connection c = new connection();
    
    
    public void gridbind()
    {
        DataTable dt = (DataTable)Session["DTTabl"];
        if (dt.Rows.Count > 0)
        {
            GridView1.Visible = true;
            GridView1.DataSource = dt;
            Session["Gv"] = dt;
            GridView1.DataBind();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        this.UnobtrusiveValidationMode = System.Web.UI.UnobtrusiveValidationMode.None;

        if (!IsPostBack)
        {
            if (Session["Login_Id"] == null)
                Response.Redirect("Home.aspx");

            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache,no-store,max-age=0,must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }

            GridView1.Visible = false;
            //DataTable dtdd = new DataTable();
            //dtdd.Columns.Add("InstNo");
            //dtdd.Columns.Add("InstAmount");
            //dtdd.Columns.Add("InstDate");
            //Session["Instal"] = dtdd;


            //Binds Name to dropdownlist
            c.getcon();
            string nam = "Select * from Enquiry inner join InitialEvaluation on Enquiry.Enq_Id=InitialEvaluation.Enq_Id where InitialEvaluation.Status='Passed' and Enquiry.Status='Not Registered'";
            SqlCommand cmdnam = new SqlCommand(nam, c.con);
            DataTable namdt = new DataTable();
            SqlDataAdapter namda = new SqlDataAdapter(cmdnam);
            namda.Fill(namdt);
            if (namdt.Rows.Count > 0)
            {
                ddl_name.DataSource = namdt;
                ddl_name.DataValueField = "Enq_Id";
                ddl_name.DataTextField = "Name";
                ddl_name.DataBind();
            }
            ddl_name.Items.Insert(0, "Select");
            c.con.Close();

            //Binds Course to dropdownlist
            c.getcon();
            string cou = "Select * from Course";
            SqlCommand cmdco = new SqlCommand(cou, c.con);
            DataTable codt = new DataTable();
            SqlDataAdapter coda = new SqlDataAdapter(cmdco);
            coda.Fill(codt);
            if (codt.Rows.Count > 0)
            {
                ddl_course.DataSource = codt;
                ddl_course.DataValueField = "Course_Id";
                ddl_course.DataTextField = "Course";
                ddl_course.DataBind();
            }
            ddl_course.Items.Insert(0, "Select");
            c.con.Close();

     

        }
    }
    protected void ddl_name_SelectedIndexChanged(object sender, EventArgs e)
    {
        lbl_enqid.Text = ddl_name.SelectedItem.Value;
    }
    
    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {

    }
    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }
    protected void rbl_paymnt_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rbl_paymnt.SelectedItem.Value == "Installment")
        {
            Panel_Instal.Visible = true;
        }
        if (rbl_paymnt.SelectedItem.Value == "Lumpsum")
        {
            Panel_Instal.Visible = false;
            lbl_due.Visible = true;
            txt_duedate.Visible = true;
            txt_instno.Text ="1";
        }
    }
    protected void btn_register_Click(object sender, EventArgs e)
    {
        
        //To upload photo of the student

        string image;
        string path = "";
        if (FileUpload1.HasFile)
        {
            image = FileUpload1.FileName;
            if (Directory.Exists(Server.MapPath("~/images1")))
            {
                FileUpload1.SaveAs(Server.MapPath("~/images1/" + image));
            }
            else
            {
                Directory.CreateDirectory(Server.MapPath("~/images1"));
                FileUpload1.SaveAs(Server.MapPath("~/images1/" + image));
            }
            path = "~/images1/" + image;
            if (ddl_course.SelectedIndex > 0 && ddl_opt.SelectedIndex > 0)
            {
                req_course.Visible = false;
                req_tech.Visible = false;
                c.getcon();
                string s = "Insert into Registration values( '" + ddl_name.SelectedItem.Value + "','" + path + "','" + txt_date.Text + "','" + txt_Dur.Text + "','" + ddl_opt.SelectedItem.Value + "','" + txt_fee.Text + "','" + rbl_paymnt.SelectedItem.Text + "','','" + txt_instno.Text + "','" + txt_startdate.Text + "','" + txt_endate.Text + "','Active','')Select @@Identity";
                SqlCommand cmd = new SqlCommand(s, c.con);
                object obj = new object();
                obj = cmd.ExecuteScalar();
                if (rbl_paymnt.SelectedItem.Value == "Lumpsum")
                {
                    string inst = "Insert into Installment values('" + Convert.ToInt32(obj) + "','1','"+txt_duedate.Text+"','"+txt_fee.Text+"')";
                    SqlCommand cmdin = new SqlCommand(inst, c.con);
                    cmdin.ExecuteNonQuery();
                    string update = "Update Enquiry set Status='Registered' where Enq_Id='" + Convert.ToInt32(obj) + "'";
                    SqlCommand cmdup = new SqlCommand(update, c.con);
                    cmdup.ExecuteNonQuery();
                    c.con.Close();
                }
                else
                {
                    DataTable dtgv = (DataTable)Session["Gv"];
                    for (int i = 0; i < dtgv.Rows.Count; i++)
                    {
                        string inst = "Insert into Installment values('" + Convert.ToInt32(obj) + "','" + dtgv.Rows[i][0] + "','" + dtgv.Rows[i][2] + "','" + dtgv.Rows[i][1] + "')";
                        SqlCommand cmdst = new SqlCommand(inst, c.con);
                        cmdst.ExecuteNonQuery();
                        string update = "Update Enquiry set Status='Registered' where Enq_Id='" + Convert.ToInt32(obj) + "'";
                        SqlCommand cmdup = new SqlCommand(update, c.con);
                        cmdup.ExecuteNonQuery();
                    }
                }
                Panel_Popup.Visible = true;
                btn_register_ModalPopupExtender.Show();
                c.con.Close();
            }
            else
            {
                req_course.Visible = true;
                req_tech.Visible = true;
            }
        }
    }
    protected void ddl_course_SelectedIndexChanged(object sender, EventArgs e)
    {
        // Binds Technology to dropdownlist

        c.getcon();
        string tec = "Select * from OptedTech inner join Technology on Technology.Tech_Id = OptedTech.Tech_Id where Course_Id ='"+ddl_course.SelectedItem.Value+"'";
        SqlCommand cmdtec = new SqlCommand(tec, c.con);
        DataTable tecdt = new DataTable();
        SqlDataAdapter tecda = new SqlDataAdapter(cmdtec);
        tecda.Fill(tecdt);
        if (tecdt.Rows.Count > 0)
        {
            ddl_opt.DataSource = tecdt;
            ddl_opt.DataValueField = "OpTech_Id";
            ddl_opt.DataTextField = "Tech";
            ddl_opt.DataBind();
        }
        ddl_opt.Items.Insert(0, "Select");
    }
    protected void txt_instno_TextChanged(object sender, EventArgs e)
    {
        if (Convert.ToInt32(txt_instno.Text)>1)
        {
            GridView1.Visible = true;
        }
    }
    protected void btn_add_Click(object sender, EventArgs e)
    {
        string txt_amount = (GridView1.FooterRow.FindControl("txt_InstAmtgrid") as TextBox).Text;
        string txt_date = (GridView1.FooterRow.FindControl("txt_Lastdategrid") as TextBox).Text;
        DataTable dtdd =  (DataTable)Session["DTTabl"];
        int gridcount = GridView1.Rows.Count;
        DataRow dr = dtdd.NewRow();
        dr["InstNo"] = dtdd.Rows.Count;
        dr["InstAmount"] = txt_amount;
        dr["InstDate"] = txt_date;
        dtdd.Rows.Add(dr);
        Session["DTTabl"] = dtdd;
        gridbind();

        
      }
    protected void btn_OK_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("InstNo");
        dt.Columns.Add("InstAmount");
        dt.Columns.Add("InstDate");
        
        DataRow dr = dt.NewRow();
        dr["InstNo"] = "";
        dr["InstAmount"] = "";
        dr["InstDate"] = "";
        dt.Rows.Add(dr);
        Session["DTTabl"] = dt;
        gridbind();
    }

    protected void btb_OK_Click(object sender, EventArgs e)
    {
        Response.Redirect("Registration.aspx");
    }
}