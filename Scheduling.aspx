﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StaffMaster.master" AutoEventWireup="true" CodeFile="Scheduling.aspx.cs" Inherits="Scheduling" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
     
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form >
        
        <table class="nav-justified">
            <tr>
                <td style="height: 100px">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <asp:Panel ID="Panel1" runat="server" BorderStyle="Solid" Width="780px">
                        <table>
            <tr>
                <td ></td>
                <td >
                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
                </td>
                <td ></td>
                <td ></td>
            </tr>
                            <tr>
                                <td class="text-center" colspan="4" style="font-size: 39px; font-family: 'Century Gothic'; font-weight: bold; color: #000099">Schedule Student</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td style="width: 400px">&nbsp;</td>
                                <td style="height: 30px">&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
            <tr>
                <td>&nbsp;</td>
                <td style="font-size: 15px; font-weight: bold">Choose Student Name</td>
                <td>
                    <asp:DropDownList ID="ddl_name" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddl_name_SelectedIndexChanged" CssClass="search_categories">
                    </asp:DropDownList>
                </td>
                <td></td>
            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
            <tr>
                <td>&nbsp;</td>
                <td style="font-size: 15px; font-weight: bold">Course</td>
                <td>
                    <asp:Label ID="lbl_course" runat="server"></asp:Label>
                </td>
                <td>
                    </td>
            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
            <tr>
                <td>&nbsp;</td>
                <td style="font-size: 15px; font-weight: bold">Opted Technology</td>
                <td>
                    <asp:Label ID="lbl_optech" runat="server"></asp:Label>
                </td>
                <td>
                    </td>
            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
            <tr>
                <td>&nbsp;</td>
                <td style="font-size: 15px; font-weight: bold">Select Lab</td>
                <td>
                    <asp:DropDownList ID="ddl_lab" runat="server" AutoPostBack="False" CssClass="search_categories">
                    </asp:DropDownList>
                </td>
                <td>
                    </td>
            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
            <tr>
                <td>&nbsp;</td>
                <td style="font-size: 15px; font-weight: bold">Time Schedule</td>
                <td>
                    <asp:DropDownList ID="ddl_time" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddl_time_SelectedIndexChanged" CssClass="search_categories">
                    </asp:DropDownList>
                </td>
                <td></td>
            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
            <tr>
                <td ></td>
                <td style="font-size: 15px; font-weight: bold">Schedule Date</td>
                <td style="width: 200px">From&nbsp;&nbsp;
                    <asp:TextBox ID="txt_From" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                    <cc1:CalendarExtender ID="txt_From_CalendarExtender" runat="server" Enabled="True" TargetControlID="txt_From">
                    </cc1:CalendarExtender>
                    </td>
                <td style="width: 200px">To&nbsp;&nbsp;
                    <asp:TextBox ID="txt_To" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                    <cc1:CalendarExtender ID="txt_To_CalendarExtender" runat="server" Enabled="True" TargetControlID="txt_To">
                    </cc1:CalendarExtender>
                </td>
            </tr> 
                             <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
            <tr>
                <td>&nbsp;</td>
                <td style="font-size: 15px; font-weight: bold">Systems Available</td>
                <td>
                    <asp:DropDownList ID="ddl_system" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddl_system_SelectedIndexChanged" CssClass="search_categories">
                    </asp:DropDownList>
                &nbsp;&nbsp;
                    <asp:Label ID="Label1" runat="server" ForeColor="Red" Visible="False"></asp:Label>
                </td>
                <td></td>
            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td style="height: 30px">&nbsp;</td>
                            </tr>
            <tr>
                <td colspan="4">
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" DataKeyNames="User_Id" Font-Bold="True" GridLines="Vertical" Height="201px" HorizontalAlign="Center" Width="528px">
                        <AlternatingRowStyle BackColor="#DCDCDC" />
                        <Columns>
                            <asp:BoundField DataField="Name" HeaderText="UserName" />
                            <asp:BoundField DataField="System_Name" HeaderText="System" />
                            <asp:BoundField DataField="Schedule" HeaderText="TimeSchedule" />
                            <asp:BoundField DataField="From_Date" HeaderText="From Date" />
                            <asp:BoundField DataField="To_Date" HeaderText="To Date" />
                            <asp:BoundField DataField="Status" HeaderText="Status" />
                        </Columns>
                        <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                        <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                        <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                        <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                        <SortedAscendingHeaderStyle BackColor="#0000A9" />
                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                        <SortedDescendingHeaderStyle BackColor="#000065" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td></td>
                <td>
                    &nbsp;</td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td style="width: 100px">&nbsp;</td>
                <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btn_submit" runat="server" CssClass="btnme" OnClick="btn_submit_Click" Text="Submit" />
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td colspan="3">&nbsp;</td>
            </tr>
        </table>
                    </asp:Panel>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td style="width: 200px; height: 100px;">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
        
    </form>
</asp:Content>

