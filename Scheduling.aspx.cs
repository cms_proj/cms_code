﻿// This code helps to schedule lab,time schedule and system for each student.It is based om the availability of labs and systems.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Scheduling : System.Web.UI.Page
{
    connection c = new connection();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            if (Session["Login_Id"] == null)
                Response.Redirect("Home.aspx");

            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache,no-store,max-age=0,must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
            //Binds Nameto dropdownlist
            c.getcon();
            string nam = "Select * from Enquiry inner join Registration on Enquiry.Enq_Id=Registration.Enq_Id";
            SqlCommand cmdnam = new SqlCommand(nam, c.con);
            DataTable namdt = new DataTable();
            SqlDataAdapter namda = new SqlDataAdapter(cmdnam);
            namda.Fill(namdt);
            if (namdt.Rows.Count > 0)
            {
                ddl_name.DataSource = namdt;
                ddl_name.DataValueField = "Reg_Id";
                ddl_name.DataTextField = "Name";
                ddl_name.DataBind();
            }
            ddl_name.Items.Insert(0, "Select");
            c.con.Close();
            BindLab();
            BindTimeSch();


        }
    }

    //Binds Lab to dropdownlist

    public void BindLab()
    {
        
        c.getcon();
        string lab = "Select * from Lab";
        SqlCommand cmdlab = new SqlCommand(lab, c.con);
        DataTable labdt = new DataTable();
        SqlDataAdapter labda = new SqlDataAdapter(cmdlab);
        labda.Fill(labdt);
        if (labdt.Rows.Count > 0)
        {
            ddl_lab.DataSource = labdt;
            ddl_lab.DataValueField = "Lab_Id";
            ddl_lab.DataTextField = "Lab_Name";
            ddl_lab.DataBind();
        }
        ddl_lab.Items.Insert(0, "Select");
        c.con.Close();
    }

    //Binds Time Schedules to dropdownlist.Like Mon-Wed, Thur-Sat ,Mon-Fri etc..

    public void BindTimeSch()
    {
        c.getcon();
        string time = "Select * from TimeSchedule";
        SqlCommand cmdtime = new SqlCommand(time, c.con);
        DataTable timedt = new DataTable();
        SqlDataAdapter timeda = new SqlDataAdapter(cmdtime);
        timeda.Fill(timedt);
        if (timedt.Rows.Count > 0)
        {
            ddl_time.DataSource = timedt;
            ddl_time.DataValueField = "TimeSch_Id";
            ddl_time.DataTextField = "Schedule";
            ddl_time.DataBind();
        }
        ddl_time.Items.Insert(0, "Select");
        c.con.Close();
    }

    public void gridbind(DataTable ds)
    {
        DataTable dtsx = new DataTable();
        dtsx = ds;
        if (dtsx.Rows.Count > 0)
        {
            GridView1.DataSource = dtsx;
            GridView1.DataBind();
            GridView1.Visible = true;
        }
        else
        {
            GridView1.Visible = false;
        }
    }
    protected void ddl_name_SelectedIndexChanged(object sender, EventArgs e)
    {
        c.getcon();
        string s = "Select c.Course,t.Tech from Registration r inner join OptedTech o on r.OpTech_Id=o.OpTech_Id inner join Course c on c.Course_Id=o.Course_Id inner join Technology t on t.Tech_Id=o.Tech_Id";
        SqlCommand cmdnam = new SqlCommand(s, c.con);
        DataTable namdt = new DataTable();
        SqlDataAdapter namda = new SqlDataAdapter(cmdnam);
        namda.Fill(namdt);
        if (namdt.Rows.Count > 0)
        {
            string course = namdt.Rows[0][0].ToString();
            string tech = namdt.Rows[0][1].ToString();
            lbl_course.Text = course;
            lbl_optech.Text = tech;

        }
        ddl_name.Items.Insert(0, "Select");
        c.con.Close();

    }

    //Adds the schedules to the UserSchedule table at database

    protected void btn_submit_Click(object sender, EventArgs e)
    {
        c.getcon();
        string strin = "Insert into UserSchedule values('" + ddl_system.SelectedItem.Value + "', '" + ddl_name.SelectedItem.Value + "','" + ddl_time.SelectedItem.Value + "', '" + txt_From.Text + "', '" + txt_To.Text + "', 'Allocated')";
        SqlCommand cmdin = new SqlCommand(strin, c.con);
        cmdin.ExecuteNonQuery();
       
        c.con.Close();
    }
    protected void ddl_system_SelectedIndexChanged(object sender, EventArgs e)
    {
        c.getcon();
        string strsel = "Select * from UserSchedule where System_Id ='" + ddl_system.SelectedItem.Value + "'";
        SqlCommand cmdsel = new SqlCommand(strsel, c.con);
        DataTable dtsel = new DataTable();
        SqlDataAdapter dasel = new SqlDataAdapter(cmdsel);
        dasel.Fill(dtsel);
        if (dtsel.Rows.Count > 0)
        {
            Label1.Text = "Seleted system is Already in the Scheduled List Please Verify the Date Before Allocating";
            Label1.Visible = true;
        }
        else
        {
            Label1.Visible = false;
        }
    }
    protected void ddl_time_SelectedIndexChanged(object sender, EventArgs e)
    {
        c.getcon();
        string str4 = "SELECT System_Id, System_Name FROM System WHERE NOT EXISTS (SELECT * FROM UserSchedule WHERE System.System_Id = UserSchedule.System_Id) AND Lab_Id='" + ddl_lab.SelectedItem.Value + "'";
        SqlCommand cmd4 = new SqlCommand(str4, c.con);
        DataTable dt4 = new DataTable();
        SqlDataAdapter da4 = new SqlDataAdapter(cmd4);
        da4.Fill(dt4);

        string str5 = "SELECT System.System_Id, System_Name, Schedule, From_Date, To_Date, UserSchedule.Status, User_Id FROM System inner join UserSchedule on System.System_Id= UserSchedule.System_Id inner join TimeSchedule on TimeSchedule.TimeSch_Id = UserSchedule.TimeSch_Id inner join Registration on Registration.Reg_Id = UserSchedule.Reg_Id where UserSchedule.Status = 'Free' AND UserSchedule.TimeSch_Id='" + ddl_time.SelectedItem.Value + "' AND Lab_Id='" + ddl_lab.SelectedItem.Value + "'";
        SqlCommand cmd5 = new SqlCommand(str5, c.con);
        DataTable dt5 = new DataTable();
        SqlDataAdapter da5 = new SqlDataAdapter(cmd5);
        da5.Fill(dt5);
        DataTable dtx = new DataTable();
        dtx = dt5;
        string str6 = "SELECT System.System_Id, System_Name, Schedule, From_Date, To_Date, UserSchedule.Status, User_Id,Name  FROM System inner join UserSchedule on System.System_Id= UserSchedule.System_Id inner join TimeSchedule on TimeSchedule.TimeSch_Id = UserSchedule.TimeSch_Id inner join Registration on Registration.Reg_Id = UserSchedule.Reg_Id inner join Enquiry e on e.Enq_Id=Registration.Enq_Id where  Lab_Id='" + ddl_lab.SelectedItem.Value + "' AND NOT UserSchedule.TimeSch_Id='" + ddl_time.SelectedItem.Value + "'";
        SqlCommand cmd6 = new SqlCommand(str6, c.con);
        DataTable dt6 = new DataTable();
        SqlDataAdapter da6 = new SqlDataAdapter(cmd6);
        da6.Fill(dt6);
        DataTable dtxx = new DataTable();
        dtxx = dt6;
        dt4.Merge(dt5);
        dt4.Merge(dt6);

        if (dt4.Rows.Count > 0)
        {
            ddl_system.DataSource = dt4;
            ddl_system.DataValueField = "System_Id";
            ddl_system.DataTextField = "System_Name";
            ddl_system.DataBind();
            ddl_system.Items.Insert(0, "Select");
        }

        dtx.Merge(dtxx);
        gridbind(dtx);
        c.con.Close();
    }
   
}