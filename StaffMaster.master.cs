﻿//Master page for staff

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class StaffMaster : System.Web.UI.MasterPage
{
    connection c=new connection();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            c.getcon();
            int Id = Convert.ToInt32(Session["SReg_Id"]);
            string s = "Select Name from StaffRegister where SReg_Id= '" + Id + "'";
            SqlCommand cmd = new SqlCommand(s, c.con);
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                lbl_welcome.Text = dt.Rows[0][0].ToString();
            }
            c.con.Close();
        }
    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        Session.Clear();
        Response.Redirect("Home.aspx");
    }
}
