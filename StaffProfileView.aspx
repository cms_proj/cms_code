﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StaffMaster.master" AutoEventWireup="true" CodeFile="StaffProfileView.aspx.cs" Inherits="StaffProfileView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
    
        .auto-style5 {
            height: 20px;
        }
    
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form>
    
        <table class="nav-justified">
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td style="height: 100px; width: 100px">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>
                    <asp:Panel ID="Panel1" runat="server" BorderStyle="Solid" style="margin-right: 325px" Width="399px">
                        <table>
        <tr>
            <td>&nbsp;</td>
            <td colspan="2">&nbsp;</td>
        </tr>
                            <tr>
                                <td class="text-center" colspan="3" style="font-size: 39px; font-weight: bold; font-family: Cambria, Cochin, Georgia, Times, 'Times New Roman', serif; color: #000099;">Profile</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2">&nbsp;</td>
                            </tr>
        <tr>
            <td>&nbsp;</td>
            <td style="font-size: 15px; font-weight: bold">Name</td>
            <td>
                <asp:Label ID="lbl_name" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
                            <tr>
                                <td class="auto-style5"></td>
                                <td class="auto-style5" style="font-size: 15px; font-weight: bold">Gender</td>
                                <td class="auto-style5">
                                    <asp:Label ID="lbl_gender" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style5">&nbsp;</td>
                                <td class="auto-style5">&nbsp;</td>
                                <td class="auto-style5">&nbsp;</td>
                            </tr>
        <tr>
            <td>&nbsp;</td>
            <td style="font-size: 15px; font-weight: bold">Date of Birth</td>
            <td>
                <asp:Label ID="lbl_dob" runat="server"></asp:Label>
            </td>
        </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
        <tr>
            <td>&nbsp;</td>
            <td style="font-size: 15px; font-weight: bold">Address</td>
            <td>
                <asp:TextBox ID="txt_address" runat="server" TextMode="MultiLine" CssClass="twitterStyleTextbox"></asp:TextBox>
            </td>
        </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td style="width: 150px">&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
        <tr>
            <td>&nbsp;</td>
            <td style="font-size: 15px; font-weight: bold">Phone</td>
            <td>
                <asp:Label ID="lbl_phn" runat="server"></asp:Label>
            </td>
        </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
        <tr>
            <td>&nbsp;</td>
            <td style="font-weight: bold; font-size: 15px">Email</td>
            <td>
                <asp:Label ID="lbl_email" runat="server"></asp:Label>
            </td>
        </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
        <tr>
            <td>&nbsp;</td>
            <td style="font-size: 15px; font-weight: bold">Qualification</td>
            <td>
                <asp:Label ID="lbl_qual" runat="server"></asp:Label>
            </td>
        </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
        <tr>
            
                <td class="auto-style5"></td>
            <td class="auto-style5" style="font-size: 15px; font-weight: bold"> Date Of Joiining</td>
            <td class="auto-style5">
                <asp:Label ID="lbl_doj" runat="server"></asp:Label>
                </td>
        </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            
                <td style="width: 100px">&nbsp;</td>
            <td style="width: 100px">&nbsp;</td>
            <td>&nbsp;</td>
        </td>
            
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
                    </asp:Panel>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td style="width: 100px">&nbsp;</td>
                <td style="width: 100px">&nbsp;</td>
                <td style="width: 300px; height: 100px;">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
    
</form>
</asp:Content>

