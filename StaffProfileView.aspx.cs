﻿

// This code allows the staff to view their details stored at the database

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class StaffProfileView : System.Web.UI.Page
{
    connection c = new connection();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Login_Id"] == null)
                Response.Redirect("Home.aspx");

            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache,no-store,max-age=0,must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
            c.getcon();
            int Id = Convert.ToInt32(Session["SReg_Id"]);
            string s = "Select Name,Gender,DOB,Address,Phone,E_mail,Qual_Id,DOJ from StaffRegister where SReg_Id= '" + Id + "'";
            SqlCommand cmd = new SqlCommand(s, c.con);
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                lbl_name.Text = dt.Rows[0][0].ToString();
                lbl_gender.Text = dt.Rows[0][1].ToString();
                lbl_dob.Text = dt.Rows[0][2].ToString();
                txt_address.Text = dt.Rows[0][3].ToString();
                lbl_phn.Text = dt.Rows[0][4].ToString();
                lbl_email.Text = dt.Rows[0][5].ToString();
                lbl_qual.Text = dt.Rows[0][6].ToString();
                lbl_doj.Text = dt.Rows[0][7].ToString();
            }
        }
    }
}