﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminMaster.master" AutoEventWireup="true" CodeFile="StaffRegistration.aspx.cs" Inherits="StaffRegistration" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>
          .modalBackground
    {
        background-color: Black;
        filter: alpha(opacity=60);
        opacity: 0.6;
    }
    .modalPopup
    {
        background-color: #FFFFFF;
        border: 3px solid #0DA9D0;
        border-radius: 12px;
        padding:0
        }
    .modalPopup .header
    {
        background-color: #2FBDF1;
        height: 30px;
        color: White;
        line-height: 30px;
        text-align: center;
        font-weight: bold;
        border-top-left-radius: 6px;
        border-top-right-radius: 6px;
    }
    .modalPopup .body
    {
        min-height: 50px;
        line-height: 30px;
        text-align: center;
        font-weight: bold;
    }
    .modalPopup .footer
    {
        padding: 6px;
    }
    .modalPopup .yes
    {
        background-color: #2FBDF1;
        border: 1px solid #0DA9D0;
    }
      
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form> 
        
        <table class="nav-justified">
            <tr>
                <td style="width: 300px"></td>
                <td style="height: 100px" ></td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <asp:Panel ID="Panel1" runat="server" Height="813px" BorderStyle="Solid" Width="645px">
                        <table>
            <tr>
                <td colspan="3" style="font-size: 39px; font-weight: bold; font-family: Constantia; color: #0000CC; text-align: center;">
                    &nbsp;</td>
            </tr>
                            <tr>
                                <td colspan="3" style="font-size: 39px; font-weight: bold; font-family: Constantia; color: #0000CC; text-align: center;">Staff Registration</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td> <asp:Button ID ="Button1" runat="server" style =" display:none;" Text="Button"/>


                                </td>
                            </tr>
            <tr>
                <td></td>
                <td style="font-weight: bold; font-size: 15px">Name</td>
                <td>
                    <asp:TextBox ID="txt_name" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txt_name" ErrorMessage="*Name can contain only letters and White Spaces" ForeColor="Red" ValidationExpression="^[a-zA-Z'.\s]{1,50}" ValidationGroup="eq"></asp:RegularExpressionValidator>
                </td>
            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
            <tr>
                <td></td>
                <td style="font-size: 15px; font-weight: bold">Gender</td>
                <td>
                    <asp:RadioButtonList ID="rbl_gender" runat="server" AutoPostBack="False" RepeatDirection="Horizontal">
                        <asp:ListItem>Female</asp:ListItem>
                        <asp:ListItem>Male</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
            <tr>
                <td></td>
                <td style="font-weight: bold; font-size: 15px">Date of Birth</td>
                <td>
                    <asp:TextBox ID="txt_dob" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                     <cc1:CalendarExtender ID="txt_dob_CalendarExtender" runat="server" Enabled="True" TargetControlID="txt_dob">
                    </cc1:CalendarExtender>
                     <asp:RequiredFieldValidator ID="req_dob" runat="server" ControlToValidate="txt_dob" ErrorMessage="*Required" ForeColor="Red" ValidationGroup="eq"></asp:RequiredFieldValidator>
                </td>
            </tr>
                            <tr>
                                <td style="width: 50px">&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
            <tr>
                <td></td>
                <td style="font-weight: bold; font-size: 15px">Address</td>
                <td>
                    <asp:TextBox ID="txt_address" runat="server" TextMode="MultiLine" CssClass="twitterStyleTextbox"></asp:TextBox>
                </td>
            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
            <tr>
                <td></td>
                <td style="font-weight: bold; font-size: 15px">Contact No.</td>
                <td>
                    <asp:TextBox ID="txt_contact" runat="server" CssClass="twitterStyleTextbox" ></asp:TextBox>
                
                    <asp:RegularExpressionValidator ID="req_ph" runat="server" ControlToValidate="txt_contact" ErrorMessage="*must be a 10 digit number" ForeColor="Red" ValidationExpression="[0-9]{10,12}" ValidationGroup="eq"></asp:RegularExpressionValidator>
                </td>
            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
            <tr>
                <td></td>
                <td style="font-weight: bold; font-size: 15px">E-mail</td>
                <td>
                    <asp:TextBox ID="txt_email" runat="server" CssClass="twitterStyleTextbox" ></asp:TextBox>
                
                    <asp:RequiredFieldValidator ID="Req_mail" runat="server" ControlToValidate="txt_email" ErrorMessage="*Required" ForeColor="Red" ValidationGroup="eq"></asp:RequiredFieldValidator>
                </td>
            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
            <tr>
                <td></td>
                <td style="font-size: 15px; font-weight: bold">Qualification</td>
                <td>
                    <asp:DropDownList ID="ddl_qual" runat="server" AutoPostBack="False" CssClass="search_categories">
                    </asp:DropDownList>
 
                    <asp:Label ID="req_qual" runat="server" Font-Bold="False" ForeColor="Red" Text="Select Qualification" Visible="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
                            <tr>
                                <td></td>
                                <td style="font-size: 15px; font-weight: bold">Date of Joining</td>
                                <td>
                                    <asp:TextBox ID="txt_doj" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                                    <cc1:CalendarExtender ID="txt_doj_CalendarExtender" runat="server" Enabled="True" TargetControlID="txt_doj">
                                    </cc1:CalendarExtender>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
            <tr>
                <td></td>
                <td style="font-size: 15px; font-weight: bold">User Type</td>
                <td>
                    <asp:TextBox ID="txt_usrtyp" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                    (Admin/Staff)
                    <asp:RequiredFieldValidator ID="req_usertype" runat="server" ControlToValidate="txt_usrtyp" ErrorMessage=" *required" ForeColor="Red" ValidationGroup="eq"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td></td>
                <td ></td>
                <td ></td>
            </tr>
            <tr>
                <td ></td>
                <td style="width: 200px" ></td>
                <td style="height: 30px">
                    <asp:Button ID ="ButDummy" runat="server" style =" display:none;" Text="Button"/>
                    <cc1:ModalPopupExtender ID="btn_add_ModalPopupExtender" runat="server" BackgroundCssClass="modalBackground" DynamicServicePath="" Enabled="True" PopupControlID="Panel_Popup" RepositionMode="None" TargetControlID="ButDummy">
                    </cc1:ModalPopupExtender>
                </td>
            </tr>
            <tr>
                <td>
                    </td>
                <td colspan="2">
                    
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Button ID="btn_add" runat="server" OnClick="btn_add_Click" style="text-align: center" Text="Add" ValidationGroup="eq" CssClass="btnme"/>
                    
                    
                </td>
            </tr>
                            <tr>
                                <td>
                                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                                    </asp:ScriptManager>
                                </td>
                                <td colspan="2">
                                    <asp:Panel ID="Panel_Popup" runat="server" BorderStyle="Solid" CssClass="modalPopup" Height="147px" Width="294px" Visible="False">
                                        <div>
                                            <br />
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <asp:Label ID="Lbl_added" runat="server" CssClass="modal-body" Text="Registered Successfully!!!" Font-Bold="True" Font-Size="11pt"></asp:Label>
                                            <br />
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
                                            <asp:Button ID="btb_OK" runat="server" CssClass="btnme" OnClick="btb_OK_Click" Text="OK" />
                                            <br />
                                            <br />
                                        </div>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="2">&nbsp;</td>
                            </tr>
        </table>
                    </asp:Panel>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td style="height: 100px">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
        
    </form>
</asp:Content>

