﻿
//This code allows the admin to register staffs

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class StaffRegistration : System.Web.UI.Page
{
    connection c = new connection();
    protected void Page_Load(object sender, EventArgs e)
    {
        this.UnobtrusiveValidationMode = System.Web.UI.UnobtrusiveValidationMode.None;
        if (!IsPostBack)
        {
            if (Session["Login_Id"] == null)
                Response.Redirect("Home.aspx");

            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache,no-store,max-age=0,must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
            
            //Binds Educational Qualification to the database

            c.getcon();
            string edu = "Select * from Qualification";
            SqlCommand cmdedu = new SqlCommand(edu, c.con);
            DataTable edudt = new DataTable();
            SqlDataAdapter eduda = new SqlDataAdapter(cmdedu);
            eduda.Fill(edudt);
            if (edudt.Rows.Count > 0)
            {
                ddl_qual.DataSource = edudt;
                ddl_qual.DataValueField = "Qual_Id";
                ddl_qual.DataTextField = "Qual";
                ddl_qual.DataBind();
            }
            ddl_qual.Items.Insert(0, "Select");
            c.con.Close();
        }
    }
    
    // Adds the details of staff to the database

    protected void btn_add_Click(object sender, EventArgs e)
    {
        if (ddl_qual.SelectedIndex > 0)
        {
            req_qual.Visible = false;
            c.getcon();
            string s = "Insert into StaffRegister values( '" + txt_name.Text + "','" + rbl_gender.SelectedItem.Value + "','" + txt_dob.Text + "','" + txt_address.Text + "','" + txt_contact.Text + "','" + txt_email.Text + "','" + ddl_qual.SelectedItem.Value + "','" + txt_doj.Text + "','" + txt_usrtyp.Text + "','Pending')";
            SqlCommand cmd = new SqlCommand(s, c.con);
            // object obj = new object();
            cmd.ExecuteNonQuery();
            c.con.Close();
            Panel_Popup.Visible = true;
         btn_add_ModalPopupExtender.Show();
        }
        else
        {
            req_qual.Visible = false;
        }
        
        

    }

    //Redirects to the Staffregistration page

    protected void btb_OK_Click(object sender, EventArgs e)
    {
        Response.Redirect("StaffRegistration.aspx");
    }
}