﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StaffMaster.master" AutoEventWireup="true" CodeFile="Test.aspx.cs" Inherits="Test" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
   
    <style type="text/css">
        .auto-style7 {
            text-align: center;
            color: #000099;
        }
    </style>
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form>
        <table style="border-spacing: 70px 20px;">
            <tr class="row">
                <td>&nbsp;</td>
                <td style="width: 100px">&nbsp;</td>
                <td style="width: 100px; height: 100px;">&nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr class="row">
                <td></td>
                <td style="width: 100px"></td>
                <td style="width: 100px">&nbsp;</td>
                <td>
                    <asp:Panel ID="Panel1" runat="server" Height="596px" BorderStyle="Solid" Width="632px">
                        <table>
            <tr class="row">
                <td colspan="4">
                    <h1 class="auto-style7" style="font-weight: bold; font-family: Consolas; font-size: 39px">Test</h1>
                </td>
            </tr>
                            <tr class="row">
                                <td colspan="4">&nbsp;</td>
                            </tr>
            <tr class="row">
                <td></td>
                <td></td>
                <td style="font-weight: bold">Name</td>
                <td>
                    <asp:DropDownList ID="ddl_name" runat="server" CssClass="search_categories" AutoPostBack="True">
                    </asp:DropDownList>
               </td>
            </tr>
                            <tr class="row">
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
            <tr class="row">
                <td></td>
                <td></td>
                <td style="font-weight: bold">Test Name</td>
                <td style="font-weight: bold">Aptitude</td>
            </tr>
                            <tr class="row">
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
            <tr class="row">
                <td></td>
                <td style="width: 100px"></td>
                <td style="font-weight: bold">Mark</td>
                <td>
                    
                    <asp:TextBox ID="txt_mark" runat="server" CssClass="twitterStyleTextbox"></asp:TextBox>
                    
                </td>
            </tr>
                            <tr class="row">
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
            <tr class="row">
                <td></td>
                <td ></td>
                <td style="font-weight: bold">Status</td>
                <td>
                    <asp:DropDownList ID="ddl_status" runat="server" CssClass="search_categories" AutoPostBack="True">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="row">
                <td>&nbsp;</td>
                <td >&nbsp;</td>
                <td >&nbsp;</td>
                <td >&nbsp;</td>
            </tr>
            <tr class="row">
                <td ></td>
                <td></td>
                <td>
                    <asp:Button ID="btn_add" runat="server" cssclass="btnme" OnClick="btn_add_Click" Text="Add"  />
                </td>
                <td >
          
                    <asp:Button ID="btn_update" runat="server" cssclass="btnme" OnClick="btn_update_Click" Text="Update" />
                </td>
            </tr>
                            <tr class="row">
                                <td>&nbsp;&nbsp;</td>
                                <td>&nbsp&nbsp;</td>
                                <td>&nbsp;&nbsp;</td>
                                <td>&nbsp;&nbsp;</td>
                            </tr>
            <tr class="row">
                <td>&nbsp;</td>
                <td colspan="3">
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" DataKeyNames="Eval_Id" Font-Bold="True" GridLines="Vertical" Height="173px" HorizontalAlign="Center" OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating" style="text-align: left" Width="503px">
                        <AlternatingRowStyle BackColor="#DCDCDC" />
                        <Columns>
                            <asp:BoundField DataField="Name" HeaderText="Name" />
                            <asp:BoundField DataField="Mark" HeaderText="Mark" />
                            <asp:BoundField DataField="Status" HeaderText="Status" />
                            <asp:BoundField DataField="Remarks" HeaderText="Remark" />
                            <asp:CommandField ShowEditButton="True" />
                        </Columns>
                        <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                        <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                        <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                        <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                        <SortedAscendingHeaderStyle BackColor="#0000A9" />
                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                        <SortedDescendingHeaderStyle BackColor="#000065" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>
                    </asp:Panel>
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td style="height: 100px; width: 200px"></td>
                <td></td>
                <td></td>
            </tr>
        </table>
            </form>
       </asp:Content>
