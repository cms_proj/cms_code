﻿
//This code adds the mark of initial entrance test to the database
//The student is choosen for the continuation of course based on the status of this test

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Test : System.Web.UI.Page
{
    connection c = new connection();

    // binds the test details to grid

    public void gridbind()
    {
        string gb = "Select Eval_Id,Name,Mark,InitialEvaluation.Status,Remarks from InitialEvaluation inner join Enquiry on InitialEvaluation.Enq_Id=Enquiry.Enq_Id";
        SqlCommand cmd = new SqlCommand(gb, c.con);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Login_Id"] == null)
                Response.Redirect("Home.aspx");

            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache,no-store,max-age=0,must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
            ddl_status.Items.Insert(0, "Select");
            ddl_status.Items.Add("Passed");
            ddl_status.Items.Add("Failed");
            ddl_status.Items.Add("Not Attended");
            
            //Bind Name of student to dropdownlist
            c.getcon();
            string nam = "Select * from Enquiry inner join Follow on Enquiry.Enq_Id = Follow.Enq_Id where Follow.Status='Confirmed'";
            SqlCommand cmdnam = new SqlCommand(nam, c.con);
            DataTable namdt = new DataTable();
            SqlDataAdapter namda = new SqlDataAdapter(cmdnam);
            namda.Fill(namdt);
            if (namdt.Rows.Count > 0)
            {
                ddl_name.DataSource = namdt;
                ddl_name.DataValueField = "Enq_Id";
                ddl_name.DataTextField = "Name";
                ddl_name.DataBind();
            }
            ddl_name.Items.Insert(0, "Select");
            gridbind();
            c.con.Close();
        }
    }


    //Adds the test details to the database

    protected void btn_add_Click(object sender, EventArgs e)
    {
        c.getcon();
        string s = "Select * from InitialEvaluation where Enq_Id='" + ddl_name.SelectedItem.Value + "'";
        SqlCommand cmdnam = new SqlCommand(s, c.con);
        DataTable namdt = new DataTable();
        SqlDataAdapter namda = new SqlDataAdapter(cmdnam);
        namda.Fill(namdt);
        if (namdt.Rows.Count > 0)

            Response.Write("<script>alert('Record already Exists')</script>");
        else
        {
            string ins = "Insert into InitialEvaluation values ('" + ddl_name.SelectedItem.Value + "','1','" + txt_mark.Text + "','" + ddl_status.SelectedItem.Value + "','')";
            SqlCommand cmd = new SqlCommand(ins, c.con);
            cmd.ExecuteNonQuery();
            gridbind();
            Response.Write("<script>alert('Inserted Sucessfully')</script>");
        }
    }
    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView1.EditIndex = e.NewEditIndex;
        c.getcon();
        gridbind();
        c.con.Close();
    }
    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        c.getcon();
        int id = Convert.ToInt32(GridView1.DataKeys[e.RowIndex].Values[0].ToString());
        string name = ((TextBox)GridView1.Rows[e.RowIndex].Cells[0].Controls[0]).Text;
        string mark = ((TextBox)GridView1.Rows[e.RowIndex].Cells[1].Controls[0]).Text;
        string status = ((TextBox)GridView1.Rows[e.RowIndex].Cells[2].Controls[0]).Text;
        string remark = ((TextBox)GridView1.Rows[e.RowIndex].Cells[3].Controls[0]).Text;
        string cmdstr = "Update InitialEvaluation set Mark='" + mark + "',Status='" + status + "',Remarks='" + remark + "' where Eval_Id ='"+id+"'";
        SqlCommand cmd = new SqlCommand(cmdstr, c.con);
        cmd.ExecuteNonQuery();
        c.con.Close();
        GridView1.EditIndex = -1;
        gridbind();
    
    }
    protected void btn_update_Click(object sender, EventArgs e)
    {
        c.getcon();
        string upd = "Update InitialEvaluation set Mark='" + txt_mark.Text + "',Status='" + ddl_status.SelectedItem.Value + "' where Enq_Id='" + ddl_name.SelectedItem.Value + "'";
        SqlCommand cmd = new SqlCommand(upd, c.con);
        cmd.ExecuteNonQuery();
        gridbind();
        Response.Write("<script>alert('Updated Sucessfully')</script>");
    }

    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView1.EditIndex = -1;
        c.getcon();
        gridbind();
        c.con.Close();
    }
}