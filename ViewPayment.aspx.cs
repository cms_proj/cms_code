﻿
//This code allows the staff to view the payment details of each student.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class ViewPayment : System.Web.UI.Page
{
    connection c = new connection();

    //Binds the name and payment id of the selected student to the grid

    public void gridbind()
    {
        string gb = "Select Reg_Id,Enquiry.Name from Registration inner join Enquiry on Registration.Enq_Id=Enquiry.Enq_Id where Payment_Mode='"+ddl_mode.SelectedItem.Text+"'";
        SqlCommand cmd = new SqlCommand(gb, c.con);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
    }

    //Binds the installment details to the grid

    public void gridtwobind()
    {
        int Rg_Id=Convert.ToInt32(Session["Rg_Id"]);
        string gb = "Select Payment_Id,Installment.Instal_No,Inst_Date,Inst_Amt,Registration.Status,Payment.Reciept_No,Payment.Reciept_Date from Payment inner join Registration on Payment.Reg_Id=Registration.Reg_Id inner join Installment on Payment.Instal_Id=Installment.Instal_Id where Payment.Reg_Id='" +Rg_Id+ "'";
        SqlCommand cmd1 = new SqlCommand(gb, c.con);
        DataTable dt1 = new DataTable();
        SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
        da1.Fill(dt1);
        if (dt1.Rows.Count > 0)
        {
            GridView2.DataSource = dt1;
            GridView2.DataBind();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Login_Id"] == null)
                Response.Redirect("Home.aspx");

            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache,no-store,max-age=0,must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
            ddl_mode.Visible = true;
        }


    }

    // displays the details in a popup window
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        Panel_popup.Visible = true;
        Link_View_ModalPopupExtender.Show();
        c.getcon();
        LinkButton lk = (LinkButton)sender;
        GridViewRow grv = lk.NamingContainer as GridViewRow;
        int id = Convert.ToInt32(GridView1.DataKeys[grv.RowIndex].Values[0].ToString());
        string sel = "Select Enquiry.Name,Technology.Tech,Registration.Status,Registration.Amount, Registration.Payment_Mode from Registration inner join Enquiry on Registration.Enq_Id=Enquiry.Enq_Id inner join OptedTech on Registration.OpTech_Id=OptedTech.OpTech_Id inner join Technology on Technology.Tech_Id=OptedTech.Tech_Id  where Registration.Reg_Id='" + id + "'";
        SqlCommand cmd = new SqlCommand(sel, c.con);
        Session["Rg_Id"] = id;
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            lbl_name.Text = dt.Rows[0][0].ToString();
            lbl_intern.Text = dt.Rows[0][1].ToString();
            lbl_status.Text = dt.Rows[0][2].ToString();
            lbl_fee.Text = dt.Rows[0][3].ToString();
            lbl_mode.Text = dt.Rows[0][4].ToString();         
        }
        cmd.ExecuteNonQuery();
        gridtwobind();
        c.con.Close();
    }
    protected void ddl_mode_SelectedIndexChanged(object sender, EventArgs e)
    {
        c.getcon();
        gridbind();
        c.con.Close();
    }
}