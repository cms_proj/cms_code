﻿
//This code allows the admin to view and approve staffs registered

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class ViewStaff : System.Web.UI.Page
{
    connection c = new connection();

    //Binds the staff details to the grid
    
    public void gridbind()
    {
        c.getcon();    
        string gb = "Select * from StaffRegister where status='Pending'" ;
        SqlCommand cmd = new SqlCommand(gb, c.con);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        c.con.Close();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Login_Id"] == null)
                Response.Redirect("Home.aspx");

            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache,no-store,max-age=0,must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
            gridbind();
        } 
    }
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "ApproveButton")
        {
            c.getcon();
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = GridView1.Rows[index];
            string up = "Update StaffRegister set Status='Approved' where SReg_Id='"+row.Cells[0].Text+"'";
            SqlCommand cmd = new SqlCommand(up, c.con);
            cmd.ExecuteNonQuery();
            c.con.Close();
            Response.Redirect("CreateUser.aspx?StaffNo=" + row.Cells[0].Text);
        }
    }
}