﻿
//This code allows the staff to view student details

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
public partial class ViewStudentStaff : System.Web.UI.Page
{
    connection c = new connection();

    //Binds the basic details of student to grid
    
    public void gridbind()
    {
        string gb = "select r.Reg_Id,e.Name,r.Status from Enquiry e inner join Registration r on e.Enq_Id=r.Enq_Id inner join OptedTech o on o.OpTech_Id=r.OpTech_Id inner join Course c on c.Course_Id=o.Course_Id where Course='" + ddl_Category.SelectedItem.Text + "' ";
        SqlCommand cmd = new SqlCommand(gb, c.con);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Login_Id"] == null)
                Response.Redirect("Home.aspx");

            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache,no-store,max-age=0,must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
            // binds Course to dropdownlist

            c.getcon();
            string cou = "Select * from Course";
            SqlCommand cmdco = new SqlCommand(cou, c.con);
            DataTable codt = new DataTable();
            SqlDataAdapter coda = new SqlDataAdapter(cmdco);
            coda.Fill(codt);
            if (codt.Rows.Count > 0)
            {
                ddl_Category.DataSource = codt;
                ddl_Category.DataValueField = "Course_Id";
                ddl_Category.DataTextField = "Course";
                ddl_Category.DataBind();
            }
            ddl_Category.Items.Insert(0, "Select");
            c.con.Close();
        }
    }
    protected void ddl_Category_SelectedIndexChanged(object sender, EventArgs e)
    {
        c.getcon();
        gridbind();
        c.con.Close();
    }

    //Shows the details of the student in a popup window

    protected void LinkButton1_Click(object sender, EventArgs e)
    {

        LinkButton1_ModalPopupExtender.Show();
        LinkButton lk = (LinkButton)sender;
        GridViewRow gw = lk.NamingContainer as GridViewRow;
        int id = Convert.ToInt32(GridView1.DataKeys[gw.RowIndex].Values[0].ToString());
        Session["id"]=id;
        Panel2.Visible = true;
        c.getcon();
        SqlCommand cmd1 = new SqlCommand("Select * from Registration where Reg_Id='" + id + "'", c.con);
        SqlDataAdapter sda = new SqlDataAdapter(cmd1);
        DataTable dt = new DataTable();
        sda.Fill(dt);
        int k = cmd1.ExecuteNonQuery();
        if (dt.Rows.Count > 0)
        {
            DataRow row1 = dt.Rows[dt.Rows.Count - 1];
            string imagepath = Convert.ToString(row1[2]);
            img_photo.ImageUrl = imagepath;
            SqlCommand cmdname = new SqlCommand("Select name, r.Status  from Enquiry e inner join Registration r on e.Enq_Id=r.Enq_Id where Reg_Id='" + id + "'", c.con);
            SqlDataAdapter namda = new SqlDataAdapter(cmdname);
            DataTable namdt = new DataTable();
            namda.Fill(namdt);
            cmdname.ExecuteNonQuery();
            if (namdt.Rows.Count > 0)
            {
                DataRow row0 = namdt.Rows[namdt.Rows.Count - 1];
                string name = Convert.ToString(row0[0]);
                lbl_name.Text = name;
                rbl_Status.Items.FindByText(Convert.ToString(row0[1])).Selected = true;

            }

            string rid = Convert.ToString(row1[0]);
            string rdate = Convert.ToString(row1[3]);
            string duration = Convert.ToString(row1[4]);
            SqlCommand cmdopt = new SqlCommand("Select Tech from Technology T inner join OptedTech O on T.Tech_Id=O.Tech_Id inner join Registration R on O.OpTech_Id=R.OpTech_Id  where Reg_Id='" + id + "'", c.con);
            SqlDataAdapter techda = new SqlDataAdapter(cmdopt);
            DataTable techdt = new DataTable();
            techda.Fill(techdt);
            cmdopt.ExecuteNonQuery();
            if (techdt.Rows.Count > 0)
            {
                DataRow row2 = techdt.Rows[techdt.Rows.Count - 1];
                string optech = Convert.ToString(row2[0]);
                lbl_optech.Text = optech;
            }
            SqlCommand cmdco = new SqlCommand("Select Course from Course C inner join OptedTech O on C.Course_Id=O.Course_Id inner join Registration R on R.OpTech_Id=O.Optech_Id where Reg_Id='" + id + "'", c.con);
            SqlDataAdapter corda = new SqlDataAdapter(cmdco);
            DataTable cordt = new DataTable();
            corda.Fill(cordt);
            cmdco.ExecuteNonQuery();
            if (cordt.Rows.Count > 0)
            {
                DataRow row3 = cordt.Rows[cordt.Rows.Count - 1];
                string course = Convert.ToString(row3[0]); ;
                lbl_course.Text = course;
                
            }
            string startdate = Convert.ToString(row1[10]);
            string endate = Convert.ToString(row1[11]);
            lbl_regdate.Text = rdate;
            lbl_duration.Text = duration;
            lbl_startdate.Text = startdate;
            lbl_enDate.Text = endate;

        }
    }
    protected void btn_ok_Click(object sender, EventArgs e)
    {
        c.getcon();
        string update = "Update Registration set Status='" + rbl_Status.SelectedItem.Text + "' where Reg_Id='" + Convert.ToInt32(Session["id"]) + "'";
        SqlCommand cmd = new SqlCommand(update, c.con);
        cmd.ExecuteNonQuery();
        gridbind();
        c.con.Close();

    }
}